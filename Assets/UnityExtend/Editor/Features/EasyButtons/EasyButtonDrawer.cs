using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public static class EasyButtonsEditorExtensions
{
	public static void DrawButtons(this UnityEditor.Editor editor)
	{
		// select methods with button attribute
		var separatorDrawn = false;
		var methodsWithButtons = editor.target.GetType()
			.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
			.Where(x => x.IsDefined(typeof(ButtonAttribute)));

		foreach (var method in methodsWithButtons)
		{
			var buttonArg = (ButtonAttribute)method.GetCustomAttribute(typeof(ButtonAttribute));
			if (buttonArg == null) { return; }

			if (!separatorDrawn)
			{
				separatorDrawn = true;
				EditorGUIX.DrawSeparator();
			}

			// Determine whether the button should be enabled based on its mode
			GUI.enabled =
				buttonArg.Mode == ButtonMode.AlwaysEnabled ||
				(EditorApplication.isPlaying && buttonArg.Mode == ButtonMode.EnabledInPlayMode) ||
				(!EditorApplication.isPlaying && buttonArg.Mode == ButtonMode.DisabledInPlayMode);

			// space before feature
			if (buttonArg.SpaceBefore)
				GUILayout.Space(10);

			// get nice name
			var buttonName = !string.IsNullOrEmpty(buttonArg.Name) ?
				ObjectNames.NicifyVariableName(buttonArg.Name) :
				ObjectNames.NicifyVariableName(method.Name);

			// Draw a button which invokes the method
			if (GUILayout.Button(buttonName))
			{
				foreach (var t in editor.targets)
					method.Invoke(t, null);
			}

			// space after feature
			if (buttonArg.SpaceAfter)
				GUILayout.Space(10);
		}
	}
}