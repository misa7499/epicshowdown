using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;

/// <summary>
/// Base class of all custom editors that need EasyEditor functionalities.
/// </summary>
[CanEditMultipleObjects]
[CustomEditor(typeof(MonoBehaviour), true)]
public class Editor : UnityEditor.Editor
{
	public override void OnInspectorGUI()
	{
		DoDrawDefaultInspector(serializedObject);
		this.DrawButtons();
	}

	public static bool DoDrawDefaultInspector(SerializedObject obj)
	{
		EditorGUI.BeginChangeCheck();
		obj.Update();

		// Loop through properties and create one field (including children) for each top level property.
		SerializedProperty property = obj.GetIterator();

		// script
		property.NextVisible(true);
		EditorGUILayout.PropertyField(property);

		// draw all the others.
		var expand = property.isExpanded;
		while(property.NextVisible(expand))
		{
			expand = DrawProperty(property);
		}

		obj.ApplyModifiedProperties();
		return EditorGUI.EndChangeCheck();
	}

	private static bool DrawProperty(SerializedProperty property)
	{
		// get everything needed through reflection
		var parsedProperty = property.Parse();

		// apply attributes
		if (parsedProperty.FieldInfo is FieldInfo field)
		{
			// check condition field attribute
			if (field.IsDefined(typeof(ConditionalFieldAttribute), true))
			{
				foreach(var attribute in field.GetCustomAttributes(true))
				{
					if (attribute is ConditionalFieldAttribute conditionalAttribute && !conditionalAttribute.ShouldShow(property))
						return false;
				}
			}
		}

		// actual draw
		Profiler.BeginSample("EasyEditor.DrawProperty");
		{
			EditorGUI.indentLevel = parsedProperty.IndentLevel;

			if (parsedProperty.IsFlatten)
			{
				// simulate header
				GUILayout.Space(8f);
				EditorGUILayout.LabelField(property.displayName, EditorStyles.boldLabel);
			}
			else
			{
				EditorGUILayout.PropertyField(property);
			}
		}
		Profiler.EndSample();

		var isTableRow = !property.isArray && parsedProperty.FieldInfo.IsDefined(typeof(TableAttribute));

		// should it show children check?
		return (parsedProperty.IsFlatten || property.isExpanded) &&	!isTableRow;
	}
}

/// <summary>
/// Replaces default inspector for scriptable objects to enable  EasyEditor functionalities.
/// </summary>
[CanEditMultipleObjects]
[CustomEditor(typeof(ScriptableObject), true)]
public class ScriptableObjectEditor : Editor<ScriptableObject>
{
}

/// <summary>
/// Extend custom editor from this to avoid casting target.
/// </summary>
[CanEditMultipleObjects]
public class Editor<T> : Editor where T : UnityEngine.Object
{
	public new T target => (T)base.target;
}