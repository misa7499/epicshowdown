using UnityEditor;
using UnityEngine;

/// <summary>
/// Extension of EditorGUI.
/// </summary>
public static class EditorGUIX
{
	/// <summary>
	/// Draw a visible separator in addition to adding some padding.
	/// </summary>
	public static void DrawSeparator(float height = 27f, float lineThickness = 2f)
	{
		GUILayout.Space(height);

		if (Event.current.type == EventType.Repaint)
		{
			var tex = EditorGUIUtility.whiteTexture;
			var rect = GUILayoutUtility.GetLastRect();
			rect.y += rect.height * 0.5f;
			rect.width = Screen.width - rect.x * 2;
			rect.height = lineThickness;

			var oldColor = GUI.color;
			GUI.color = new Color(0f, 0f, 0f, 0.25f);
				GUI.DrawTexture(rect, tex);
			GUI.color = oldColor;
		}
	}
}