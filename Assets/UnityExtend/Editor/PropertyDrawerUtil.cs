﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public static class PropertyDrawerUtil
{
	/// <summary>
	/// Gets attribute linked to this property.
	/// </summary>
	public static T GetValue<T>(this SerializedProperty property)
	{
		return property.GetValueInternal<T>(property.serializedObject.targetObject);
	}

	public static IEnumerable<T> GetValues<T>(this SerializedProperty property)
	{
		foreach(var target in property.serializedObject.targetObjects)
		{
			yield return property.GetValueInternal<T>(target);
		}
	}

	public static T GetValueInternal<T>(this SerializedProperty property, object target)
	{
		IList list = null;
		object value = target;
		foreach (var node in property.ParsePath())
		{
			try
			{
				value = node.GetValue(value, out list);
			}
			catch(System.Exception e)
			{
				Debug.LogError("Error while accessing: " + node);
				Debug.LogException(e);
				return default(T);
			}
		}

		// check if this is maybe the main return value.
		if (value is T) { return (T)value; }
		if (list is T) { return (T)list; }
		return default(T);
	}

	/// <summary>
	/// Sets new value to the field or array element represented by this SerializedProperty.
	/// </summary>
	public static void SetValue<T>(this SerializedProperty property, T newValue)
	{
		var nodes = property.ParsePath();

		foreach(var target in property.serializedObject.targetObjects)
		{
			object value = target;

			for (int i = 0; i < nodes.Count - 1; i++)
			{
				IList list;
				value = nodes[i].GetValue(value, out list);
			}

			var lastNode = nodes[nodes.Count - 1];
			lastNode.SetValue(value, newValue);
		}
	}

	public static void SetValueRecourdUndo<T>(this SerializedProperty property, T newValue, string undoOp = "Value Changed")
	{
		UnityEditor.Undo.RecordObjects(property.serializedObject.targetObjects, undoOp);
		property.SetValue(newValue);
	}

	/// <summary>
	/// Returns true if property is direct field of the serialized object, by checking length of property path.
	/// </summary>
	public static bool IsDirectChild(this SerializedProperty property)
	{
		return property.ParsePath().Count <= 1;
	}

	/// <summary>
	/// Turns property path into a list of nodes.
	/// </summary>
	public static List<Node> ParsePath(this SerializedProperty property)
	{
		List<Node> nodes;
		var path = property.propertyPath;
		if (!nodeCache.TryGetValue(path, out nodes))
		{
			nodes = new List<Node>();
			foreach(var n in path.Replace(".Array.data[", "[").Split('.'))
			{
				nodes.Add(new Node(n));
			}

			nodeCache.Add(path, nodes);
		}
		return nodes;
	}

	/// <summary>
	/// Turns property path into a list of nodes.
	/// </summary>
	public static int GetIndentLevel(this SerializedProperty property)
	{
		try
		{
			var nodePath = property.ParsePath();
			if (nodePath != null && nodePath.Count > 0) { return nodePath.Count - 1; }
		}
		catch(Exception e)
		{
			Debug.LogException(e);
		}
		return 0;
	}

	/// <summary>
	/// PropertyPaths are strings, cache list of nodes after parsing.
	/// </summary>
	private static readonly Dictionary<string, List<Node>> nodeCache = new Dictionary<string, List<Node>>();

	/// <summary>
	/// Node of SerializedProperty path. Represetns one nested field or array element.
	/// </summary>
	public class Node
	{
		public string fieldName;
		public int arrayIndex;
		public FieldInfo field;

		public override string ToString()
		{
			return fieldName + " " + arrayIndex;
		}

		public Node(string path)
		{
			fieldName = path;

			// check if node represents list element
			arrayIndex = -1;
			var arrayIndexStart = path.IndexOf('[');
			if (arrayIndexStart != -1)
			{
				arrayIndex = Int32.Parse(path.Substring(arrayIndexStart + 1, path.Length - arrayIndexStart - 2));
				fieldName = fieldName.Substring(0, arrayIndexStart);
			}
		}

		public object GetValue(object value, out IList list)
		{
			field = GetField(value.GetType(), fieldName);
			value = field.GetValue(value);

			list = value as IList;
			return list != null && list.Count > arrayIndex && arrayIndex > 0 ? list[arrayIndex] : value;
		}

		public void SetValue<T>(object value, T newValue)
		{
			field = GetField(value.GetType(), fieldName);
			if (arrayIndex != -1)
			{
				var list = (IList<T>)field.GetValue(value);
				list[arrayIndex] = newValue;
			}
			else
			{
				field.SetValue(value, newValue);
			}
		}

		public static FieldInfo GetField(Type type, string fieldName)
		{
			var field = type.GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			if (field != null) { return field; }
			if (type.BaseType != null) { return GetField(type.BaseType, fieldName); }
			return null;
		}
	}
}