using System;

public enum ButtonMode
{
	AlwaysEnabled,
	EnabledInPlayMode,
	DisabledInPlayMode
}

[Flags]
public enum ButtonSpacing
{
	None = 0,
	Before = 1,
	After = 2
}

/// <summary>
/// Attribute to create a button in the inspector for calling the method it is attached to.
/// The method must have no arguments.
/// </summary>
/// <example>
/// [Button]
/// public void MyMethod()
/// {
///     Debug.Log("Clicked!");
/// }
/// </example>
[AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
public sealed class ButtonAttribute : Attribute
{
	public readonly string Name;
	public readonly ButtonMode Mode = ButtonMode.AlwaysEnabled;
	public readonly ButtonSpacing Spacing = ButtonSpacing.None;

	public bool SpaceBefore => Spacing == ButtonSpacing.Before;
	public bool SpaceAfter => Spacing == ButtonSpacing.After;

	public ButtonAttribute()
	{
	}

	public ButtonAttribute(string name)
	{
		Name = name;
	}

	public ButtonAttribute(ButtonMode mode)
	{
		Mode = mode;
	}

	public ButtonAttribute(ButtonSpacing spacing)
	{
		Spacing = spacing;
	}

	public ButtonAttribute(string name, ButtonMode mode)
	{
		Name = name;
		Mode = mode;
	}

	public ButtonAttribute(string name, ButtonSpacing spacing)
	{
		Name = name;
		Spacing = spacing;
	}

	public ButtonAttribute(string name, ButtonMode mode, ButtonSpacing spacing)
	{
		Name = name;
		Mode = mode;
		Spacing = spacing;
	}
}