using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(BattleObject3D), true)]
public class BattleObjectInspector : Editor<BattleObject3D>
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		{
			if (ShowMonoScript.Show(serializedObject))
			{
				return;
			}

			// get list of all properties.
			var allProps = new List<SerializedProperty>();
			var property = serializedObject.GetIterator();
			property.NextVisible(true);
			while(property.NextVisible(false))
			{
				var propertyPath = property.propertyPath;
				if (propertyPath == "serializedData") { continue; }

				allProps.Add(serializedObject.FindProperty(propertyPath));
			}

			// draw them
			DrawProperties(allProps, target.name);
		}
		serializedObject.ApplyModifiedProperties();

		// display activate object state
		DrawSimObjectState();
	}

	void DrawSimObjectState()
	{
		if (target.Data != null)
		{
			var serialziedDataProp = serializedObject.FindProperty("serializedData");
			if (serialziedDataProp == null) { return; }

			var serialziedData = serialziedDataProp.Parse();
			serialziedData.FieldInfo.SetValue(target, target.Data);

			using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
			{
				GUILayout.Label("Game State", EditorStyles.boldLabel);
				EditorGUILayout.PropertyField(serialziedDataProp, true);
			}
		}
	}

	void DrawProperties(IEnumerable<SerializedProperty> props, string label, bool onlyWithChildren = true)
	{
		if (props.Count() == 0) { return; }

		// draw props
		EditorGUI.indentLevel++;
		using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
		{
			GUILayout.Label(label, EditorStyles.boldLabel);
			foreach(var prop in props)
			{
				if (onlyWithChildren || prop.hasVisibleChildren)
				{
					EditorGUILayout.PropertyField(prop, includeChildren: true);
				}

				foreach(var editorUpdate in prop.GetValues<EditorUpdate>())
				{
					editorUpdate?.EditorUpdate();
				}
			}
			GUILayout.Space(6f);
		}
		EditorGUI.indentLevel--;
	}
}