using GameConsole;
using UnityEditor;
using UnityEngine;
using static UnityEngine.GUILayout;
using static UnityEditor.EditorGUILayout;
using static EditorGUIX;
using static BattleCommands;
using static Game;

public class BattleCommandsWindow : EditorWindow
{
	public UnitSettings UnitToSpawn;

	[ExecutableCommand]
	public static void ShowBattleCommandsWindow()
	{
		GetWindow<BattleCommandsWindow>("Battle Commands");
	}

	void OnGUI()
	{
		using(new EditorGUI.DisabledGroupScope(Game.Battle == null))
		{
			if (Button("Spawn All"))
				SpawnAll();

			if (Button("Kill All Minions"))
				KillAllMinions();

			if (!BattleLog.IsLogging && Button("Log Battle"))
			{
				BattleLog.StartLogging(Game.Battle, BattleLoggerSettings.Instance);
			}
			else if (BattleLog.IsLogging && Button("Stop Logging"))
			{
				BattleLog.StopLogging();
			}
		}

		if (Button("Collision Settings"))
			Selection.activeObject = CollisionSettings.Instance;

		DrawSeparator();

		Time.timeScale = Slider("Time Scale", Time.timeScale, 0, 5);
	}
}