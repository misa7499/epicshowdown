using System.Linq;
using UnityEngine;

public static class BattleDebug
{
	public static bool IsBeingDebugged(this SimObject2D obj)
	{
#if UNITY_EDITOR
		var selectedGO = UnityEditor.Selection.activeTransform;
		return selectedGO != null && Game.ViewController?.GetView(obj)?.transform is Transform view && selectedGO.IsChildOf(view);
#else
		return false;
#endif
	}
}