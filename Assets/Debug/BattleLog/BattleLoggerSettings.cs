public class BattleLoggerSettings : ScriptableObject<BattleLoggerSettings>
{
	public bool LogAveragePosition = true;
	public bool LogMinionPositions = true;
	public bool LogRelativeToNexus = true;
}