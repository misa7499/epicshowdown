using System;
using System.IO;
using System.Linq;
using UnityEngine;

public static class BattleLog
{
	public static string LogDirectory = "Logs/BattleLogs/";
	public static string LogPath = "Logs/BattleLogs/battleLog-{0:D3}.txt";
	public static BattleLogger ActiveLog { get; private set; }
	public static bool IsLogging => ActiveLog != null;

	public static void StartLogging(BattleController battle, BattleLoggerSettings logSettings)
	{
		Directory.CreateDirectory(LogDirectory);

		ActiveLog = new BattleLogger
		(
			battle: battle,
			logPath: string.Format(LogPath, Directory.GetFiles(LogDirectory).Count(p => p.EndsWith(".txt")) + 1),
			settings: logSettings
		);
	}

	public static void StopLogging()
	{
		if (ActiveLog == null)
		{
			Debug.LogWarning("No battle logger active");
			return;
		}

		ActiveLog.StopLogging();
		ActiveLog = null;
	}
}

public class BattleLogger
{
	public readonly BattleController Battle;
	public readonly StreamWriter FileWriter;
	public readonly BattleLoggerSettings Settings;

	public BattleLogger(BattleController battle, BattleLoggerSettings settings, string logPath)
	{
		Battle = battle;
		Settings = settings;
		FileWriter = new StreamWriter(logPath, append: true);
		Game.Battle.OnTimeTick += LogTick;
		Game.Battle.OnDispose += BattleLog.StopLogging;
	}

	void LogTick(TimeTick timeTick)
	{
		FileWriter.WriteLine(timeTick.CurrentTime);
		FileWriter.WriteLine("----------");

		if (Settings.LogAveragePosition) LogAveragePositions();
		if (Settings.LogMinionPositions) LogMinions();

		FileWriter.WriteLine("");
	}

	private void LogMinions()
	{
		foreach (var minion in Battle.Simulator.AllUnits.OfType<Minion>())
		{
			FileWriter.WriteLine($"{minion.Id} pos={logPos(minion.Owner, minion.position)} dir={minion.lookDirection} target={minion.ActiveTarget?.Id ?? 0} actionType={minion.ActionType}");
		}
	}

	public void LogAveragePositions()
	{
		foreach(var playerOwner in Battle.Simulator.AllPlayers.Select(p => p.Owner))
		{
			var castlePos = Battle.Simulator.AllPlayers.Get(playerOwner).Nexus.position;
			var averagePos = Battle.Simulator.AllUnits.Where(x => x.Owner == playerOwner).OfType<Minion>().Average(x => x.position);
			FileWriter.WriteLine($"{playerOwner} averagePos={logPos(playerOwner, averagePos)}");
		}
	}

	private NVector2 logPos(OwnerId owner, NVector2 position)
	{
		if (Settings.LogRelativeToNexus && Battle.GetNexus(owner) is Nexus nexus)
		{
			position = position - nexus.position;
		}
		return position;
	}

	public void StopLogging()
	{
		Battle.OnTimeTick -= LogTick;
		FileWriter.Close();
		FileWriter.Dispose();
	}
}