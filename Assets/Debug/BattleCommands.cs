using System;
using System.Collections;
using System.Linq;
using GameConsole;
using UnityEngine;
using static Game;

public static class BattleCommands
{
	[ExecutableCommand]
	public static void ExitBattle()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	[ExecutableCommand]
	public static void PlaceUnit(int x, int y, string unit)
	{
		var tile = Board.Tiles[x, y];
		if (tile == null) { return; }

		var settings = (UnitSettings)unit;
		Game.Simulator.AddUnits(settings, tile.Position, Game.BattleUI.DisplayedPlayer.Owner, settings.SpawnCount);
	}

	[ExecutableCommand]
	public static void StressTestUnitCount(int count)
	{
		Invoker.StartCoroutine(coroutine());

		IEnumerator coroutine()
		{
			while(Game.Simulator.AllUnits.Count < count)
			{
				SpawnAll();
				yield return new WaitForSeconds(0.25f);
			}
		}
	}


	[ExecutableCommand]
	public static void SpawnAll()
	{
		foreach(var player in Game.Simulator.AllPlayers)
			SpawnUnits((int)player.Owner, "Warrior");
	}

	[ExecutableCommand]
	public static void SpawnUnits(int owner, string unit)
	{
		var settings = (UnitSettings)unit;
		var player = Game.Simulator.AllPlayers.Get((OwnerId)owner);
		if (!player.Nexus.IsActive)
		{
			return;
		}

		foreach(var tile in player.Nexus.Tile.Neighbours)
		{
			Game.Simulator.AddUnits(settings, tile.Position, player.Owner, settings.SpawnCount);
		}
	}

	[ExecutableCommand]
	public static void GiveGold(int amount)
	{
		Game.BattleUI.DisplayedPlayer?.Gold.Give(amount);
	}

	[ExecutableCommand]
	public static void KillAllMinions()
	{
		Game.Simulator.AllUnits
			.Where(unit => !unit.IsStructure)
			.Foreach(x => x.Deactivate());
	}

	[ExecutableCommand]
	public static void SayHello()
	{
		Game.Simulator.AllUnits[0].ShowFloatingText("HELLO!");
	}
}