using System.Text;
using GameConsole;

public static class AICommands
{
	[ExecutableCommand]
	public static string LogMyOptions()
	{
		var player = Game.BattleUI.DisplayedPlayer;
		if (player == null)
			return null;

		var sb = new StringBuilder();

		var aiPlayer = new AIPlayer(player, Game.Settings.AIKernels.Load("DefaultAIKernel"));
		var options = aiPlayer.OptionsCollector.CollectOptions();

		if (options.Count == 0)
			sb.AppendLine($"No options available.");

		foreach(var option in options)
			sb.AppendLine($"{option} = {aiPlayer.Evaluate(option)}");

		return sb.ToString();
	}
}