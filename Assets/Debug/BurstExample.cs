using System;
using System.Diagnostics;
using System.Text;
using GameConsole;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public static class BurstExample
{
	public const int COUNT1 = 1000000;

	[ExecutableCommand]
	public static string BurstnemMuGaMajke()
	{
		var sb = new StringBuilder();

		// ------
		{
			var job = new JobNFloatParallel2()
			{
				input = new NativeArray<double>(COUNT1, Allocator.TempJob),
				data = new NativeArray<double>(COUNT1, Allocator.TempJob),
				output = new NativeArray<double>(COUNT1, Allocator.TempJob),
			};

			for(int i = 0; i < COUNT1; i++)
			{
				job.input[i] = (double)i;
				job.data[i] = (double)i;
			}

			var sw = Stopwatch.StartNew();
			{
				job.Run(COUNT1);
			}
			sw.Stop();

			var result = job.output[16];
			job.output.Dispose();
			sb.AppendLine($"The result of the {job.GetType()} is: {result} calcLated in {sw.Elapsed.TotalMilliseconds}");
		}
		// ------

		// ------
		{
			var job = new JobNFloatParallel()
			{
				input = new NativeArray<long>(COUNT1, Allocator.TempJob),
				data = new NativeArray<long>(COUNT1, Allocator.TempJob),
				output = new NativeArray<long>(COUNT1, Allocator.TempJob),
			};

			for(int i = 0; i < COUNT1; i++)
			{
				job.input[i] = (long)i;
				job.data[i] = (long)i;
			}

			var sw = Stopwatch.StartNew();
			{
				job.Run(COUNT1);
			}
			sw.Stop();

			var result = job.output[16];
			job.output.Dispose();
			sb.AppendLine($"The result of the {job.GetType()} is: {result} calcLated in {sw.Elapsed.TotalMilliseconds}");
		}
		// ------

		// ------
		{
			var job = new JobNFloatParallel2()
			{
				input = new NativeArray<double>(COUNT1, Allocator.TempJob),
				data = new NativeArray<double>(COUNT1, Allocator.TempJob),
				output = new NativeArray<double>(COUNT1, Allocator.TempJob),
			};

			for(int i = 0; i < COUNT1; i++)
			{
				job.input[i] = (double)i;
				job.data[i] = (double)i;
			}

			var sw = Stopwatch.StartNew();
			{
				job.Run(COUNT1);
			}
			sw.Stop();

			var result = job.output[16];
			job.output.Dispose();
			sb.AppendLine($"The result of the {job.GetType()} is: {result} calcLated in {sw.Elapsed.TotalMilliseconds}");
		}
		// ------

		return sb.ToString();
	}

	[BurstCompile]
	public struct JobNFloatParallel : IJobParallelFor
	{
		[ReadOnly, DeallocateOnJobCompletion] public NativeArray<long> input;
		[ReadOnly, DeallocateOnJobCompletion] public NativeArray<long> data;
		[WriteOnly] public NativeArray<long> output;

		public void Execute(int i)
		{
			long u = input[i] << NFloat.DecimalBits;
			long g = 0x80000000L;
			if (0x00800000L*0x00800000L < u)
			{
				if (g * g > u) g ^= 0xC0000000L; else g |= 0x40000000L;
				if (g * g > u) g ^= 0x60000000L; else g |= 0x20000000L;
				if (g * g > u) g ^= 0x30000000L; else g |= 0x10000000L;
				if (g * g > u) g ^= 0x18000000L; else g |= 0x08000000L;
				if (g * g > u) g ^= 0x0C000000L; else g |= 0x04000000L;
				if (g * g > u) g ^= 0x06000000L; else g |= 0x02000000L;
				if (g * g > u) g ^= 0x03000000L; else g |= 0x01000000L;
				if (g * g > u) g ^= 0x01800000L; else g |= 0x00800000L;
			}
			else
			{
				g = 0x00800000L;
			}

			if (g * g > u) g ^= 0x00C00000L; else g |= 0x00400000L;
			if (g * g > u) g ^= 0x00600000L; else g |= 0x00200000L;
			if (g * g > u) g ^= 0x00300000L; else g |= 0x00100000L;
			if (g * g > u) g ^= 0x00180000L; else g |= 0x00080000L;
			if (g * g > u) g ^= 0x000C0000L; else g |= 0x00040000L;
			if (g * g > u) g ^= 0x00060000L; else g |= 0x00020000L;
			if (g * g > u) g ^= 0x00030000L; else g |= 0x00010000L;
			if (g * g > u) g ^= 0x00018000L; else g |= 0x00008000L;

			if (g * g > u) g ^= 0x0000C000L; else g |= 0x00004000L;
			if (g * g > u) g ^= 0x00006000L; else g |= 0x00002000L;
			if (g * g > u) g ^= 0x00003000L; else g |= 0x00001000L;
			if (g * g > u) g ^= 0x00001800L; else g |= 0x00000800L;
			if (g * g > u) g ^= 0x00000C00L; else g |= 0x00000400L;
			if (g * g > u) g ^= 0x00000600L; else g |= 0x00000200L;
			if (g * g > u) g ^= 0x00000300L; else g |= 0x00000100L;
			if (g * g > u) g ^= 0x00000180L; else g |= 0x00000080L;

			if (g * g > u) g ^= 0x000000C0L; else g |= 0x00000040L;
			if (g * g > u) g ^= 0x00000060L; else g |= 0x00000020L;
			if (g * g > u) g ^= 0x00000030L; else g |= 0x00000010L;
			if (g * g > u) g ^= 0x00000018L; else g |= 0x00000008L;
			if (g * g > u) g ^= 0x0000000CL; else g |= 0x00000004L;
			if (g * g > u) g ^= 0x00000006L; else g |= 0x00000002L;
			if (g * g > u) g ^= 0x00000003L; else g |= 0x00000001L;
			if (g * g > u) g ^= 0x00000001L;

			output[i] = g;
		}
	}

	[BurstCompile]
	public struct JobNFloatParallel2 : IJobParallelFor
	{
		[ReadOnly, DeallocateOnJobCompletion] public NativeArray<double> input;
		[ReadOnly, DeallocateOnJobCompletion] public NativeArray<double> data;
		[WriteOnly] public NativeArray<double> output;

		public void Execute(int i)
		{
			output[i] = Math.Sqrt((double)i);
		}
	}
}