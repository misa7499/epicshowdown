using System;
using UnityEngine;
using static FixedFloatMath;

public static class MathNFloat2D
{
	public static bool PathObstructed(NFloat from_x, NFloat from_y, NFloat to_x, NFloat to_y, NFloat circle_x, NFloat circle_y,
		NFloat radius, out NFloat distance)
	{
		var f_x = from_x.value - circle_x.value;
		var f_y = from_y.value - circle_y.value;
		var f_sqr = f_x*f_x + f_y*f_y >> DecimalBits;

		var d_x = to_x.value - from_x.value;
		var d_y = to_y.value - from_y.value;
		var d_sqr = d_x*d_x + d_y*d_y >> DecimalBits;
		if (d_sqr <= f_sqr)
		{
			distance = new NFloat();
			return false;
	}

		// forms quadratic eq, checks if results exist_
		var b = f_x * d_x + f_y * d_y >> DecimalBits;
		if (b >= 0)
		{
			distance = new NFloat();
			return false;
		}

		// final check if the line and circle intersect in the given interval.
		var c = f_sqr - sqr(radius.value);
		if (d_sqr * c >= b*b)
		{
			distance = new NFloat();
			return false;
		}

		// line and circle intersect, just get the distance.
		var det = (d_y*circle_x.value - d_x*circle_y.value + to_x.value*from_y.value - to_y.value*from_x.value);
		if (det < 0) { det = -det; }

		distance.value = det / sqrt(d_sqr);
		return true;
	}

	/// <summary>
	/// Path obstructed that skips calculating actual distance.
	/// </summary>
	public static bool PathObstructed(NFloat from_x, NFloat from_y, NFloat to_x, NFloat to_y, NFloat circle_x, NFloat circle_y,
		NFloat radius)
	{
		var f_x = from_x.value - circle_x.value;
		var f_y = from_y.value - circle_y.value;
		var f_sqr = f_x*f_x + f_y*f_y >> DecimalBits;

		var d_x = to_x.value - from_x.value;
		var d_y = to_y.value - from_y.value;
		var d_sqr = d_x*d_x + d_y*d_y >> DecimalBits;
		if (d_sqr <= f_sqr)
		{
			return false;
		}

		// forms quadratic eq, checks if results exist_
		var b = f_x * d_x + f_y * d_y >> DecimalBits;
		if (b >= 0)
		{
			return false;
		}

		// final check if the line and circle intersect in the given interval.
		var c = f_sqr - (radius.value*radius.value >> DecimalBits);
		return (d_sqr * c < b*b);
	}

	public static NFloat LineDistance(NFloat from_x, NFloat from_y, NFloat to_x, NFloat to_y, NFloat circle_x, NFloat circle_y)
	{
		var d_x = to_x - from_x;
		var d_y = to_y - from_y;
		var d_sqr = (d_x*d_x + d_y*d_y).Sqrt();

		return MathNFloat.Abs(d_y*circle_x - d_x*circle_y + to_x*from_y - to_y*from_x) / d_sqr;
	}
}