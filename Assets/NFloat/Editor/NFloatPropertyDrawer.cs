using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

[CustomPropertyDrawer(typeof(NFloat))]
public class NFloatPropertyDrawer : PropertyDrawer
{
	/// <summary>
	/// Draws NFloat as a float field.
	/// </summary>
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		DrawNFloatField(position, property, label);
	}

	public static void DrawNFloatField(Rect position, SerializedProperty property, GUIContent label)
	{
		var rawValueProp = property.FindPropertyRelative("value");

		NFloat oldValue = rawValueProp.longValue.ToNFloatRaw();
		NFloat newValue = EditorGUI.FloatField(position, label, (float)Math.Round((float)oldValue, 4)).ToNFloat();

		if (newValue != oldValue)
		{
			// update value and mark target object as modified.
			rawValueProp.longValue = newValue.value;
			property.serializedObject.ApplyModifiedProperties();
			EditorUtility.SetDirty(property.serializedObject.targetObject);
		}
	}
}