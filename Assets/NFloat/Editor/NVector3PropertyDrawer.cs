using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

[CustomPropertyDrawer(typeof(NVector3))]
public class NVector3PropertyDrawer : PropertyDrawer
{
	/// <summary>
	/// Draws NFloat as a float field.
	/// </summary>
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var rawValueProp_x = property.FindPropertyRelative("x.value");
		var rawValueProp_y = property.FindPropertyRelative("y.value");
		var rawValueProp_z = property.FindPropertyRelative("z.value");

		var oldValue = new NVector3(rawValueProp_x.longValue.ToNFloatRaw(), rawValueProp_y.longValue.ToNFloatRaw(), rawValueProp_z.longValue.ToNFloatRaw());
		var oldValueFloat = new Vector3((float)Math.Round((float)oldValue.x, 4), (float)Math.Round((float)oldValue.y, 4), (float)Math.Round((float)oldValue.z, 4));
		var newValue = EditorGUI.Vector3Field(position, label, oldValueFloat).ToNVector3();

		if (newValue != oldValue)
		{
			// update value and mark target object as modified.
			rawValueProp_x.longValue = newValue.x.value;
			rawValueProp_y.longValue = newValue.y.value;
			rawValueProp_z.longValue = newValue.z.value;
			property.serializedObject.ApplyModifiedProperties();
			EditorUtility.SetDirty(property.serializedObject.targetObject);
		}
	}
}