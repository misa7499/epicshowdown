using GameConsole;

public static class NFloatUnitTests
{
	[ExecutableCommand]
	public static string PathObstruction_Linear()
	{
		return PathObstruction_Test
		(
			from_x: 0,
			from_y: 0,
			to_x: 5,
			to_y: 0,
			circle_x: 3,
			circle_y: 0,
			radius: 1
		);
	}

	[ExecutableCommand]
	public static string PathObstruction_Tangent()
	{
		return PathObstruction_Test
		(
			from_x: 0,
			from_y: 0,
			to_x: 5,
			to_y: 0,
			circle_x: 3,
			circle_y: 0.99f,
			radius: 1
		);
	}

	public static string PathObstruction_Test(NFloat from_x, NFloat from_y, NFloat to_x, NFloat to_y,
		NFloat circle_x, NFloat circle_y,	NFloat radius)
	{
		var isObstructed = MathNFloat2D.PathObstructed
		(
			from_x,
			from_y,
			to_x,
			to_y,
			circle_x,
			circle_y,
			radius,
			out NFloat dist
		);

		var isObstructed_noDist = MathNFloat2D.PathObstructed
		(
			from_x,
			from_y,
			to_x,
			to_y,
			circle_x,
			circle_y,
			radius
		);

		var lineDist = MathNFloat2D.LineDistance(
			from_x,
			from_y,
			to_x,
			to_y,
			circle_x,
			circle_y);

		return $"pathObstructed:{isObstructed} noDist:{isObstructed_noDist} lineDistance:{dist} {lineDist}";
	}
}