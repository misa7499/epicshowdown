using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

[CustomPropertyDrawer(typeof(NVector2))]
public class NVector2PropertyDrawer : PropertyDrawer
{
	/// <summary>
	/// Draws NFloat as a float field.
	/// </summary>
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var rawValueProp_x = property.FindPropertyRelative("x.value");
		var rawValueProp_y = property.FindPropertyRelative("y.value");

		NVector2 oldValue = new NVector2(rawValueProp_x.longValue.ToNFloatRaw(), rawValueProp_y.longValue.ToNFloatRaw());
		var oldValueFloat = new Vector2((float)Math.Round((float)oldValue.x, 4), (float)Math.Round((float)oldValue.y, 4));
		NVector2 newValue = EditorGUI.Vector2Field(position, label, oldValueFloat).ToNVector2();

		if (newValue != oldValue)
		{
			// update value and mark target object as modified.
			rawValueProp_x.longValue = newValue.x.value;
			rawValueProp_y.longValue = newValue.y.value;
			property.serializedObject.ApplyModifiedProperties();
			EditorUtility.SetDirty(property.serializedObject.targetObject);
		}
	}
}