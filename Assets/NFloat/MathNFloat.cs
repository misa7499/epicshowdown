﻿using System;

public static class MathNFloat
{
	#region Constants n stuff

	public static readonly NFloat PI = new NFloat(3.14159265359f);
	public static readonly NFloat TwoPI = new NFloat(6.28318530718f);
	public static readonly NFloat HalfPI = new NFloat(1.57079632679f);

	#endregion

	public static NFloat Max(NFloat x, NFloat y)
	{
		if (x.value > y.value)
		{
			return x;
		}

		return y;
	}

	public static NFloat Min(NFloat x, NFloat y)
	{
		if (x.value < y.value)
		{
			return x;
		}

		return y;
	}

	public static NFloat Abs(NFloat value)
	{
		if (value.value < 0)
		{
			return -value;
		}
		else
		{
			return value;
		}
	}

	public static NFloat Lerp(NFloat a, NFloat b, NFloat t)
	{
		return a + (b - a) * t;
	}

	public static int ToInt(this NFloat value)
	{
		return (int)value.Round();
	}

	public static NFloat Round(NFloat value)
	{
		return value.Round();
	}

	public static NFloat Clamp(NFloat value, NFloat min, NFloat max)
	{
		if (min > max) return Clamp(value, max, min);

		if (value < min)
		{
			value = min;
		}
		else if (value > max)
		{
			value = max;
		}

		return value;
	}

	public static NFloat Clamp01(NFloat value)
	{
		return Clamp(value, NFloat.Zero, NFloat.One);
	}

	public static int Sign(NFloat lhs)
	{
		if (lhs.value < 0)
			return -1;
		else if (lhs.value > 0)
			return 1;
		else
			return 0;
	}

	// ---- special implementation

	public static readonly NFloat Deg2Rad = new NFloat(0.01745329f);

	private static readonly NFloat sinCoeffA = new NFloat(1.27323954f);
	private static readonly NFloat sinCoeffB = new NFloat(0.405284735f);
	private static readonly NFloat sinCoeffC = new NFloat(0.225f);

	public static NFloat Sin(NFloat radian)
	{
		if (radian < 0)
		{
			return -Sin(-radian);
		}

		// fix for quadrants
		bool shouldNegate = false;

		// Make it between 0 and 2xPi;
		radian %= TwoPI;

		if (radian > PI)
		{
			shouldNegate = true;
			radian -= PI;
		}

		// Slow but more accurate
		var sin = sinCoeffA * radian - sinCoeffB * radian * radian;

		if (sin < 0)
			sin = sinCoeffC * (sin * -sin - sin) + sin;
		else
			sin = sinCoeffC * (sin * sin - sin) + sin;

		return shouldNegate ? -sin : sin;
	}

	public static NFloat Cos(NFloat radian)
	{
		return Sin(PI * NFloat.Half - radian);
	}

	public static NFloat Sqrt(NFloat value) => value.Sqrt();

	public static bool IsBetween(this NFloat a, NFloat b, NFloat c)
	{
		if (b.value < c.value && a.value >= b.value && a.value <= c.value) return true;
		if (c.value < b.value && a.value >= c.value && a.value <= b.value) return true;
		return false;
	}

	public static NFloat ApplyDrag(this NFloat lhs, NFloat relative, NFloat constant, NFloat dT)
	{
		lhs.value = (lhs.value * relative.value - constant.value * dT.value) >> NFloat.DecimalBits;
		return lhs.value < 0 ? new NFloat() : lhs;
	}
}