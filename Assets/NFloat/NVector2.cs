using System;
using UnityEngine;
using static NFloat;

[Serializable]
public struct NVector2
{
	[HideInInspector] public NFloat x;
	[HideInInspector] public NFloat y;

	public NVector2(NFloat x, NFloat y)
	{
		this.x = x;
		this.y = y;
	}

	public static readonly NVector2 zero = new NVector2(0, 0);
	public static readonly NVector2 right = new NVector2(1, 0);
	public static readonly NVector2 left = new NVector2(-1, 0);
	public static readonly NVector2 up = new NVector2(0, 1);
	public static readonly NVector2 down = new NVector2(0, -1);

	public static NVector2 FromTo(NVector2 lhs, NVector2 rhs)
	{
		var dist = NFloat.Distance(lhs.x, lhs.y, rhs.x, rhs.y).value;
		lhs.x.value = ((rhs.x.value - lhs.x.value) << DecimalBits) / dist;
		lhs.y.value = ((rhs.y.value - lhs.y.value) << DecimalBits) / dist;
		return lhs;
	}

	/* Operators */
	public static bool operator ==(in NVector2 lhs, in NVector2 rhs)
	{
		return lhs.x.value == rhs.x.value && lhs.y.value == rhs.y.value;
	}

	public static bool operator !=(in NVector2 lhs, in NVector2 rhs)
	{
		return lhs.x.value != rhs.x.value || lhs.y.value != rhs.y.value;
	}

	public static NVector2 operator +(NVector2 a, NVector2 b)
	{
		a.x.value += b.x.value;
		a.y.value += b.y.value;
		return a;
	}

	public static NVector2 operator /(NVector2 a, NFloat d)
	{
		a.x.value = (a.x.value << DecimalBits) / d.value;
		a.y.value = (a.y.value << DecimalBits) / d.value;
		return a;
	}

	public static NVector2 operator /(NVector2 a, int d)
	{
		a.x.value /= d;
		a.y.value /= d;
		return a;
	}

	public static NVector2 operator *(int d, NVector2 a)
	{
		a.x.value *= d;
		a.y.value *= d;
		return a;
	}

	public static NVector2 operator *(NVector2 a, int d)
	{
		a.x.value *= d;
		a.y.value *= d;
		return a;
	}

	public static NVector2 operator *(NFloat d, NVector2 a)
	{
		a.x.value = (a.x.value * d.value) >> DecimalBits;
		a.y.value = (a.y.value * d.value) >> DecimalBits;
		return a;
	}

	public static NVector2 operator *(NVector2 a, NFloat d)
	{
		a.x.value = (a.x.value * d.value) >> DecimalBits;
		a.y.value = (a.y.value * d.value) >> DecimalBits;
		return a;
	}

	public static NVector2 operator -(NVector2 a, NVector2 b)
	{
		a.x.value -= b.x.value;
		a.y.value -= b.y.value;
		return a;
	}

	public static NVector2 operator -(NVector2 a)
	{
		a.x.value = -a.x.value;
		a.y.value = -a.y.value;
		return a;
	}

	/* Math */
	public NFloat magnitude => NFloat.Distance(x, y);
	public NFloat sqrMagnitude => NFloat.SqrSum(x, y);

	public static NFloat Dot(in NVector2 lhs, in NVector2 rhs) => new NFloat()
	{
		value = (lhs.x.value * rhs.x.value + lhs.y.value * rhs.y.value) >> DecimalBits
	};

	public static NFloat Distance(in NVector2 a, in NVector2 b) => NFloat.Distance(a.x, a.y, b.x, b.y);
	public static NFloat SqrDistance(in NVector2 a, in NVector2 b) => NFloat.DistanceSqr(a.x, a.y, b.x, b.y);

	public NVector2 normalized
	{
		get
		{
			NFloat magnitude = NFloat.Distance(x, y);

			// check if zero length vector
			if (magnitude.value == 0)
				return new NVector2();

			return new NVector2()
			{
				x = new NFloat() { value = (x.value << DecimalBits) / magnitude.value },
				y = new NFloat() { value = (y.value << DecimalBits) / magnitude.value },
			};
		}
	}

	/* System.Object */
	public override string ToString() => $"[{x}, {y}]";

	public override bool Equals(object other)
	{
		if (!(other is NVector2))
		{
			return false;
		}
		NVector2 vector = (NVector2)other;
		return x == vector.x && y == vector.y;
	}

	public override int GetHashCode()
	{
		return x.GetHashCode() ^ y.GetHashCode() << 2;
	}
}

public static class NVector2Extensions
{
	public static NVector2 ToNVector2(this Vector2 vector)
	{
		return new NVector2((NFloat)vector.x, (NFloat)vector.y);
	}

	public static NVector2 ToNVector2(this Vector3 vector)
	{
		return new NVector2((NFloat)vector.x, (NFloat)vector.z);
	}
}