using System;
using UnityEngine;
using static FixedFloatMath;

/// <summary>
/// Use this to do performance sensitive math directly on longs.
/// </summary>
public static class FixedFloatMath
{
	public const int DecimalBits = NFloat.DecimalBits;

	public static long mul(long a, NFloat b) => (a * b.value) >> DecimalBits;
	public static long mul(NFloat a, long b) => (a.value * b) >> DecimalBits;
	public static long mul(long a, long b) => (a * b) >> DecimalBits;
	public static long mul(int a, long b) => (a * b);
	public static long mul(long a, int b) => (a * b);
	public static long mul(long a, long b, long c) => (((a * b) >> DecimalBits) * c) >> DecimalBits;

	public static long div(long a, long b) => (a << DecimalBits) / b;
	public static long div(int a, long b) => (((long)a) << DecimalBits*2) / b;
	public static long div(long a, int b) => a / b;

	public static long inverse(long a) => (1L << DecimalBits*2) / a;

	public static long sqr(long a) => (a * a) >> DecimalBits;
	public static long sqrSum(long a, long b) => (a * a + b * b) >> DecimalBits;

	public static long min(long a, long b) => a < b ? a : b;
	public static long max(long a, long b) => a > b ? a : b;
	public static long abs(long a) => a > 0 ? a : -a;
	public static int sign(long a) => a > 0 ? 1 : (a < 0 ? -1 : 0);

	public static float asFloat(long a) => a / (float)NFloat.RawOne;

	public static long sqrt(long value)
	{
		if (value < 0)
		{
			throw new InvalidOperationException(value.ToString());
		}

		ulong u = (ulong)value << DecimalBits;
		ulong c = 0x80000000;
		ulong g = 0x80000000;

		for (; ; )
		{
			if (g * g > u)
				g ^= c;
			c >>= 1;
			if (c == 0) return (long)g;
			g |= c;
		}
	}
}

#pragma warning disable

public struct long2
{
	public long x;
	public long y;

	public static bool operator ==(long2 a, long2 b) => a.x == b.x && a.y == b.y;
	public static bool operator !=(long2 a, long2 b) => a.x != b.x && a.y != b.y;

	public long magnitude
	{
		get
		{
			return sqrt( (x*x + y*y) >> DecimalBits);
		}
	}

	public static long2 operator +(long2 a, long2 b)
	{
		a.x += b.x;
		a.y += b.y;
		return a;
	}

	public static long2 operator -(long2 a, long2 b)
	{
		a.x -= b.x;
		a.y -= b.y;
		return a;
	}

	public static long2 operator *(long2 a, long b)
	{
		a.x = b * a.x >> DecimalBits;
		a.y = b * a.y >> DecimalBits;
		return a;
	}

	public static long2 operator *(long2 a, NFloat b)
	{
		a.x = b.value * a.x >> DecimalBits;
		a.y = b.value * a.y >> DecimalBits;
		return a;
	}

	public static long2 operator *(long b, long2 a)
	{
		a.x = b * a.x >> DecimalBits;
		a.y = b * a.y >> DecimalBits;
		return a;
	}

	public static long2 operator *(NFloat b, long2 a)
	{
		a.x = b.value * a.x >> DecimalBits;
		a.y = b.value * a.y >> DecimalBits;
		return a;
	}


	public static long2 operator /(long2 a, long b)
	{
		a.x = (a.x << DecimalBits) / b;
		a.y = (a.y << DecimalBits) / b;
		return a;
	}

	public static long2 operator -(long2 a)
	{
		a.x = -a.x;
		a.y = a.y;
		return a;
	}

	public static long2 operator +(long2 a)
	{
		return a;
	}

	public override string ToString() => $"{asFloat(x)} {asFloat(y)}";

	public static implicit operator long2(NVector2 lhs) => new long2()
	{
		x = lhs.x.value,
		y = lhs.y.value,
	};

	public static implicit operator long2(NVector3 lhs) => new long2()
	{
		x = lhs.x.value,
		y = lhs.y.value,
	};

	public static implicit operator NVector2(long2 lhs) => new NVector2()
	{
		x = new NFloat() { value = lhs.x },
		y = new NFloat() { value = lhs.y },
	};
}