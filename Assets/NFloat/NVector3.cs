using System;
using UnityEngine;
using static NFloat;

[Serializable]
public struct NVector3
{
	[HideInInspector] public NFloat x;
	[HideInInspector] public NFloat y;
	[HideInInspector] public NFloat z;

	public NVector3(NFloat x, NFloat y, NFloat z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static readonly NVector3 zero = new NVector3(0, 0, 0);
	public static readonly NVector3 right = new NVector3(1, 0, 0);
	public static readonly NVector3 left = new NVector3(-1, 0, 0);
	public static readonly NVector3 up = new NVector3(0, 1, 0);
	public static readonly NVector3 down = new NVector3(0, -1, 0);

	/* Operators */
	public static NVector3 operator +(NVector3 a, NVector3 b)
	{
		a.x.value += b.x.value;
		a.y.value += b.y.value;
		a.z.value += b.z.value;
		return a;
	}

	public static NVector3 operator +(NVector3 a, NVector2 b)
	{
		a.x.value += b.x.value;
		a.y.value += b.y.value;
		return a;
	}

	public static NVector3 operator +(NVector2 b, NVector3 a)
	{
		a.x.value += b.x.value;
		a.y.value += b.y.value;
		return a;
	}

	public static NVector3 operator /(NVector3 a, NFloat d)
	{
		a.x.value = (a.x.value << DecimalBits) / d.value;
		a.y.value = (a.y.value << DecimalBits) / d.value;
		a.z.value = (a.z.value << DecimalBits) / d.value;
		return a;
	}

	public static implicit operator NVector2(NVector3 lhs) => new NVector2()
	{
		x = lhs.x,
		y = lhs.y,
	};

	public static implicit operator NVector3(NVector2 lhs) => new NVector3()
	{
		x = lhs.x,
		y = lhs.y,
	};

	public static bool operator ==(NVector3 lhs, NVector3 rhs) =>
		lhs.x.value == rhs.x.value && lhs.y.value == rhs.y.value && lhs.z.value == rhs.z.value;

	public static bool operator !=(NVector3 lhs, NVector3 rhs) =>
		lhs.x.value != rhs.x.value || lhs.y.value != rhs.y.value || lhs.z.value != rhs.z.value;

	public static NVector3 operator *(NFloat d, NVector3 a)
	{
		a.x.value = (a.x.value * d.value) >> DecimalBits;
		a.y.value = (a.y.value * d.value) >> DecimalBits;
		a.z.value = (a.z.value * d.value) >> DecimalBits;
		return a;
	}

	public static NVector3 operator *(NVector3 a, NFloat d)
	{
		a.x.value = (a.x.value * d.value) >> DecimalBits;
		a.y.value = (a.y.value * d.value) >> DecimalBits;
		a.z.value = (a.z.value * d.value) >> DecimalBits;
		return a;
	}

	public static NVector3 operator -(NVector3 a, NVector3 b)
	{
		a.x.value = a.x.value - b.x.value;
		a.y.value = a.y.value - b.y.value;
		a.z.value = a.z.value - b.z.value;
		return a;
	}

	public static NVector3 operator -(NVector3 a)
	{
		a.x.value = -a.x.value;
		a.y.value = -a.y.value;
		a.z.value = -a.z.value;
		return a;
	}

	/* Math */
	public NFloat magnitude
	{
		get { return (x.Squared + y.Squared + z.Squared).Sqrt(); }
	}

	public NVector3 normalized
	{
		get
		{
			NFloat m = magnitude;
			if (m > Epsilon)
			{
				return new NVector3() { x = x / m, y = y / m, z = z / m };
			}
			else
			{
				return new NVector3();
			}
		}
	}

	public static NFloat Dot(in NVector3 lhs, in NVector3 rhs) => new NFloat()
	{
		value = (lhs.x.value * rhs.x.value + lhs.y.value * rhs.y.value + lhs.z.value * rhs.z.value) >> DecimalBits
	};

	public static NFloat Distance(in NVector3 a, in NVector3 b)
	{
		var x_dist = a.x.value - b.x.value;
		var y_dist = a.y.value - b.y.value;
		var z_dist = a.z.value - b.z.value;
		return new NFloat()
		{
			value = FixedFloatMath.sqrt((x_dist*x_dist + y_dist*y_dist + z_dist*z_dist) >> DecimalBits)
		};
	}

	public static NFloat SqrDistance(in NVector3 a, in NVector3 b)
	{
		var x_dist = a.x.value - b.x.value;
		var y_dist = a.y.value - b.y.value;
		var z_dist = a.z.value - b.z.value;
		return new NFloat()
		{
			value = (x_dist*x_dist + y_dist*y_dist + z_dist*z_dist) >> DecimalBits
		};
	}

	/* System.Object */
	public override string ToString()
	{
		return $"({x}, {y}, {z})";
	}

	public override bool Equals(object other)
	{
		return other is NVector3 vector &&
			x.value == vector.x.value && y.value == vector.y.value && z.value == vector.z.value;
	}

	public override int GetHashCode()
	{
		return x.GetHashCode() ^ y.GetHashCode() << 2;
	}
}

public static class NVector3Extensions
{
	public static NVector3 ToNVector3(this Vector3 vector) => new NVector3()
	{
		x = vector.x,
		y = vector.z,
		z = vector.y,
	};

	public static NVector3 ToNVector3(this Vector2 vector) => new NVector3()
	{
		x = vector.x,
		y = vector.y,
	};
}