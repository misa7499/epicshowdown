﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

[Serializable]
public partial struct NFloat
{
	#region Constants

	public const int DecimalBits = 20;
	public const int WholePartBits = 64 - DecimalBits;

	private const long DecimalPartMask = RawOne - 1;
	private const long WholePartMask = ~DecimalPartMask;

	public static readonly NFloat Zero = new NFloat();
	public static readonly NFloat MaxValue = new NFloat() { value = long.MaxValue };
	public static readonly NFloat MinValue = new NFloat() { value = long.MinValue };
	public static readonly NFloat PositiveInfinity = new NFloat() { value = long.MaxValue };

	public static readonly NFloat One = new NFloat() { value = RawOne };
	public static readonly NFloat Half = new NFloat() { value = RawOne >> 1 };
	public static readonly NFloat Quarter = new NFloat() { value = RawOne >> 2 };
	public static readonly NFloat Tenth = new NFloat() { value = RawOne / 10 };
	public static readonly NFloat Hunderth = new NFloat() { value = RawOne / 100 };
	public static readonly NFloat Tousandth = new NFloat() { value = RawOne / 1000 };
	public static readonly NFloat Epsilon = new NFloat() { value = 1L };

	public const double DecimalMultiplier = 1 << DecimalBits;
	public const long RawOne = 1 << DecimalBits;
	public const long RawZeroPointFive = RawOne >> 1;

	#endregion

	#region Fields

	[HideInInspector, SerializeField, FormerlySerializedAs("value")]
	public long value;

	#endregion

	#region Ctor

	public NFloat(double value)
	{
		this.value = (long)(value * DecimalMultiplier);
	}

	public NFloat(long value)
	{
		this.value = value;
	}

	#endregion

	#region Arithmetic Operations

	// Unary operations
	public static NFloat operator +(NFloat a)
	{
		return a;
	}

	public static NFloat operator -(NFloat a)
	{
		a.value = -a.value;
		return a;
	}

	public static NFloat operator +(NFloat a, NFloat b)
	{
		a.value += b.value;
		return a;
	}

	public static NFloat operator +(int a, NFloat b)
	{
		b.value += (long)a << DecimalBits;
		return b;
	}

	public static NFloat operator +(NFloat a, int b)
	{
		a.value = a.value + ((long)b << DecimalBits);
		return a;
	}

	public static NFloat operator -(NFloat a, NFloat b)
	{
		a.value = a.value - b.value;
		return a;
	}

	public static NFloat operator -(int a, NFloat b)
	{
		b.value = ((long)a << DecimalBits) - b.value;
		return b;
	}

	public static NFloat operator -(NFloat a, int b)
	{
		a.value = a.value - (b << DecimalBits);
		return a;
	}

	// Multiply
	public static NFloat operator *(NFloat a, NFloat b)
	{
		a.value = (a.value * b.value) >> DecimalBits;
		return a;
	}

	public static NFloat operator *(int b, NFloat a)
	{
		a.value *= b;
		return a;
	}

	public static NFloat operator *(NFloat a, int b)
	{
		a.value *= b;
		return a;
	}

	public static NFloat operator /(NFloat a, NFloat b)
	{
		if (b.value == 0)
		{
			throw new DivideByZeroException();
		}

		a.value = (a.value << DecimalBits) / b.value;
		return a;
	}

	public static NFloat operator /(NFloat a, int b)
	{
		if (b == 0)
		{
			throw new DivideByZeroException();
		}

		a.value /= b;
		return a;
	}

	public static NFloat operator %(NFloat a, NFloat b) => new NFloat()
	{
		value = a.value % b.value
	};

	#endregion

	#region Square Root

	public NFloat Squared => new NFloat()
	{
		value = (value * value) >> DecimalBits
	};

	public NFloat Inverse => new NFloat()
	{
		value = (NFloat.RawOne << DecimalBits) / value
	};

	public static NFloat SqrSum(NFloat dist_x, NFloat dist_y)
	{
		dist_x.value = (dist_x.value*dist_x.value + dist_y.value*dist_y.value) >> DecimalBits;
		return dist_x;
	}

	public NFloat Sqrt()
	{
		if (value < 0)
		{
			throw new InvalidOperationException();
		}

		long u = value << DecimalBits;

		long g = 0x80000000L;
		if (0x00800000L*0x00800000L < u)
		{
			if (g * g > u) g ^= 0xC0000000L; else g |= 0x40000000L;
			if (g * g > u) g ^= 0x60000000L; else g |= 0x20000000L;
			if (g * g > u) g ^= 0x30000000L; else g |= 0x10000000L;
			if (g * g > u) g ^= 0x18000000L; else g |= 0x08000000L;
			if (g * g > u) g ^= 0x0C000000L; else g |= 0x04000000L;
			if (g * g > u) g ^= 0x06000000L; else g |= 0x02000000L;
			if (g * g > u) g ^= 0x03000000L; else g |= 0x01000000L;
			if (g * g > u) g ^= 0x01800000L; else g |= 0x00800000L;
		}
		else
		{
			g = 0x00800000L;
		}

		if (g * g > u) g ^= 0x00C00000L; else g |= 0x00400000L;
		if (g * g > u) g ^= 0x00600000L; else g |= 0x00200000L;
		if (g * g > u) g ^= 0x00300000L; else g |= 0x00100000L;
		if (g * g > u) g ^= 0x00180000L; else g |= 0x00080000L;
		if (g * g > u) g ^= 0x000C0000L; else g |= 0x00040000L;
		if (g * g > u) g ^= 0x00060000L; else g |= 0x00020000L;
		if (g * g > u) g ^= 0x00030000L; else g |= 0x00010000L;
		if (g * g > u) g ^= 0x00018000L; else g |= 0x00008000L;

		if (g * g > u) g ^= 0x0000C000L; else g |= 0x00004000L;
		if (g * g > u) g ^= 0x00006000L; else g |= 0x00002000L;
		if (g * g > u) g ^= 0x00003000L; else g |= 0x00001000L;
		if (g * g > u) g ^= 0x00001800L; else g |= 0x00000800L;
		if (g * g > u) g ^= 0x00000C00L; else g |= 0x00000400L;
		if (g * g > u) g ^= 0x00000600L; else g |= 0x00000200L;
		if (g * g > u) g ^= 0x00000300L; else g |= 0x00000100L;
		if (g * g > u) g ^= 0x00000180L; else g |= 0x00000080L;

		if (g * g > u) g ^= 0x000000C0L; else g |= 0x00000040L;
		if (g * g > u) g ^= 0x00000060L; else g |= 0x00000020L;
		if (g * g > u) g ^= 0x00000030L; else g |= 0x00000010L;
		if (g * g > u) g ^= 0x00000018L; else g |= 0x00000008L;
		if (g * g > u) g ^= 0x0000000CL; else g |= 0x00000004L;
		if (g * g > u) g ^= 0x00000006L; else g |= 0x00000002L;
		if (g * g > u) g ^= 0x00000003L; else g |= 0x00000001L;
		if (g * g > u) g ^= 0x00000001L;

		return new NFloat() { value = g };
	}

	public static NFloat Distance(NFloat x1, NFloat y1, NFloat x2, NFloat y2)
	{
		var x_dist = x1.value - x2.value;
		var y_dist = y1.value - y2.value;

		long u = x_dist*x_dist + y_dist*y_dist;

		long g = 0x80000000L;
		if (0x00800000L*0x00800000L < u)
		{
			if (g * g > u) g ^= 0xC0000000L; else g |= 0x40000000L;
			if (g * g > u) g ^= 0x60000000L; else g |= 0x20000000L;
			if (g * g > u) g ^= 0x30000000L; else g |= 0x10000000L;
			if (g * g > u) g ^= 0x18000000L; else g |= 0x08000000L;
			if (g * g > u) g ^= 0x0C000000L; else g |= 0x04000000L;
			if (g * g > u) g ^= 0x06000000L; else g |= 0x02000000L;
			if (g * g > u) g ^= 0x03000000L; else g |= 0x01000000L;
			if (g * g > u) g ^= 0x01800000L; else g |= 0x00800000L;
		}
		else
		{
			g = 0x00800000L;
		}

		if (g * g > u) g ^= 0x00C00000L; else g |= 0x00400000L;
		if (g * g > u) g ^= 0x00600000L; else g |= 0x00200000L;
		if (g * g > u) g ^= 0x00300000L; else g |= 0x00100000L;
		if (g * g > u) g ^= 0x00180000L; else g |= 0x00080000L;
		if (g * g > u) g ^= 0x000C0000L; else g |= 0x00040000L;
		if (g * g > u) g ^= 0x00060000L; else g |= 0x00020000L;
		if (g * g > u) g ^= 0x00030000L; else g |= 0x00010000L;
		if (g * g > u) g ^= 0x00018000L; else g |= 0x00008000L;

		if (g * g > u) g ^= 0x0000C000L; else g |= 0x00004000L;
		if (g * g > u) g ^= 0x00006000L; else g |= 0x00002000L;
		if (g * g > u) g ^= 0x00003000L; else g |= 0x00001000L;
		if (g * g > u) g ^= 0x00001800L; else g |= 0x00000800L;
		if (g * g > u) g ^= 0x00000C00L; else g |= 0x00000400L;
		if (g * g > u) g ^= 0x00000600L; else g |= 0x00000200L;
		if (g * g > u) g ^= 0x00000300L; else g |= 0x00000100L;
		if (g * g > u) g ^= 0x00000180L; else g |= 0x00000080L;

		if (g * g > u) g ^= 0x000000C0L; else g |= 0x00000040L;
		if (g * g > u) g ^= 0x00000060L; else g |= 0x00000020L;
		if (g * g > u) g ^= 0x00000030L; else g |= 0x00000010L;
		if (g * g > u) g ^= 0x00000018L; else g |= 0x00000008L;
		if (g * g > u) g ^= 0x0000000CL; else g |= 0x00000004L;
		if (g * g > u) g ^= 0x00000006L; else g |= 0x00000002L;
		if (g * g > u) g ^= 0x00000003L; else g |= 0x00000001L;
		if (g * g > u) g ^= 0x00000001L;

		return new NFloat() { value = g };
	}

	public static NFloat Distance(NFloat x1, NFloat y1)
	{
		long u = x1.value*x1.value + y1.value*y1.value;

		long g = 0x80000000L;
		if (0x00800000L*0x00800000L < u)
		{
			if (g * g > u) g ^= 0xC0000000L; else g |= 0x40000000L;
			if (g * g > u) g ^= 0x60000000L; else g |= 0x20000000L;
			if (g * g > u) g ^= 0x30000000L; else g |= 0x10000000L;
			if (g * g > u) g ^= 0x18000000L; else g |= 0x08000000L;
			if (g * g > u) g ^= 0x0C000000L; else g |= 0x04000000L;
			if (g * g > u) g ^= 0x06000000L; else g |= 0x02000000L;
			if (g * g > u) g ^= 0x03000000L; else g |= 0x01000000L;
			if (g * g > u) g ^= 0x01800000L; else g |= 0x00800000L;
		}
		else
		{
			g = 0x00800000L;
		}

		if (g * g > u) g ^= 0x00C00000L; else g |= 0x00400000L;
		if (g * g > u) g ^= 0x00600000L; else g |= 0x00200000L;
		if (g * g > u) g ^= 0x00300000L; else g |= 0x00100000L;
		if (g * g > u) g ^= 0x00180000L; else g |= 0x00080000L;
		if (g * g > u) g ^= 0x000C0000L; else g |= 0x00040000L;
		if (g * g > u) g ^= 0x00060000L; else g |= 0x00020000L;
		if (g * g > u) g ^= 0x00030000L; else g |= 0x00010000L;
		if (g * g > u) g ^= 0x00018000L; else g |= 0x00008000L;

		if (g * g > u) g ^= 0x0000C000L; else g |= 0x00004000L;
		if (g * g > u) g ^= 0x00006000L; else g |= 0x00002000L;
		if (g * g > u) g ^= 0x00003000L; else g |= 0x00001000L;
		if (g * g > u) g ^= 0x00001800L; else g |= 0x00000800L;
		if (g * g > u) g ^= 0x00000C00L; else g |= 0x00000400L;
		if (g * g > u) g ^= 0x00000600L; else g |= 0x00000200L;
		if (g * g > u) g ^= 0x00000300L; else g |= 0x00000100L;
		if (g * g > u) g ^= 0x00000180L; else g |= 0x00000080L;

		if (g * g > u) g ^= 0x000000C0L; else g |= 0x00000040L;
		if (g * g > u) g ^= 0x00000060L; else g |= 0x00000020L;
		if (g * g > u) g ^= 0x00000030L; else g |= 0x00000010L;
		if (g * g > u) g ^= 0x00000018L; else g |= 0x00000008L;
		if (g * g > u) g ^= 0x0000000CL; else g |= 0x00000004L;
		if (g * g > u) g ^= 0x00000006L; else g |= 0x00000002L;
		if (g * g > u) g ^= 0x00000003L; else g |= 0x00000001L;
		if (g * g > u) g ^= 0x00000001L;

		return new NFloat() { value = g };
	}

	public static NFloat DistanceSqr(NFloat x1, NFloat y1, NFloat x2, NFloat y2)
	{
		var x_dist = x1.value - x2.value;
		var y_dist = y1.value - y2.value;
		x1.value = (x_dist*x_dist + y_dist*y_dist) >> DecimalBits;
		return x1;
	}

	#endregion

	#region Compare operations

	public static bool operator >(int a, NFloat b) => (a << DecimalBits) > b.value;
	public static bool operator <(int a, NFloat b) => (a << DecimalBits) < b.value;
	public static bool operator <=(int a, NFloat b) => (a << DecimalBits) <= b.value;
	public static bool operator >=(int a, NFloat b) => (a << DecimalBits) >= b.value;
	public static bool operator ==(int a, NFloat b) => (a << DecimalBits) == b.value;
	public static bool operator !=(int a, NFloat b) => (a << DecimalBits) != b.value;

	public static bool operator >(NFloat a, int b) => a.value > (b << DecimalBits);
	public static bool operator <(NFloat a, int b) => a.value < (b << DecimalBits);
	public static bool operator <=(NFloat a, int b) => a.value <= (b << DecimalBits);
	public static bool operator >=(NFloat a, int b) => a.value >= (b << DecimalBits);
	public static bool operator ==(NFloat a, int b) => a.value == (b << DecimalBits);
	public static bool operator !=(NFloat a, int b) => a.value != (b << DecimalBits);

	public static bool operator >(NFloat a, NFloat b) => a.value > b.value;
	public static bool operator <(NFloat a, NFloat b) => a.value < b.value;
	public static bool operator >=(NFloat a, NFloat b) => a.value >= b.value;
	public static bool operator <=(NFloat a, NFloat b) => a.value <= b.value;
	public static bool operator ==(NFloat a, NFloat b) => a.value == b.value;
	public static bool operator !=(NFloat a, NFloat b) => a.value != b.value;

	#endregion

	#region Conversions

	public static implicit operator NFloat(int value) => new NFloat()
	{
	 	value = value << DecimalBits
	};

	public static implicit operator NFloat(float value)
	{
		return new NFloat(value);
	}

	public static explicit operator float(NFloat value)
	{
		return value.value / (float)RawOne;
	}

	public static explicit operator long(NFloat value)
	{
		if (value.value >= 0)
		{
			return (value.value & WholePartMask) >> DecimalBits;
		}

		// do for positive and return multiplied with minus one
		return -((-value.value & WholePartMask) >> DecimalBits);
	}

	#endregion

	#region Methods

	public override string ToString()
	{
		return ((float)this).ToString();
	}

	public string ToString(string format)
	{
		return ((float)this).ToString(format);
	}

	public int ToInt()
	{
		return (int)(float)this;
	}

	public NFloat Floor()
	{
		if (value >= 0)
		{
			return new NFloat() { value = value & WholePartMask };
		}

		// negative value
		var wholePart = (-value) & WholePartMask;
		var decimalPart = (-value) & DecimalPartMask;

		if (decimalPart == 0)
		{
			return new NFloat(-wholePart);
		}
		else
		{
			return new NFloat(-wholePart - RawOne);
		}
	}

	public int FloorToInt()
	{
		if (value >= 0)
		{
			return (int)((value & WholePartMask) >> DecimalBits);
		}

		// negative value
		var wholePart = (-value) & WholePartMask;
		var decimalPart = (-value) & DecimalPartMask;

		if (decimalPart == 0)
		{
			return (int)((-wholePart) >> DecimalBits);
		}
		else
		{
			return (int)((-wholePart - RawOne) >> DecimalBits);
		}
	}

	public NFloat Ceiling()
	{
		var positiveRawValue = value > 0 ? value : -value;

		var resultRaw = positiveRawValue & WholePartMask;
		var decimalPart = positiveRawValue & DecimalPartMask;

		if (decimalPart != 0 && value > 0)
		{
			resultRaw += RawOne;
		}

		return new NFloat(value >= 0 ? resultRaw : -resultRaw);
	}

	public int CeilToInt()
	{
		var positiveRawValue = value > 0 ? value : -value;

		var resultRaw = positiveRawValue & WholePartMask;
		var decimalPart = positiveRawValue & DecimalPartMask;

		if (decimalPart != 0 && value > 0)
		{
			resultRaw += RawOne;
		}

		return value >= 0 ? (int)(resultRaw >> DecimalBits) : (int)((-resultRaw) >> DecimalBits);
	}

	public NFloat Round()
	{
		var rawPositiveValue = value > 0 ? value : -value;

		var decimalPart = rawPositiveValue & DecimalPartMask;
		var rawResult = rawPositiveValue & WholePartMask;

		// If decimal part is greater than 0.5 or exactly 0.5 and base is odd value -> round up
		if (decimalPart > RawZeroPointFive || (decimalPart == RawZeroPointFive && (rawResult & RawOne) != 0))
		{
			rawResult += RawOne;
		}

		return value >= 0 ? new NFloat(rawResult) : new NFloat(-rawResult);
	}

	public int RoundToInt()
	{
		return (int)Round();
	}

	#endregion

	#region Equality members

	public static bool IsNaN(NFloat value) => false;
	public static bool IsInfinity(NFloat value) => false;

	public bool Equals(NFloat other) => value == other.value;

	public override bool Equals(object obj) => obj is NFloat nFloat && Equals(nFloat);

	public override int GetHashCode() => (int)value;

	#endregion
}

public static class NFloatExtensions
{
	public static NFloat ToNFloat(this float value) => new NFloat(value);
	public static NFloat ToNFloat(this int value) => new NFloat() { value = value << NFloat.DecimalBits };
	public static NFloat ToNFloatRaw(this long value) => new NFloat() { value = value };
}