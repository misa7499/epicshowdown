using System;
/// <summary>
/// Immutable NRect
/// </summary>
public readonly struct NRect
{
	public readonly NFloat LeftEdge, RightEdge, BottomEdge, TopEdge;
	public readonly NVector2 Position;
	public readonly NFloat Width, Height;

	public NRect(NVector2 pos, NFloat width, NFloat height)
	{
		Position = pos;
		Width = width;
		Height = height;
		LeftEdge = pos.x - width * NFloat.Half;
		RightEdge = pos.x + width * NFloat.Half;
		TopEdge = pos.y - height * NFloat.Half;
		BottomEdge = pos.y + height * NFloat.Half;
	}

	public NRect(NFloat leftEdge, NFloat rightEdge, NFloat topEdge, NFloat bottomEdge)
	{
		LeftEdge = leftEdge;
		RightEdge = rightEdge;
		TopEdge = topEdge;
		BottomEdge = bottomEdge;
		Width = RightEdge - LeftEdge;
		Height = BottomEdge - TopEdge;
		Position.x = (LeftEdge + RightEdge) * NFloat.Half;
		Position.y = (TopEdge + BottomEdge) * NFloat.Half;
	}

	public void Clamp(ref NVector2 pos)
	{
		if (pos.x.value < LeftEdge.value) { pos.x.value = LeftEdge.value; }
		else if (pos.x.value > RightEdge.value) { pos.x.value = RightEdge.value; }

		if (pos.y.value < TopEdge.value) { pos.y.value = TopEdge.value; }
		else if (pos.y.value > BottomEdge.value) { pos.y.value = BottomEdge.value; }
	}

	public void Clamp(ref NVector2 pos, NFloat radius)
	{
		var leftEdge = LeftEdge.value + radius.value;
		if (pos.x.value < leftEdge) { pos.x.value = leftEdge; }
		else
		{
			var rightEdge = RightEdge.value - radius.value;
			if (pos.x.value > rightEdge) { pos.x.value = rightEdge; }
		}

		var topEdge = TopEdge.value + radius.value;
		if (pos.y.value < topEdge) { pos.y.value = topEdge; }
		else
		{
			var bottomEdge = BottomEdge.value - radius.value;
			if (pos.y.value > bottomEdge) { pos.y.value = bottomEdge; }
		}
	}

	public bool IsInside(in NVector2 pos, NFloat radius)
	{
		return pos.x.value >= LeftEdge.value + radius.value
			&& pos.x.value <= RightEdge.value - radius.value
			&& pos.y.value >= TopEdge.value + radius.value
			&& pos.y.value <= BottomEdge.value - radius.value;
	}

	public bool IsOutside(in NVector2 pos, NFloat radius)
	{
		return pos.x.value < LeftEdge.value + radius.value
			&& pos.x.value > RightEdge.value - radius.value
			&& pos.y.value < TopEdge.value + radius.value
			&& pos.y.value > BottomEdge.value - radius.value;
	}

	public bool IsInside(in NVector2 pos)
	{
		return pos.x.value >= LeftEdge.value
			&& pos.x.value <= RightEdge.value
			&& pos.y.value >= TopEdge.value
			&& pos.y.value <= BottomEdge.value;
	}

	public bool IsOutside(in NVector2 pos)
	{
		return pos.x.value < LeftEdge.value
			|| pos.x.value > RightEdge.value
			|| pos.y.value < TopEdge.value
			|| pos.y.value > BottomEdge.value;
	}

	public NRect Expand(NFloat expand) =>
		new NRect(Position, Width + expand * 2, Height + expand * 2);
}