using System;
using System.Collections.Generic;

public static class NFloatLinq
{
	public static NVector2 Average<TSource>(this IEnumerable<TSource> sources, Func<TSource, NVector2> selector)
	{
		int count = 0;
		var sum = new NVector2();

		foreach(var source in sources)
		{
			count++;
			sum += selector(source);
		}

		if (count > 0) sum /= count;
		return sum;
	}

	public static NFloat Average<TSource>(this IEnumerable<TSource> sources, Func<TSource, NFloat> selector)
	{
		int count = 0;
		var sum = new NFloat();

		foreach(var source in sources)
		{
			count++;
			sum += selector(source);
		}

		if (count > 0) sum /= count;
		return sum;
	}

	public static NFloat Min<TSource>(this IEnumerable<TSource> sources, Func<TSource, NFloat> selector)
	{
		var min = NFloat.MaxValue;

		foreach(var source in sources)
		{
			var candidate = selector(source);
			if (min.value > candidate.value)
				min.value = candidate.value;
		}

		return min;
	}

	public static NFloat Max<TSource>(this IEnumerable<TSource> sources, Func<TSource, NFloat> selector)
	{
		var max = NFloat.MinValue;

		foreach(var source in sources)
		{
			var candidate = selector(source);
			if (max.value < candidate.value)
				max.value = candidate.value;
		}

		return max;
	}
}