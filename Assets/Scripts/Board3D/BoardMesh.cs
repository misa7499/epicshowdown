﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// In charge of directing all visualisations of the terrain
/// </summary>
public class BoardMesh : MonoBehaviour
{
	int width, height;
	BoardArray<Transform> tiles3D;
	GameBoard board;

	public static BoardMesh Spawn(GameBoard board)
	{
		var boardMesh = new GameObject("GameBoard").AddComponent<BoardMesh>();
		boardMesh.Initialize(board);
		return boardMesh;
	}

	private void Initialize(GameBoard board)
	{
		this.board = board;
		width = board.Width;
		height = board.Height;
		tiles3D = new BoardArray<Transform>(width, height);

		for(int i = 0; i < width; i++)
			for(int j = 0; j < height; j++)
			{
				tiles3D[i, j] = SpawnTile3D(board.Tiles[i, j]);
			}

		Sync();
	}

	private Transform SpawnTile3D(Tile tile)
	{
		var newTile = GameObject.Instantiate(Resources.Load<Transform>("Board/Tile"));
		var id = (tile.x % 2 + (tile.y % 3) * 2) % 3;
		newTile.GetComponent<MeshRenderer>().sharedMaterial = Resources.Load<Material>("Board/Tile" + id);
		return newTile;
	}

	public void Sync()
	{
		for(int i = 0; i < width; i++)
			for(int j = 0; j < height; j++)
			{
				var tile = board.Tiles[i, j];
				var pos = tile.Position3D;
				pos.y = (float)tile.Height;
				tiles3D[i, j].localPosition = pos;
			}
	}

	public static Color Darken(Color c, float percent)
	{
		Color.RGBToHSV(c, out float h, out float s, out float v);
		v *= 1 - percent;
		return Color.HSVToRGB(h, s, v);
	}

	public BoardData SaveBoardData()
	{
		var newBoardData = new BoardData();
		newBoardData.Width = tiles3D.Width;
		newBoardData.Height = tiles3D.Height;

		newBoardData.Heights = new NFloat[tiles3D.Count];
		for(int i = 0; i < newBoardData.Heights.Length; i++)
			newBoardData.Heights[i] = tiles3D[i]?.transform.position.y ?? 0;

		return newBoardData;
	}
}
