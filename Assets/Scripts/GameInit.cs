﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class GameInit
{
	[RuntimeInitializeOnLoadMethod]
	public static void Init()
		=> Invoker.StartCoroutine(InitFlow(PlayArgs.Load()));

	/// <summary>
	/// Start game flow with the given play args.
	/// </summary>
	private static IEnumerator InitFlow(PlayArgs playArgs)
	{
		// load battle.
		yield return LoadBattle(playArgs.SelectedBattle);

		// start logging if needed.
		if (playArgs.LogBattleState)
			BattleLog.StartLogging(Game.Battle, BattleLoggerSettings.Instance);
	}

	/// <summary>
	/// Load battle with the given battle settings.
	/// </summary>
	private static IEnumerator LoadBattle(BattleSettings battleSettings)
	{
		// load battle scene
		yield return SceneManager.LoadSceneAsync("Battle", LoadSceneMode.Single);

		// initialze battle
		var battle = new BattleController(battleSettings);

		// async load battle UI
		yield return SceneManager.LoadSceneAsync("BattleUI", LoadSceneMode.Additive);

		// initialize UI controllers
		Game.BattleUI = GameObject.Find("BattleUI").GetComponent<BattleUI>();

		foreach(var uiController in GameObject.Find("BattleUI").GetComponentsInChildren<IBattleInitialization>())
		{
			uiController.OnBattleInitialized(battle);
		}

		// I was born to direct
		BattleDirector.StartBattle(battle);
	}
}