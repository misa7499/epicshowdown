using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Profiling.Profiler;

public class BattleController
{
	public event Action<BattleAction> OnActionApplied;
	public event Action<TimeTick> OnTimeTick;
	public event Action OnDispose;

	public readonly BattleSettings BattleSettings;
	public readonly BattleSimulator Simulator;
	public readonly BattleViewController ViewController;
	public readonly BoardMesh BoardMesh;
	public readonly OfflineBattleServer Server;
	public readonly GameCamera Camera;
	public bool IsFinished { get; private set; }

	public Nexus GetNexus(OwnerId owner) => Simulator.AllPlayers.Get(owner)?.Nexus;

	public BattleController(BattleSettings settings)
	{
		// 👌 View, Simulator, Server
		Game.Battle = this;
		BattleSettings = settings;
		ViewController = new BattleViewController();
		Server = new OfflineBattleServer(Game.dT);
		Simulator = new BattleSimulator(settings, ViewController, Server);

		// 🗺 board 3d mesh
		var board = Simulator.Board;
		BoardMesh = BoardMesh.Spawn(board);
		ViewController.Subscribe(ViewEvents.BoardChanged, x => BoardMesh.Sync());

		// 🎥 camera limits
		Camera = GameCamera.Spawn(CameraSettings.Instance);

		Camera.Position = board.Center3D;
		Camera.Position.z -= 3;

		Camera.LeftEdge = Camera.Position.x;
		Camera.RightEdge = Camera.Position.x;
		Camera.TopEdge = Camera.Position.z;
		Camera.BottomEdge = Camera.Position.z;
	}

	public void Update()
	{
		// check if update is valid
		if (IsFinished) { return; }

		// tick real time to server
		Server.TickTime(Time.deltaTime);

		// get deterministic queued actions from the server.
		foreach(var action in Server.GetReadyActions())
		{
			action.Apply(this);

			OnActionApplied.Dispatch(action);
			if (action is TimeTick timeTick) { OnTimeTick.Dispatch(timeTick); }
		}

		// update view stuff!
		BeginSample("ViewController.Update");
			ViewController.Update(Time.deltaTime);
		EndSample();
	}

	public void Dispose()
	{
		OnDispose.Dispatch();
	}
}