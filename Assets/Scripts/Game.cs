using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Static access to currently active battle instance.
/// Use BattleObject.Simulator or BattleObject3D.ViewController whenever possible.
/// </summary>
public static class Game
{
	public static readonly NFloat dT = 1 / (NFloat)64;

	public static BattleController Battle;
	public static BattleUI BattleUI;
	public static BattleSimulator Simulator => Battle?.Simulator;
	public static GameBoard Board => Battle?.Simulator.Board;
	public static BattleViewController ViewController => Battle?.ViewController;
	public static IBattleServer Server => Battle?.Server;
	public static Camera Camera3D => Game.Battle?.Camera.Camera;

	public static class Settings
	{
		public static readonly SettingsCache<UnitSettings> UnitSettings
			= new SettingsCache<UnitSettings>("Units/UnitSettings/{0}");

		public static readonly SettingsCache<BattleSettings> BattleSettings
			= new SettingsCache<BattleSettings>("Settings/BattleSettings/{0}");

		public static readonly SettingsCache<AIKernel> AIKernels
			= new SettingsCache<AIKernel>("Settings/AISettings/{0}");

		public static readonly UnitSettings[] AllUnitSettings = new UnitSettings[]
		{
			"Nexus",
			"Warrior",
			"Archer"
		};
	}
}