using UnityEngine;

/// <summary>
/// Converts 2D to 3D and vica versa.
/// </summary>
public static class PositionConverter
{
	/// <summary>
	/// Allows inverting or scaling x axis of the battlefield.
	/// </summary>
	public static float xConvert = 1;

	/// <summary>
	/// Allows inverting or scaling y axis of the battlefield.
	/// </summary>
	public static float yConvert = 1;

	public static Vector3 Position3D(this NVector3 pos) => new Vector3()
	{
		x = (float)pos.x * xConvert,
		y = (float)pos.z,
		z = (float)pos.y * yConvert
	};

	public static Vector3 Position3D(this NVector2 pos, float y = 0f) => new Vector3()
	{
		x = (float)pos.x * xConvert,
		y = (float)y,
		z = (float)pos.y * yConvert
	};

	public static Vector3 Direction3D(this NVector2 pos) => pos.x != 0 || pos.y != 0 ?
		new Vector3((float)pos.x * xConvert, 0f, (float)pos.y * yConvert) :
		new Vector3(0, 0, 1);

	public static Vector3 Direction3D(this NVector3 pos) => pos.x != 0 || pos.y != 0 || pos.z != 0 ?
		new Vector3((float)pos.x * xConvert, (float)pos.z, (float)pos.y * yConvert) :
		new Vector3(0, 0, 1);

	public static Vector3 Position3D(this Vector2 pos, float y = 0f) => new Vector3(pos.x * xConvert, y, pos.y * yConvert);
	public static Vector3 Direction3D(this Vector2 pos) => pos.x != 0 || pos.y != 0 ? new Vector3(pos.x * xConvert, 0f, pos.y * yConvert) : new Vector3(0, 0, 1);

	public static void SyncWith(this Transform transform, SimObject2D obj)
	{
		if (obj is Unit unit && unit.IsStructure)
		{
			transform.position = obj.Position3D();
		}
		else
		{
			transform.SetPositionAndRotation(obj.Position3D(), Quaternion.LookRotation(obj.Direction3D()));
		}
	}
}