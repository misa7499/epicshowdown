using UnityEngine;

public static class BattleObject3DExtensions
{
	public static Vector3 BodyCenter3D(this SimObject2D obj)
	{
		var pos = obj.Position3D();
		pos.y += obj.Height() * 0.5f;
		return pos;
	}
}