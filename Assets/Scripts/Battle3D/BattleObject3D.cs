using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class BattleObject3D : UIMonoBehaviour
{
	/* Serialized fields */
	public float EndAnimationDuration;
	public AnimatorAddon Animator;

	/* View Events */
	public ViewEventDispatcher ViewEventDispatcher;

	/* Data */
	public SimObject2D Data { get; private set; }
	public BattleViewController ViewController { get; private set; }
	public bool IsInitialized { get; private set; }

	private AddonList Addons;

	public virtual void Awake()
	{
		Addons = new AddonList();
		AddAddons(Addons);
		Addons.Initialize(this);
	}

	protected virtual void AddAddons(AddonList addons)
	{
		addons.Add(Animator);
	}

	public void Initialize(SimObject2D newData, BattleViewController controller)
	{
		IsInitialized = true;
		Data = newData;
		ViewController = controller;

		// initialize main object
		OnDataInitialized(newData);
		Addons.OnDataIntiialized(this, newData);

		// subscribe to view events.
		ViewEventDispatcher.Subscribe(ViewEvents.Message, evt => FloatingText.Show(this, evt.Data.ToString()));
	}

	public void Sync(float dT)
	{
		OnSync(dT);
		Addons.OnSync(this, dT);

		if (!Data.IsActive)
		{
			OnDeadSync(dT);
		}
	}

	public void Deactivate()
	{
		OnDeactivate();
		Addons.OnDeactivate(this);
		InvokeAfterSeconds(EndAnimationDuration, Dispose);
	}

	public void Dispose()
	{
		OnDispose();
		Addons?.OnDispose(this);
		ViewController?.Dispose(this);

		Data = null;
		IsInitialized = false;
		GameObject.Destroy(gameObject);
	}

	/* Events */
	protected virtual void OnSync(float dT) { transform.SyncWith(Data); }
	protected virtual void OnDeadSync(float dT) {}
	protected virtual void OnDeactivate() {}
	protected virtual void OnDispose() {}
	protected virtual void OnDataInitialized(SimObject2D data) {}

	public override void OnDestroy()
	{
		base.OnDestroy();
		Dispose();
	}
}

public class BattleObject3D<T> : BattleObject3D where T : SimObject2D
{
	/// <summary>
	/// Used to display data in the inspector.
	/// </summary>
	[SerializeField] private T serializedData;

	public new T Data => (T)base.Data;
	protected virtual void OnInitialized(T data) {}
	protected override sealed void OnDataInitialized(SimObject2D data) => OnInitialized((T)data);
}