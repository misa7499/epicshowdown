using System;
using System.Collections.Generic;
using UnityEngine;

public class BattleViewController : IViewBridge
{
	public ViewEventDispatcher ViewEventDispatcher;

	readonly List<ViewEvent> viewEvents = new List<ViewEvent>();
	readonly List<BattleObject3D> allObjects = new List<BattleObject3D>();
	readonly Dictionary<long, BattleObject3D> objectMap = new Dictionary<long, BattleObject3D>();
	public void SendViewEvent(ViewEvent evt) => viewEvents.Add(evt);

	public BattleObject3D GetView(SimObject obj)
	{
		if (obj == null) { return null; }
		objectMap.TryGetValue(obj.Id, out BattleObject3D obj3D);
		return obj3D;
	}

	public void Update(float dT)
	{
		/* invoke time */
		for(int i = 0; i < allObjects.Count; i++)
		{
			try { allObjects[i].Sync(dT); }
			catch(Exception e)
			{
				if (allObjects[i] == null)
				{
					allObjects.RemoveAt(i--);
					continue;
				}

				Debug.LogException(e);
			}
		}

		/* handle events */
		foreach(var evt in viewEvents)
		{
			try { OnEvent(evt); }
			catch(Exception e) { Debug.LogException(e); }
		}
		viewEvents.Clear();
	}

	public void OnEvent(ViewEvent evt)
	{
		// special event for object creating.
		if (evt.Type == ViewEvents.ObjectCreated) { OnObjectCreated(evt.Target); }

		// handle events
		var obj3D = GetView(evt.Target);
		if (obj3D != null) { obj3D.ViewEventDispatcher.Dispatch(evt); }

		// dispatch
		ViewEventDispatcher.Dispatch(evt);

		// special event for object deactivating.
		if (evt.Type == ViewEvents.ObjectDeactivated) { OnObjectDeactivated(evt.Target); }
	}

	/// <summary>
	/// Create new 3D representation of the given object.
	/// </summary>
	void OnObjectCreated(SimObject2D obj)
	{
		if (objectMap.ContainsKey(obj.Id)) { return; }
		var newObj3D = ViewPool.Fetch(obj);

		if (newObj3D == null)
		{
			throw new NullReferenceException($"Failed to fetch view for {obj} [name={obj?.Name} prefabPath={obj?.PrefabPath}]");
		}

		newObj3D.Initialize(obj, this);
		Add(obj, newObj3D);
		newObj3D.Sync(0);
	}

	/// <summary>
	/// Deactivate 3D representation of the given object.
	/// </summary>
	void OnObjectDeactivated(SimObject2D obj)
	{
		GetView(obj)?.Deactivate();
	}

	/// <summary>
	/// Adds object to global collection.
	/// </summary>
	public bool Add(SimObject data, BattleObject3D obj3D)
	{
		if (objectMap.ContainsKey(data.Id)) { return false; }
		objectMap.Add(data.Id, obj3D);
		allObjects.Add(obj3D);
		return true;
	}

	/// <summary>
	/// Removes 3d object from all collections on dispose.
	/// </summary>
	public void Dispose(BattleObject3D obj)
	{
		allObjects.Remove(obj);
		if (obj != null && obj.Data != null) { objectMap.Remove(obj.Data.Id); }
	}
}