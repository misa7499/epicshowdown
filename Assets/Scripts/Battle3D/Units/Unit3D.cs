using System;
using UnityEngine;
using UnityEngine.UI;

public class Unit3D : BattleObject3D<Unit>
{
	public UnitHealthBarAddon UIWidget;
	public Transform ProjectileFirePoint;

	protected override void AddAddons(AddonList addons)
	{
		base.AddAddons(addons);
		addons.Add(UIWidget);
	}

	protected override void OnDeadSync(float dT)
	{
		base.OnDeadSync(dT);

		Data.position += Data.velocity * dT;
		Data.velocity *= CollisionSettings.Instance.Drag;
	}
}