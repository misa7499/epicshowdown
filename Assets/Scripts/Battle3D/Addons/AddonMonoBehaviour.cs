using UnityEngine;

/// <summary>
/// MonoBehaviour implementation of battle addon, without sync method call each frame.
/// </summary>
public abstract class AddonMonoBehaviour : MonoBehaviour, IAddon
{
	public BattleObject3D parent;

	public void OnAwake(BattleObject3D parent)
	{
		this.parent = parent;
		OnInitialized(parent);
	}

	public virtual void OnDataInitialized(BattleObject3D parent, SimObject2D data) {}
	public virtual void OnDeactivate(BattleObject3D parent) {}
	public virtual void OnDispose(BattleObject3D parent) {}
	protected virtual void OnInitialized(BattleObject3D parent) {}
}

/// <summary>
/// MonoBehaviour implementation of battle addon with sync method call.
/// </summary>
public abstract class SyncAddonMonoBehaviour : AddonMonoBehaviour, ISyncAddon
{
	public virtual void OnSync(BattleObject3D parent, NFloat dT) {}
}