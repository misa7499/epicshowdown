using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface to battle addon without update each frame.
/// </summary>
public interface IAddon
{
	void OnAwake(BattleObject3D parent);
	void OnDataInitialized(BattleObject3D parent, SimObject2D data);
	void OnDeactivate(BattleObject3D parent);
	void OnDispose(BattleObject3D parent);
}

/// <summary>
/// Addon which wants to be updated each frame.
/// </summary>
public interface ISyncAddon : IAddon
{
	void OnSync(BattleObject3D parent, NFloat dT);
}