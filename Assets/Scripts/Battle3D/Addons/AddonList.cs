using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// List of BattleObject3D addons.
/// </summary>
public class AddonList
{
	readonly List<IAddon> allAddons = new List<IAddon>();
	readonly List<ISyncAddon> syncAddons = new List<ISyncAddon>();

	/// <summary>
	/// Add all addons before the initialization pass.
	/// </summary>
	public void Add(IAddon addon)
	{
		if (addon == null)
			throw new NullReferenceException();

		allAddons.Add(addon);

		// only behaviours that have some logic on each tick will be added to the sync list.
		if (addon is ISyncAddon syncBehaviour) { syncAddons.Add(syncBehaviour); }
	}

	/// <summary>
	/// Initialized all addons.
	/// </summary>
	public void Initialize(BattleObject3D obj)
	{
		// check if there are MonoBehaviours implementing sync interface.
		foreach (var component in obj.GetComponentsInChildren<IAddon>())
		{
			Add(component);
		}

		// initialize all addons in one pass.
		for (int i = 0; i < allAddons.Count; i++)
		{
			try { allAddons[i].OnAwake(obj); }
			catch (Exception e) { Debug.LogException(e); }
		}
	}

	internal void OnDataIntiialized(BattleObject3D parent, SimObject2D newData)
	{
		for (int i = 0; i < allAddons.Count; i++)
		{
			try { allAddons[i].OnDataInitialized(parent, newData); }
			catch (Exception e) { Debug.LogException(e); }
		}
	}

	public void OnSync(BattleObject3D obj, NFloat dT)
	{
		for (int i = 0; i < syncAddons.Count; i++)
		{
			try { syncAddons[i].OnSync(obj, dT); }
			catch (Exception e) { Debug.LogException(e); }
		}
	}

	public void OnDeactivate(BattleObject3D obj)
	{
		for(int i = 0; i < allAddons.Count; i++)
		{
			try { allAddons[i].OnDeactivate(obj); }
			catch(Exception e) { Debug.LogException(e); }
		}
	}

	public void OnDispose(BattleObject3D obj)
	{
		for (int i = 0; i < allAddons.Count; i++)
		{
			try { allAddons[i].OnDispose(obj); }
			catch (Exception e) { Debug.LogException(e); }
		}
	}
}