﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialForSide : AddonMonoBehaviour
{
    public Material Left;
    public Material Right;

    public override void OnDataInitialized(BattleObject3D parent, SimObject2D data)
    {
        GetComponent<Renderer>().sharedMaterial = data.Owner.IsOnTheLeft() ? Left : Right;
    }
}