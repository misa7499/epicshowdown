using System;
using UnityEngine;

/// <summary>
/// Plain class implementation of battle addon, without sync method call each frame.
/// </summary>
[Serializable]
public class Addon : IAddon
{
	[NonSerialized] public BattleObject3D parent;
	public SimObject2D Data => parent.Data;
	public BattlePlayer MyPlayer => Data.MyPlayer;

	public void OnAwake(BattleObject3D parent)
	{
		this.parent = parent;
		OnInitialized(parent);
	}

	public virtual void OnDataInitialized(BattleObject3D parent, SimObject2D data) {}
	public virtual void OnDeactivate(BattleObject3D parent) {}
	public virtual void OnDispose(BattleObject3D parent) {}
	protected virtual void OnInitialized(BattleObject3D parent) {}
}

/// <summary>
/// Plain class implementation of battle addon with sync method call.
/// </summary>
public abstract class SyncAddon : Addon, ISyncAddon
{
	public abstract void OnSync(BattleObject3D parent, NFloat dT);
}