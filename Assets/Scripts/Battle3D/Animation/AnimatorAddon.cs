using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class AnimatorAddon : Addon
{
	public Animator Animator;

	protected override void OnInitialized(BattleObject3D parent)
	{
		Animator = parent.GetComponentInChildren<Animator>();
		if (Animator == null) { return; }

		// subscribe to all triggers.
		foreach(var trigger in Animator.parameters.Where(a => a.type == AnimatorControllerParameterType.Trigger))
		{
			parent.Subscribe(trigger.nameHash, (evt) => Animator?.SetTrigger(trigger.nameHash));
		}
	}
}