using System.Collections.Generic;
using UnityEngine;

public static class ViewPool
{
	/// <summary>
	/// First key - folder, second key - name of the asset.
	/// Value: pools which have the reference to the prefab on the disk.
	/// </summary>
	static readonly Dictionary<string, Dictionary<string, Pool>> AllFolders = new Dictionary<string, Dictionary<string, Pool>>();

	/// <summary>
	/// Lookup on AllFolders with data in given BattleObject.
	/// </summary>
	public static Pool GetPool(string prefabPath, string name)
	{
		if (!AllFolders.TryGetValue(prefabPath, out Dictionary<string, Pool> prefabsInFolder))
		{
			prefabsInFolder = new Dictionary<string, Pool>();
			AllFolders.Add(prefabPath, prefabsInFolder);
		}

		if (!prefabsInFolder.TryGetValue(name, out Pool pool))
		{
			var prefab = Resources.Load<GameObject>(string.Format(prefabPath, name));
			if (prefab != null) { pool = new Pool(prefab); }
			prefabsInFolder.Add(name, pool);
		}

		return pool;
	}

	/// <summary>
	/// Lookup on AllFolders with data in given BattleObject.
	/// </summary>
	private static Pool GetPool(SimObject2D data)
	{
		return GetPool(data.PrefabPath, data.Name);
	}

	/// <summary>
	/// Gets prefab with lookup.
	/// </summary>
	public static GameObject LoadPrefab(string name, string prefabPath)
	{
		if (string.IsNullOrEmpty(prefabPath) || string.IsNullOrEmpty(name))
		{
			return null;
		}

		var pool = GetPool(prefabPath, name);
		return pool?.Prefab;
	}

	/// <summary>
	/// Gets prefab with lookup.
	/// </summary>
	public static GameObject LoadPrefab(SimObject2D data)
	{
		if (data == null || string.IsNullOrEmpty(data.PrefabPath) || string.IsNullOrEmpty(data.Name))
		{
			return null;
		}

		var pool = GetPool(data);
		return pool?.Prefab;
	}

	/// <summary>
	/// LoadPrefab() call which also returns an attached script, if such exists.
	/// </summary>
	public static T LoadPrefab<T>(SimObject2D data) where T : BattleObject3D
	{
		var prefab = LoadPrefab(data);
		return prefab?.GetComponent<T>();
	}

	/// <summary>
	/// LoadPrefab() call which also returns an attached script, if such exists.
	/// </summary>
	public static T LoadPrefab<T>(string name, string prefabPath) where T : BattleObject3D
	{
		var prefab = LoadPrefab(name, prefabPath);
		return prefab?.GetComponent<T>();
	}

	public static BattleObject3D Fetch(SimObject2D data)
	{
		if (string.IsNullOrEmpty(data.PrefabPath) || string.IsNullOrEmpty(data.Name))
		{
			return null;
		}

		var pool = GetPool(data);
		if (pool == null) { return null; }

		var view = pool.Fetch(data.Position3D()).GetComponent<BattleObject3D>();
		if (view == null) { return null; }

		return view;
	}

	public static void Clear()
	{
		AllFolders.Clear();
	}

	/// <summary>
	/// Simple pool for one prefab.
	/// </summary>
	public class Pool
	{
		private readonly Stack<GameObject> stack = new Stack<GameObject>();
		public readonly GameObject Prefab;

		/// <summary>
		/// Initializes prefab necessary for this PrefabPath.
		/// </summary>
		public Pool(GameObject prefab)
		{
			Prefab = prefab ?? throw new System.ArgumentNullException();
		}

		public GameObject Fetch(Vector3 position)
		{
			while (stack.Count > 0)
			{
				var fetched = stack.Pop();
				if (fetched == null) { continue; }
				fetched.transform.position = position;
				return fetched;
			}

			var newGameObject = (GameObject) Object.Instantiate(Prefab, position, Quaternion.identity);
			// newGameObject.AddComponentIfDoesntExist<VfxPoolingData>().parentPool = this;
			return newGameObject;
		}

		public void Release(GameObject released)
		{
			stack.Push(released);
		}
	}
}