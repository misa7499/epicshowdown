﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Profiling.Profiler;

public partial class BattleSimulator
{
	public readonly GameBoard Board;
	public readonly CollisionJob CollisionJob;
	public readonly SteeringJob SteeringJob;
	public readonly AggroJob AggroJob;
	public readonly IBattleServer Server;
	public readonly IViewBridge ViewBridge;
	public readonly BattleSettings BattleSettings;
	public readonly NRect PlayableArea;

	public readonly ArrayBuffer<SimObject2D> AllSimObjects = new ArrayBuffer<SimObject2D>();
	public readonly ArrayBuffer<Unit> AllStructures = new ArrayBuffer<Unit>();
	public readonly ArrayBuffer<Projectile> AllProjectiles = new ArrayBuffer<Projectile>();
	public readonly ArrayBuffer<Unit> AllUnits = new ArrayBuffer<Unit>();
	public readonly ArrayBuffer<BattlePlayer> AllPlayers = new ArrayBuffer<BattlePlayer>();

	public NFloat Time;
	public readonly IdGenerator IdGenerator = new IdGenerator(1);
	public readonly RandomnessProvider RandomnessProvider;

	public BattleSimulator(BattleSettings battleSettings, IViewBridge viewBridge, IBattleServer server)
	{
		Server = server;
		BattleSettings = battleSettings;
		ViewBridge = viewBridge;
		Board = new GameBoard(this, battleSettings);
		AggroJob = new AggroJob();
		SteeringJob = new SteeringJob();
		PlayableArea = new NRect(Board.LeftEdge, Board.RightEdge, Board.TopEdge, Board.BottomEdge);
		CollisionJob = new CollisionJob(CollisionSettings.Instance, PlayableArea);
		RandomnessProvider = new RandomnessProvider(battleSettings.GetInstanceID());

		// add player
		battleSettings.Players.Foreach(x => AddPlayer(x));

		// add scripted unit spawns
		foreach(var spawn in battleSettings.BoardSettings.UnitSpawns)
		{
			var tile = Board.Tiles[spawn.x, spawn.y];
			if (tile == null) { continue; }

			AddUnit(spawn.UnitSettings, tile.Position, spawn.Owner);
		}
	}

	public BattlePlayer AddPlayer(BattlePlayerData playerData)
	{
		// add player
		var player = new BattlePlayer(this, playerData);
		AllPlayers.Add(player);

		// add nexus
		var startTile = Board.Tiles[playerData.x, playerData.y];
		startTile.Owner = playerData.Owner;
		player.Nexus = (Nexus)AddUnit("Nexus", startTile.Position, player.Owner);

		return player;
	}

	public void AddUnits(UnitSettings settings, NVector2 position, OwnerId owner, int count)
	{
		if (count == 1)
		{
			AddUnit(settings, position, owner);
			return;
		}
		else
		{
			foreach(var pos in position.GetRadial(count, settings.Radius * 2, owner.IsOnTheLeft() ? 0 : 180))
				AddUnit(settings, pos, owner);
		}
	}

	public Unit AddUnit(UnitSettings settings, NVector2 position, OwnerId owner)
	{
		if (settings == null)
			throw new ArgumentNullException(nameof(settings));

		var newUnit = InstantiateNewUnit();
		newUnit.position = position;
		AllSimObjects.Add(newUnit);
		AllUnits.Add(newUnit);

		if (newUnit.IsStructure)
			AllStructures.Add(newUnit);

		newUnit.EventHappened(ViewEvents.ObjectCreated);
		return newUnit;

		Unit InstantiateNewUnit()
		{
			if (settings.Name == "Nexus")
				return new Nexus(settings, this, owner, Board.GetTile(position));
			if (settings.CollisionType == ColliderType.Hexagon)
				return new BoardStructureUnit(settings, this, owner, Board.GetTile(position));
			// default
				return new Minion(this, settings, owner);
		}
	}

	public Projectile FireProjectile(Unit parent, NFloat range, ProjectileSettings settings, TargetInfo target, Action<Projectile, TargetInfo> onReached)
	{
		var newProjectile = new Projectile(parent, range, settings, target, onReached);
		AllProjectiles.Add(newProjectile);
		AllSimObjects.Add(newProjectile);
		newProjectile.EventHappened(ViewEvents.ObjectCreated);
		return newProjectile;
	}

	public void OnTick(NFloat dT)
	{
		BeginSample("PreUpdate");
			AllSimObjects.PreUpdate();
		EndSample();

		BeginSample("AggroJob");
			AggroJob.Run(this);
		EndSample();

		BeginSample("OnUpdate");
			AllSimObjects.Update(dT);
		EndSample();

		BeginSample("SteeringJob.Run");
			SteeringJob.Run(this, dT);
		EndSample();

		BeginSample("LateUpdate");
			AllSimObjects.LateUpdate(dT);
		EndSample();

		// Collision!
		CollisionJob.Run(AllUnits, dT);

		// End of tick checks
		for(int i = 0; i < AllUnits.Count; i++)
		{
			var unit = AllUnits[i];

			// record diff.
			unit.lastPositionDiff = NVector2.Distance(unit.lastPosition, unit.position);

			// check if some units have been eliminated.
			if (unit.IsEliminated)
			{
				unit.Deactivate();
			}
		}

		// check if some objects was deactivated.
		for(int i = 0; i < AllSimObjects.Count; i++)
		{
			var obj = AllSimObjects[i];
			if (!obj.IsActive)
			{
				AllSimObjects.RemoveAt(i--);
				AllUnits.Remove(obj);
				AllStructures.Remove(obj);
				obj.EventHappened(ViewEvents.ObjectDeactivated);
				obj.OnDispose();
				continue;
			}
		}

		// Update clock
		Time += dT;
	}
}

public static class SimObjectLifecycleMethods
{
	public static void PreUpdate<T>(this ArrayBuffer<T> list) where T : SimObject
	{
		for (int i = 0; i < list.Count; i++)
		{
			try { list[i].PreUpdate(); }
			catch(Exception e) { list[i].LogException(e); }
		}
	}

	public static void Update<T>(this ArrayBuffer<T> list, NFloat dT) where T : SimObject
	{
		for (int i = 0; i < list.Count; i++)
		{
			try { list[i].Update(dT); }
			catch(Exception e) { list[i].LogException(e); }
		}
	}

	public static void LateUpdate<T>(this ArrayBuffer<T> list, NFloat dT) where T : SimObject
	{
		for (int i = 0; i < list.Count; i++)
		{
			try { list[i].LateUpdate(dT); }
			catch(Exception e) { list[i].LogException(e); }
		}
	}
}