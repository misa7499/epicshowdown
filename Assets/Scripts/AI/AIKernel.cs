using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class AIKernel : GameSettings
{
	public float CaptureEnemyTile = 500f;
	public float CaptureNeutralTile = 300f;
	public float CloseToEnemy = -25f;
	public float CoverUnprotectedTile = 200f;
	public float CoverProtectedTile = 50f;
	public float SaleUnitValue = 50;
	[Table] public List<PlaceUnitValue> PlaceUnitValues;

	public float ValueForPlacement(UnitSettings settings)
	{
		return PlaceUnitValues.FirstOrDefault(x => x.Settings == settings)?.Value ?? 0;
	}
}

[Serializable]
public class PlaceUnitValue
{
	public UnitSettings Settings;
	public float Value;
}