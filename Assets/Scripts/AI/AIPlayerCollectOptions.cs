using System;
using System.Collections.Generic;

public class AIPlayerOptionsCollector
{
	readonly List<AIPlayerOption> options = new List<AIPlayerOption>();
	readonly List<List<Tile>> tilePaths = new List<List<Tile>>();
	readonly AIPlayer aiPlayer;
	readonly BattlePlayer player;

	public AIPlayerOptionsCollector(AIPlayer aiPlayer)
	{
		this.aiPlayer = aiPlayer;
		this.player = aiPlayer.Player;
	}

	public List<AIPlayerOption> CollectOptions()
	{
		options.Clear();

		foreach(var settings in Game.Settings.AllUnitSettings)
			options.AddRange(CollectPlaceUnitOptions(settings));

		return options;
	}

	public IEnumerable<AIPlayerOption> CollectPlaceUnitOptions(UnitSettings settings)
	{
		// can I buy this at all?
		if (!player.Gold.CanAfford(settings.GoldCost))
		{
			yield break;
		}

		// flood algorithm to test all places where I can place my unit
		var possiblePlacementLocations = Tile.Flood
		(
			player.GetMyTilesList(),

			(from, to) => from.Owner == player.Owner
		);
	}

	/// <summary>
	/// On end of player turn, release acquired memory to pool for later use.
	/// </summary>
	public void FreeUpMemory()
	{
		foreach(var tilePath in tilePaths)
			player.Simulator.Board.Pathfinding.ReturnToPool(tilePath);

		tilePaths.Clear();
	}
}