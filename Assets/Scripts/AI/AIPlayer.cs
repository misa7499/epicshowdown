﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIPlayer
{
	public readonly BattleSimulator Simulator;
	public readonly BattlePlayer Player;
	public readonly AIPlayerOptionsCollector OptionsCollector;
	public readonly IBattleServer Server;
	public readonly AIKernel Kernel;
	public OwnerId Owner => Player.Owner;

	public AIPlayer(BattlePlayer player, AIKernel kernel)
	{
		Player = player;
		Simulator = player.Simulator;
		Server = player.Simulator.Server;
		Kernel = kernel;
		OptionsCollector = new AIPlayerOptionsCollector(this);
	}

	public IEnumerator PlayTurn()
	{
		while(true)
		{
			// find best options
			var bestOption = OptionsCollector.CollectOptions().MaxBy(
				evaluation: opt => Evaluate(opt) * UnityEngine.Random.Range(0.99f, 1.01f),
				minMaxValue: 0
			);

			// execute if defined.
			var action = bestOption?.Schedule(Server);
			if (action != null)
				yield return action;

			// options collector can be very memory heavy, some custom managment
			OptionsCollector.FreeUpMemory();

			// if nothing was found or something misfired, just end the turn
			if (action == null)
				break;
		}
	}

	public float Evaluate(AIPlayerOption option)
	{
		switch(option)
		{
			case PlaceUnitOption placeUnit:
				return 0;
			case MoveUnitOption moveUnit:
				return 0;
			default:
				return -1;
		}
	}
}