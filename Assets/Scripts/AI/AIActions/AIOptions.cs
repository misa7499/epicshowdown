public interface AIPlayerOption
{
	BattleAction Schedule(IBattleServer server);
}

public class MoveUnitOption : AIPlayerOption
{
	public Unit unit;
	public Tile tile;

	public MoveUnitOption(Unit unit, Tile tile)
	{
		this.unit = unit;
		this.tile = tile;
	}

	public BattleAction Schedule(IBattleServer server)
	{
		return null;
		//return server.ScheduleAction(unit.MoveAction, unit.Tile.FindPath(tile, unit));
	}
}

public class PlaceUnitOption : AIPlayerOption
{
	public UnitSettings settings;
	public Tile tile;
	private BattlePlayer player;

	public PlaceUnitOption(BattlePlayer player, Tile tile, UnitSettings settings)
	{
		this.player = player;
		this.tile = tile;
		this.settings = settings;
	}

	public BattleAction Schedule(IBattleServer server)
	{
		return null;// server.ScheduleAction(player.PlaceUnit, (tile, settings));
	}
}