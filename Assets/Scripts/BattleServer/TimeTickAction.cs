public class TimeTick : BattleAction
{
	public readonly NFloat dT;
	public readonly NFloat CurrentTime;

	public TimeTick(NFloat duration, NFloat currentTime) : base(ActionId.Time)
	{
		dT = duration;
		CurrentTime = currentTime;
	}

	protected override void OnApply(BattleController battle)
	{
		battle.Simulator.OnTick(dT);
	}
}