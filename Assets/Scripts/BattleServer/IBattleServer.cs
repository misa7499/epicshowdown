using System.Collections.Generic;

public interface IBattleServer
{
	T ScheduleAction<T>(T action) where T : BattleAction;
	void TickTime(float dT);
	BattleAction NextAction();
}

public static class ServerUtils
{
	/// <summary>
	/// Helper method - gets all ready battle actions in queue.
	/// </summary>
	public static IEnumerable<BattleAction> GetReadyActions(this IBattleServer server)
	{
		for(var action = server.NextAction(); action != null; action = server.NextAction())
			yield return action;
	}
}