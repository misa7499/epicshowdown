using System.Collections.Generic;

public class OfflineBattleServer : IBattleServer
{
	public readonly NFloat TickDuration;

	public int TickCount { get; private set; }
	public NFloat CurrentTime { get; private set; }

	readonly Queue<BattleAction> queuedActions;

	public OfflineBattleServer(NFloat tickDuration)
	{
		TickDuration = tickDuration;
		queuedActions = new Queue<BattleAction>();
	}

	public T ScheduleAction<T>(T action) where T : BattleAction
	{
		queuedActions.Enqueue(action);
		return action;
	}

	public void TickTime(float dT)
	{
		accumulatedTime += dT;
		while (accumulatedTime > TickDuration)
		{
			queuedActions.Enqueue(new TimeTick(TickDuration, CurrentTime));
			CurrentTime += TickDuration;
			TickCount++;

			accumulatedTime -= (float)TickDuration;
		}
	}
	float accumulatedTime;

	public BattleAction NextAction()
	{
		return queuedActions.Count > 0 ? queuedActions.Dequeue() : null;
	}
}