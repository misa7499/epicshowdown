using UnityEngine.UI;

public class WalletUIController : UIRenderer<BattlePlayer>, IBattleInitialization
{
	public Text GoldAmount;

	public void OnBattleInitialized(BattleController battle)
	{
		Data = Game.BattleUI.DisplayedPlayer;
	}

	protected override void OnDataChanged(BattlePlayer newValue)
	{
		if (newValue != null)
		{
			UpdateGoldAmount();
			newValue.Gold.OnAmountChanged += UpdateGoldAmount;
		}
	}

	public void UpdateGoldAmount()
	{
		GoldAmount.text = $"Gold: {Data.Gold.Amount.ToStringLookup()}";
	}
}