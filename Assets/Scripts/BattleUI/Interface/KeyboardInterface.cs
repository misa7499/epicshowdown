using UnityEngine;

public static class KeyboardInterface
{
	public static void ProcessInput()
	{
		if (Input.GetKeyDown(KeyCode.Tab))
			BattleCommands.SpawnAll();

		if (Input.GetKeyDown(KeyCode.Escape))
			BattleCommands.ExitBattle();
	}
}