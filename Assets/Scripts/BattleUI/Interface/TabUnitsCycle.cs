using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TabUnitsCycle
{
	public readonly BattlePlayer Player;
	public TabUnitsCycle(BattlePlayer player) => Player = player;

	/* Tab cycle state */
	readonly List<Unit> tabCycle = new List<Unit>();
	int tabIndex = -1;

	public Unit GetNextUnit()
	{
		// method for finding new unit to put into tab cycle
		Unit FindNewUnit() =>
				Player.Simulator.AllUnits.
				Except(tabCycle).
				Where(x => x.Owner == Player.Owner).
					OrderByDescending(x => x.Settings.GoldCost).
					ThenBy(x => x.DistanceTo(Game.Battle.Camera.Position)).
				FirstOrDefault();

		// try add new unit to tab cycle.
		if (tabIndex == tabCycle.Count - 1 && FindNewUnit() is Unit nextUnit)
		{
			tabIndex = tabCycle.Count;
			tabCycle.Add(nextUnit);
			return nextUnit;
		}

		// nothing new found, cycle through the list.
		if (tabCycle.Count > 0)
		{
			tabIndex = (tabIndex + 1) % tabCycle.Count;
			return tabCycle[tabIndex];
		}

		// in case it is empty.
		return null;
	}
}