using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour
{
	public Text messageLabel;
	public Transform target;
	public Vector3 targetPosition;
	public float Height;

	[Header("Animations")]
	public AnimationCurve Position;
	public AnimationCurve Alpha;
	private float animationStartTime;

	public static void Show(Vector3 targetPosition, string message, float height = 0)
		=> Info(null, targetPosition, message, height);

	public static void Show(Transform transform, string message, float height = 0)
		=> Info(transform, transform.position, message, height);

	public static void Show(BattleObject3D target, string message)
		=> Info(target.transform, target.transform.position, message, target.Data.Height());

	/// <summary>
	/// Create new message animation.
	/// </summary>
	public static void Info(Transform target, Vector3 targetPosition, string message, float height = 0)
	{
		var instance = GameObject.Instantiate<FloatingText>(UISettings.Instance.FloatingText, HUDController.Instance.transform);

		/* prepare for animation */
		instance.target = target;
		instance.Height = height;
		instance.targetPosition = targetPosition;
		instance.messageLabel.text = message;
		instance.messageLabel.SetAlpha(0f);
		instance.animationStartTime = Time.realtimeSinceStartup;

		/* animate alpha and disappear */
		instance.Animate
		(
			action: x => instance.messageLabel.SetAlpha(x),
			curve: instance.Alpha,
			callback: () => Destroy(instance.gameObject)
		);
	}

	/// <summary>
	/// Match UI object screen position to the 3d position of the tracked object.
	/// </summary>
	void LateUpdate()
	{
		var parent = messageLabel.rectTransform.parent as RectTransform;
		if (parent == null)
			return;

		if (target != null)
			targetPosition = target.position;

		// convert 3D position to viewportPosition.
		var viewportPosition = Game.Camera3D.WorldToViewportPoint(new Vector3()
		{
			x = targetPosition.x,
			y = targetPosition.y + Height,
			z = targetPosition.z,
		});

		// animate Y.
		var animationOffset = new Vector2()
		{
			x = 0,
			y = Position.Evaluate(Time.realtimeSinceStartup - animationStartTime)
		};

		messageLabel.rectTransform.localPosition =
			parent.ViewportToLocal(viewportPosition) + animationOffset;
	}
}

public static class UnityUILib
{
	/// <summary>
	/// Convert from viewport position to local position of a RectTransform.
	/// </summary>
	public static Vector2 ViewportToLocal(this RectTransform transform, Vector2 viewport)
	{
		var sizeDelta = transform.sizeDelta;
		return new Vector2(viewport.x*sizeDelta.x - sizeDelta.x*0.5f, viewport.y*sizeDelta.y - sizeDelta.y*0.5f);
	}
}