using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class UnitHealthBarAddon : SyncAddon
{
	public bool HealthBarEnabled = true;

	private ScreenSpaceBillboard uiInstance;
	private Image uiGraphic;

	public override void OnSync(BattleObject3D parent, NFloat dT)
	{
		// maybe we disabled this?
		if (!HealthBarEnabled)
		{
			if (uiInstance != null) { GameObject.Destroy(uiInstance.gameObject); }
			return;
		}

		// create if needed.
		if (uiInstance == null)
		{
			if (HUDController.Instance == null) { return; }
			uiInstance = HUDController.Spawn("UI/HUD/UnitEmblem", parent);
			uiGraphic = uiInstance.GetComponent<Image>();
		}

		uiInstance.Height = parent.Data.Height();
		uiInstance.Offset2D = 0.25f;

		if (uiGraphic != null && parent.Data is Unit unit)
		{
			NormalState(unit);
		}
	}

	public void NormalState(Unit unit)
	{
		uiGraphic.sprite = UISettings.Instance.NormalStateSprite;
		uiGraphic.color = MyPlayer?.Data.UIColor ?? Color.white;
		uiGraphic.fillAmount = (float)unit.CurrentHealthPercent;
	}

	public override void OnDispose(BattleObject3D parent)
	{
		if (uiInstance != null)
		{
			uiInstance.gameObject.SetActive(false);
		}
	}
}