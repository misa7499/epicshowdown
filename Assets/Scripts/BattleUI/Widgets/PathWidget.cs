﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathWidget : MonoSingleton<PathWidget>
{
    public LineRenderer Line;

    public void Show(IList<Tile> tiles)
    {
        if (tiles.IsEmpty())
        {
            Hide();
            return;
        }
        else
        {
            Line.enabled = true;
            Line.positionCount = tiles.Count;
            for(int i = 0; i < tiles.Count; i++)
                Line.SetPosition(i, tiles[i].Position3D);
        }
    }

    public void Hide()
    {
        Line.enabled = false;
    }
}
