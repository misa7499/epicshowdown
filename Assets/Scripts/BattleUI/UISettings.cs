using UnityEngine;

public class UISettings : ScriptableObject<UISettings>
{
	public Sprite NormalStateSprite;
	public Sprite OutOfMovesSprite;
	public FloatingText FloatingText;
}