using UnityEngine;

public static class GameCameraFlyAnimations
{
	static Tile animatingTo;
	static NAnimation animation;

	public static void FlyTo(this GameCamera camera, Vector3 position)
	{
		camera.FlyTo(Game.Board.GetTile(position.ToNVector2()));
	}

	public static void FlyTo(this GameCamera camera, Tile tile)
	{
		if (animatingTo == tile || tile == null)
			return;

		// stop previous animation if needed.
		animation?.Stop(skipCallbacks: true);
		animatingTo	= tile;

		// set up targets
		var startPosition = camera.Position;
		var endPosition = tile.Position3D;

		// animate
		animation = NAnimator.Animate
		(
			action: (x) => camera.Position = Vector3.Lerp(startPosition, endPosition, x),
			easing: EasingFunctions.EaseOutSine,
			duration: 0.5f,
			callback: () =>
			{
				animatingTo = null;
				animation = null;
			}
		);
	}
}