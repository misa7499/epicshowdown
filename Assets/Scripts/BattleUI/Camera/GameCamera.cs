﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using static Game;

public class GameCamera : CameraController<GameCamera>
{
	public override void OnGroundClick(PointerEventData evt, Vector3 groundPoint)
	{
		var tile = Game.Board.GetTile(groundPoint.ToNVector2());
		if (tile == null)
		{
			return;
		}

		if (evt.button == PointerEventData.InputButton.Left)
		{
			Game.BattleUI.OnLeftMouseBtnClick(tile);
			return;
		}

		if (evt.button == PointerEventData.InputButton.Right)
		{
			Game.BattleUI.OnRightMouseBtnClick(tile);
			return;
		}
	}

	public override void Drag(PointerEventData evt)
	{
		var startTile = GetTile(evt);
		var endTile = Game.Simulator.AllPlayers.Get(OwnerId.Player1).Nexus.Tile;
		if (startTile == null || endTile == null)
			return;

		var path = Game.Simulator.Board.Pathfinding.FindPath(startTile, endTile, Game.Simulator.Board.GamePathfinder);
		PathWidget.Instance.Show(path);
	}

	public override void LateUpdate()
	{
		base.LateUpdate();
		KeyboardInterface.ProcessInput();
		HUDController.Instance.SetScale(Settings.StartDistance / Distance);
	}

	public Tile GetTile(PointerEventData data) =>
		Camera.GetPoint(data.position, out Vector3 position3D) ? Simulator.Board.GetTile(position3D.ToNVector2()) : null;
}