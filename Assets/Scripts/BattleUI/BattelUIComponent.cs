using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class BattleUIComponent : UIMonoBehaviour, IBattleInitialization
{
	public BattleController Battle { get; private set; }

	void IBattleInitialization.OnBattleInitialized(BattleController battle)
	{
		Battle = battle;

		try { OnBattleInitialized(battle); }
		catch(Exception e) { Debug.LogException(e); }
	}

	protected abstract void OnBattleInitialized(BattleController battle);
}

/// <summary>
/// Implement this to getcallback when battle gets initialized.
/// </summary>
public interface IBattleInitialization
{
	void OnBattleInitialized(BattleController battle);
}