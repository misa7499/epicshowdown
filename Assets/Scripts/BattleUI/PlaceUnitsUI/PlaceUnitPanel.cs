﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlaceUnitPanel : UIRenderer<BattlePlayer>
{
	public UIList CardList;
	public Selectable<BattleCardRenderer> SelectedCard = new Selectable<BattleCardRenderer>
	(
		onSelect: (card) => card.Dispatch(UIEvents.OnSelect),
		onDeselect: (card) => card.Dispatch(UIEvents.OnDeselect)
	);

	protected override void OnDataChanged(BattlePlayer player)
	{
		CardList.DataProvider = player.Deck.Cards;
		CardList.Subscribe(UIListEvents.ItemClicked, OnCardClicked);
	}

	private void OnCardClicked(EventData evtData)
	{
		if (evtData.Data is BattleCardRenderer cardRenderer)
			SelectedCard.Value = cardRenderer;
	}

	public void OnTileClick(Tile tile)
	{
		if (SelectedCard.Value?.Data is CardInfo card)
		{
			var castData = new CastData(tile);
			if (card.Validate(castData))
			{
				card.OnPlayed(castData);
			}
		}
	}
}