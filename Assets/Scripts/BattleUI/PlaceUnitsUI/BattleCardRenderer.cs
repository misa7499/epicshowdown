﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BattleCardRenderer : UIRenderer<CardInfo>, IPointerClickHandler
{
    public Image Image;
    public Text UnitName;

	void Start()
	{
		Subscribe(UIEvents.OnSelect, OnSelect);
		Subscribe(UIEvents.OnDeselect, OnDeselect);
	}

	private void OnDeselect()
	{
		transform.localScale = Vector3.one;
	}

	private void OnSelect()
	{
		transform.localScale = Vector3.one * 1.15f;
	}

	protected override void OnDataChanged(CardInfo newValue)
	{
        UnitName.text = newValue.Name;
	}
}