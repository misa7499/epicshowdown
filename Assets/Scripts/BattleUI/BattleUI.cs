using System;
using UnityEngine;
using UnityEngine.UI;

public class BattleUI : BattleUIComponent
{
	public BattlePlayer DisplayedPlayer;

	public HeaderUIController Header;
	public PlaceUnitPanel LeftPlayerController;
	public PlaceUnitPanel RightPlayerController;

	protected override void OnBattleInitialized(BattleController battle)
	{
		DisplayedPlayer = battle.Simulator.AllPlayers.Get(OwnerId.Player1);
		LeftPlayerController.Data = battle.Simulator.AllPlayers.Get(OwnerId.Player1);
		RightPlayerController.Data = battle.Simulator.AllPlayers.Get(OwnerId.Player2);

		if (GetComponent<Canvas>() is Canvas canvas)
		{
			canvas.renderMode = RenderMode.ScreenSpaceCamera;
			canvas.worldCamera = battle.Camera.Camera;
			canvas.planeDistance = 1;
		}
	}

	public void OnLeftMouseBtnClick(Tile tile)
	{
		LeftPlayerController.OnTileClick(tile);
	}

	public void OnRightMouseBtnClick(Tile tile)
	{
		RightPlayerController.OnTileClick(tile);
	}
}