using UnityEngine;

/// <summary>
/// Data provided when starting the game.
/// </summary>
[System.Serializable]
public class PlayArgs
{
	/// <summary>
	/// Which battle to start on game load?
	/// </summary>
	public string SelectedBattle = "DefaultBattleSettings";

	/// <summary>
	/// Should battle state be logged?
	/// </summary>
	public bool LogBattleState;

	public static void Set(PlayArgs runArgs)
	{
		var savedJson = runArgs != null ? JsonUtility.ToJson(runArgs) : "";
		PlayerPrefs.SetString("RunArgsProvider_RunArgs", savedJson);
	}

	public static PlayArgs Load()
	{
		var loadedArgs = PlayerPrefs.GetString("RunArgsProvider_RunArgs");
		var runArgs = JsonUtility.FromJson<PlayArgs>(loadedArgs) ?? new PlayArgs();
		PlayerPrefs.DeleteKey("RunArgsProvider_RunArgs");
		return runArgs;
	}
}