﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using GameConsole;
using UnityEngine;
using UnityEngine.SceneManagement;
using static SceneLoaderHelper;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public static class SceneLoadCommands
{
	public static void RunFromBattleScene(PlayArgs runArgs)
	{
		PlayArgs.Set(runArgs);
		RunFromBattleScene();
	}

	[ExecutableCommand]
#if UNITY_EDITOR
	[MenuItem("Scenes/Run", false, -999)]
#endif
	public static void RunFromBattleScene()
	{
		RunScene("Battle");
	}

	[ExecutableCommand]
#if UNITY_EDITOR
	[MenuItem("Scenes/Battle", false, 1)]
#endif
	public static void LoadBattle()
	{
		LoadScene("Battle");
	}

	[ExecutableCommand]
#if UNITY_EDITOR
	[MenuItem("Scenes/BattleUI", false, 2)]
#endif
	public static void LoadBattleUI()
	{
		LoadScene("BattleUI");
	}
}