using System.Collections;
using UnityEngine;

public static class BattleControllerCoroutine
{
	/// <summary>
	/// Hack for now.
	/// </summary>
	public static Coroutine StartCoroutine(this BattleController controller, IEnumerator coroutine)
	{
		return Invoker.StartCoroutine(coroutine);
	}
}