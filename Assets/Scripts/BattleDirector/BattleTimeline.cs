using System.Collections.Generic;
using static Validation;

public class BattleTimeline
{
	/* Actions */
	public readonly BattleSimulator Simulator;

	public BattleTimeline(BattleSimulator simulator)
	{
		Simulator = simulator;
	}
}