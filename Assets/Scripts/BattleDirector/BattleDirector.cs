﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleDirector : MonoBehaviour
{
	private BattleController battle;

	public static void StartBattle(BattleController battle)
	{
		var directorGO = new GameObject("BattleDirector");
		var director = directorGO.AddComponent<BattleDirector>();
		director.battle = battle;
	}

	void Update()
	{
		battle?.Update();
	}

	void OnDestroy()
	{
		battle?.Dispose();
	}
}