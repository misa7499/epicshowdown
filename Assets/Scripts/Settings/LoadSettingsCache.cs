using UnityEngine;
using System;
using System.Collections.Generic;

public class SettingsCache<T> where T : GameSettings
{
	public readonly string Format;
	readonly Dictionary<string, T> cache = new Dictionary<string, T>();

	public SettingsCache(string format)
	{
		Format = format;
	}

	public T Load(string name)
	{
		if (!cache.TryGetValue(name, out T setting) || setting == null)
		{
			var fullPath = string.Format(Format, name);
			setting = Resources.Load<T>(fullPath);

			if (setting == null)
			{
				Debug.LogError($"Failed to load {typeof(T)} at path {fullPath}");
				return null;
			}

			cache.Add(name, setting);
		}
		return setting;
	}
}

public abstract class GameSettings : ScriptableObject
{
	private string nameCached;

	public string Name
	{
		get
		{
			if (string.IsNullOrEmpty(nameCached)) nameCached = name;
			return nameCached;
		}
	}
}