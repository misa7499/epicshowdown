using System;

/// <summary>
/// Complete information about a map layout.
/// </summary>
[Serializable]
public class BoardData
{
	public int Width = 20;
	public int Height = 11;
	public NFloat[] Heights;

	[Table]
	public UnitSpawnData[] UnitSpawns;
}

[Serializable]
public class UnitSpawnData
{
	public UnitSettings UnitSettings;
	public OwnerId Owner;
	public int x;
	public int y;
}