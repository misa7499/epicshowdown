﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSettings : GameSettings
{
	public BoardData BoardSettings;
	[Table] public List<BattlePlayerData> Players;


	[Button(ButtonMode.DisabledInPlayMode)]
	void PlayBattle()
	{
		SceneLoadCommands.RunFromBattleScene(new PlayArgs()
		{
			SelectedBattle = name
		});
	}

	[Button(ButtonMode.DisabledInPlayMode)]
	void PlayAndLogBattle()
	{
		SceneLoadCommands.RunFromBattleScene(new PlayArgs()
		{
			SelectedBattle = name,
			LogBattleState = true
		});
	}

	[Button(ButtonMode.EnabledInPlayMode)]
	void SaveBoard()
	{
		BoardSettings = Game.Battle.BoardMesh.SaveBoardData();
	}

	/// <summary>
	/// Loads battle settings with given name
	/// </summary>
	public static implicit operator BattleSettings(string name)
		=> Game.Settings.BattleSettings.Load(name);
}