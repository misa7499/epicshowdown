using System;
using UnityEngine;

/// <summary>
/// Base class for all simulation related objects.
/// </summary>
public class SimObject
{
	/* Parent simulator I am part of and Id I was assigned to on creation. */
	public readonly BattleSimulator Simulator;
	public readonly int Id;

	/* Everybody has an owner */
	public OwnerId Owner;
	public SimObject Parent;

	/* Lifecycle State */
	private NFloat timeAlive;
	bool isActive = true;
	bool isFirstActDone;

	/* Getter only API */
	public bool IsActive => isActive;
	public NFloat TimeAlive => timeAlive;

	/* Constructor */
	public SimObject(BattleSimulator simulator, OwnerId owner)
	{
		Simulator = simulator;
		Id = Simulator.IdGenerator.NextId();
		Owner = owner;
	}

	public SimObject(SimObject parent) : this(parent.Simulator, parent.Owner)
	{
		Parent = parent;
	}

	/// <summary>
	/// Mark object as deactivated - invokes deactivation events and mark object for removal from the simulation.
	/// </summary>
	public void Deactivate()
	{
		if (!isActive) { return; }
		isActive = false;

		try { OnDeactivate(); }
		catch(Exception e) { LogException(e); }
	}

	/// <summary>
	/// Change Owner variable with invoking an event.
	/// </summary>
	public bool ChangeOwner(OwnerId newOwner)
	{
		if (newOwner != Owner)
		{
			var oldOwner = Owner;
			Owner = newOwner;
			OnChangeOwner(oldOwner, newOwner);
			return true;
		}

		return false;
	}

	/* Lifecycle methods */
	public virtual void PreUpdate()
	{
	}

	public void Update(NFloat dT)
	{
		if (!isFirstActDone)
		{
			OnFirstAct();
			isFirstActDone = true;
		}

		OnUpdate(dT);
	}

	public virtual void LateUpdate(NFloat dT)
	{
		timeAlive += dT;
	}

	/* Helper methods */
	public BattlePlayer MyPlayer => Simulator.AllPlayers.Get(Owner);
	public BattlePlayer EnemyPlayer => Simulator.AllPlayers.GetEnemyOf(Owner);

	/* Events methods and utilities. */
	protected virtual void OnFirstAct() {}
	protected virtual void OnUpdate(NFloat dT) {}
	protected virtual void OnDeactivate() {}
	public virtual void OnDispose() {}
	protected virtual void OnChangeOwner(OwnerId oldOwner, OwnerId newOwner) {}
	public void LogException(Exception e) => UnityEngine.Debug.LogException(e);
}

