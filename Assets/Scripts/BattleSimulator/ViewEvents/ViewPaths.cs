public static class ViewPaths
{
	public static readonly string Units = "Units/UnitPrefabs/{0}";
	public static readonly string Projectiles = "Projectiles/{0}";
}