using System;
using System.Collections.Generic;

public static class ViewEventSubscribeUtil
{
	public static void Subscribe(this BattleObject3D target, int typeHash, Action<ViewEvent> action)
	{
		target.ViewEventDispatcher.Subscribe(typeHash, action);
	}

	public static void Subscribe(this BattleObject3D target, Action<ViewEvent> action)
	{
		target.ViewEventDispatcher.Subscribe(action);
	}

	public static void Subscribe(this BattleViewController target, int typeHash, Action<ViewEvent> action)
	{
		target.ViewEventDispatcher.Subscribe(typeHash, action);
	}

	public static void Subscribe(this BattleViewController target, Action<ViewEvent> action)
	{
		target.ViewEventDispatcher.Subscribe(action);
	}
}

public struct ViewEventDispatcher
{
	List<(int, Action<ViewEvent>)> eventSubscriptions;

	public void Subscribe(Action<ViewEvent> sub)
	{
		Subscribe(0, sub);
	}

	public void Subscribe(int typeHash, Action<ViewEvent> sub)
	{
		if (eventSubscriptions == null) eventSubscriptions = new List<(int, Action<ViewEvent>)>();
		eventSubscriptions.Add((typeHash, sub));
	}

	public void Dispatch(ViewEvent evt)
	{
		if (eventSubscriptions == null) return;

		foreach(var (typeHash, action) in eventSubscriptions)
		{
			if (typeHash == evt.TypeHash || typeHash == 0)
			{
				action.Dispatch(evt);
			}
		}
	}
}