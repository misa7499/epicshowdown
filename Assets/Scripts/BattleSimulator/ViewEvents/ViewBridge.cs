﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Bridge interface between Battle Simulation and Battle View.
/// </summary>
public interface IViewBridge
{
    void SendViewEvent(ViewEvent evt);
}