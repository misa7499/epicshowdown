using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ViewEvents
{
	public static readonly ViewEventType ObjectCreated = new ViewEventType("ObjectCreated");
	public static readonly ViewEventType ObjectDeactivated = new ViewEventType("ObjectDeactivated");
	public static readonly ViewEventType Start = new ViewEventType("Start");
	public static readonly ViewEventType End = new ViewEventType("End");
	public static readonly ViewEventType ChangeOwner = new ViewEventType("ChangeOwner");
	public static readonly ViewEventType SelectionChanged = new ViewEventType("SelectionChanged");
	public static readonly ViewEventType OnSelected = new ViewEventType("OnSelected");
	public static readonly ViewEventType OnDeselected = new ViewEventType("OnDeselected");
	public static readonly ViewEventType TileLeft = new ViewEventType("OnTileLeft");
	public static readonly ViewEventType TileEnter = new ViewEventType("OnTileEnter");
	public static readonly ViewEventType BoardChanged = new ViewEventType("BoardChanged");
	public static readonly ViewEventType Fire = new ViewEventType("Fire");
	public static readonly ViewEventType Message = new ViewEventType("Message");
	public static readonly ViewEventType DamageRecieved = new ViewEventType("DamageRecieved");
}

public struct ViewEvent
{
	public readonly string Type;
	public readonly SimObject2D Target;
	public readonly object Data;
	public readonly int TypeHash;

	public ViewEvent(SimObject2D target, ViewEventType type, object data = null)
	{
		Target = target;
		Data = data;
		Type = type;
		TypeHash = type.TypeHash;
	}
}

public struct ViewEventType
{
	public readonly string Type;
	public readonly int TypeHash;

	public ViewEventType(string type)
	{
		Type = type;
		TypeHash = Animator.StringToHash(type);
	}

	public static implicit operator ViewEventType(string type)
	{
		return new ViewEventType(type);
	}

	public static implicit operator string(ViewEventType type)
	{
		return type.Type;
	}

	public static implicit operator int(ViewEventType type)
	{
		return type.TypeHash;
	}
}