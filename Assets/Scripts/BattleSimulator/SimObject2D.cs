using UnityEngine;

/// <summary>
/// SimObject that can be represented graphicly on the battlefield.
/// </summary>
public abstract class SimObject2D : SimObject
{
	public string Name;
	public string PrefabPath;

	public abstract Vector3 Position3D();
	public abstract Vector3 Direction3D();
	public abstract float Height();

	public SimObject2D(SimObject parent) : base(parent) {}
	public SimObject2D(BattleSimulator simulator, OwnerId owner) : base(simulator, owner) { }

	/* View Events Support */
	public void EventHappened(ViewEventType type, object data = null) => Simulator.ViewBridge.SendViewEvent(new ViewEvent(this, type, data));
	public void ShowFloatingText(string msg) => EventHappened(ViewEvents.Message, msg);
	protected override void OnDeactivate() => EventHappened(ViewEvents.End);
	protected override void OnChangeOwner(OwnerId oldOwner, OwnerId newOwner) => EventHappened(ViewEvents.ChangeOwner, oldOwner);


	public override string ToString() => $"{base.ToString()} [{Name}]";
}