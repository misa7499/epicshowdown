/// <summary>
/// Powerfull tower of a spellcaster.
/// Game ends when this gets destroyed.
/// </summary>
public class Nexus : BoardStructureUnit
{
	public readonly NexusSettings NexusSettings;

	public SimTimer SpawnTimer = new SimTimer()
	{
		Duration = 6,
		TimeUntilNextTick = 0
	};

	public Nexus(UnitSettings settings, BattleSimulator simulator, OwnerId owner, Tile tile) : base(settings, simulator, owner, tile)
	{
		NexusSettings = NexusSettings.Instance;
	}

	protected override void OnUpdate(NFloat dT)
	{
		base.OnUpdate(dT);

		if (NexusSettings.SpawnWaves)
		{
			SpawnTimer.Duration = NexusSettings.SpawnWavesPeriod;

			if (SpawnTimer.Tick(dT))
			{
				foreach(var tile in Tile.Neighbours)
					if (tile.IsEmpty)
						Simulator.AddUnits("Warrior", tile.Position, Owner, NexusSettings.SpawnWavesCount);
			}
		}
	}
}