/// <summary>
/// Default non-structure units.
/// </summary>
public class Minion : Unit
{
	public Minion(BattleSimulator simulator, UnitSettings settings, OwnerId owner) : base(simulator, settings, owner)
	{
	}
}