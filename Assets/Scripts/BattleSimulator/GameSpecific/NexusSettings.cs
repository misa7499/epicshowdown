public class NexusSettings : ScriptableObject<NexusSettings>
{
	public bool SpawnWaves;
	[ShowIf(nameof(SpawnWaves)), Indent] public int SpawnWavesCount = 3;
	[ShowIf(nameof(SpawnWaves)), Indent] public NFloat SpawnWavesPeriod;
}