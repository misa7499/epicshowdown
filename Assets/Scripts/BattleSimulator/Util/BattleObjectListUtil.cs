﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BattleObjectListUtil
{
	public static void Add<T>(this ArrayBuffer<T> list, SimObject obj) where T : SimObject
	{
		if (obj is T objT) { list.Add(objT); }
	}

	public static bool Remove<T>(this ArrayBuffer<T> list, SimObject obj) where T : SimObject
	{
		return (obj is T objT && list.Remove(objT));
	}

	/* ACTIONS */

	public static void Deactivate<T>(this ArrayBuffer<T> list, Func<T, bool> filter) where T : SimObject
	{
		for(var i = 0; i < list.Count; i++)
		{
			if (filter == null || filter(list[i]))
			{
				list[i].Deactivate();
				continue;
			}
		}
	}

	public static void RemoveInactive<T>(this ArrayBuffer<T> list) where T : SimObject
	{
		for(var i = 0; i < list.Count; i++)
		{
			if (!list[i].IsActive)
			{
				list.RemoveAt(i--);
				continue;
			}
		}
	}

	public static T Get<T>(this ArrayBuffer<T> list, OwnerId owner) where T : SimObject
	{
		for(var i = 0; i < list.Count; i++)
		{
			if (list[i].Owner == owner)
			{
				return list[i];
			}
		}
		return null;
	}

	public static T GetEnemyOf<T>(this ArrayBuffer<T> list, OwnerId owner) where T : SimObject
	{
		for(var i = 0; i < list.Count; i++)
		{
			if (list[i].Owner != owner && list[i].Owner != OwnerId.Undefined)
			{
				return list[i];
			}
		}
		return null;
	}

	public static T GetById<T>(this ArrayBuffer<T> list, long objectId) where T : SimObject
	{
		for(var i = 0; i < list.Count; i++)
		{
			if (list[i].Id == objectId)
			{
				return list[i];
			}
		}
		return null;
	}

	public static T Closest<T>(this IEnumerable<T> list, Vector3 position3D) where T : SimObject2D
	{
		T closest = null;
		float minDist = float.MaxValue;

		foreach(var obj in list)
		{
			var dist = Vector3.Distance(position3D, obj.Position3D());
			if (dist < minDist)
			{
				minDist = dist;
				closest = obj;
			}
		}

		return closest;
	}
}