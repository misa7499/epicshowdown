﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Stat
{
	public NFloat Value => value;

	public NFloat BaseValue
	{
		get { return baseValue; }
		set
		{
			baseValue = value;
			RecalculateValue();
		}
	}

	[SerializeField] NFloat baseValue;
	NFloat incFlat;
	NFloat incPercent;
	NFloat decreaseRelative;

	/* final value */
	NFloat value;

	void RecalculateValue()
 	{
		value = (baseValue * (1 + incPercent) + incFlat) * (NFloat.One - decreaseRelative);
	}

	public void IncFlat(NFloat value)
	{
		incFlat += value;
		RecalculateValue();
	}

	public void IncPercent(NFloat value)
	{
		incPercent += value;
		RecalculateValue();
	}

	public void Decrease(NFloat value)
	{
		decreaseRelative = MathNFloat.Max(decreaseRelative, value);
		RecalculateValue();
	}

	public void Reset()
	{
		incFlat.value = 0L;
		decreaseRelative.value = 0L;
		incPercent.value = 0L;
		value = baseValue;
	}

	public static implicit operator NFloat(Stat stat) => stat.value;

	public override string ToString() => value.ToString();
}

[System.Serializable]
public struct StatModifier
{
}