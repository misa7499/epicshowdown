using System;
using System.Collections.Generic;
using UnityEngine;

public static class SimObjectExtensions
{
	public static bool IsHumanPlayer(this SimObject lhs) => lhs?.MyPlayer?.Data.PlayerType == BattlePlayerType.Human;

	public static bool IsEnemyOf(this SimObject lhs, SimObject rhs) => lhs.Owner != OwnerId.Undefined && lhs.Owner != rhs.Owner;
	public static bool IsEnemyOf(this SimObject lhs, OwnerId rhs) => lhs.Owner != OwnerId.Undefined && lhs.Owner != rhs;
	public static bool IsEnemyOf(this OwnerId lhs, OwnerId rhs) => lhs != OwnerId.Undefined && rhs != OwnerId.Undefined && lhs != rhs;

	public static float DistanceTo(this SimObject2D lhs, Vector3 pos) => Vector3.Distance(lhs.Position3D(), pos);

	public static IEnumerable<Unit> MyUnits(this SimObject lhs)
	{
		foreach(var unit in lhs.Simulator.AllUnits)
			if (unit.Owner == lhs.Owner)
				yield return unit;
	}

	public static void Kill(this SimObject lhs) => lhs.Deactivate();
}