using System.Collections.Generic;

public static class AllUnitsInRadiusFilters
{
	public static IEnumerable<Unit> AllEnemiesInRadius(this SimObject obj, NVector2 position, NFloat radius)
	{
		return UnitFilterEnumerator<Unit>.Fetch(obj.Simulator.AllUnits, position, radius, OwnerMask.GetEnemiesOf(obj.Owner));
	}

	public static IEnumerable<Unit> AllObjectsInRadius(this BattleSimulator simulator, NVector2 position, NFloat radius)
	{
		return UnitFilterEnumerator<Unit>.Fetch(simulator.AllUnits, position, radius);
	}
}

public class UnitFilterEnumerator<T> : ReusableEnumerator<T, UnitFilterEnumerator<T>> where T : Unit
{
	int i;
	ArrayBuffer<T> list;

	bool checkOwner;
	OwnerMask ownerMask;

	bool checkRadius;
	NVector2 position;
	NFloat radius;

	public static UnitFilterEnumerator<T> Fetch(ArrayBuffer<T> list, NVector2 position, NFloat radius)
	{
		var fetched = Fetch();
		fetched.i = 0;
		fetched.list = list;
		fetched.SetRadiusCheck(position, radius);
		return fetched;
	}

	public static UnitFilterEnumerator<T> Fetch(ArrayBuffer<T> list, NVector2 position, NFloat radius, OwnerMask ownerMask)
	{
		var fetched = Fetch();
		fetched.i = 0;
		fetched.list = list;
		fetched.SetOwnerMask(ownerMask);
		fetched.SetRadiusCheck(position, radius);
		return fetched;
	}

	public static UnitFilterEnumerator<T> Fetch(ArrayBuffer<T> list, OwnerMask ownerMask)
	{
		var fetched = Fetch();
		fetched.i = 0;
		fetched.list = list;
		fetched.SetOwnerMask(ownerMask);
		return fetched;
	}

	void SetOwnerMask(OwnerMask mask)
	{
		checkOwner = true;
		ownerMask = mask;
	}

	void SetRadiusCheck(NVector2 position, NFloat radius)
	{
		checkRadius = true;
		this.position = position;
		this.radius = radius;
	}

	public override bool MoveNext()
	{
		while (i < list.Count)
		{
			var unit = list[i++];

			if (checkOwner && !ownerMask.Match(unit))
				continue;

			if (checkRadius && !unit.IsInRange(position, radius))
				continue;

			Current = unit;
			return true;
		}

		return false;
	}

	public override void OnDisposed()
	{
		list = null;
		checkRadius = false;
		checkOwner = false;
	}
}

public struct OwnerMask
{
	public OwnerId owner;
	public bool MatchIfEqual;

	public bool Match(OwnerId rhs)
	{
		return MatchIfEqual ^ (owner != rhs);
	}

	public bool Match(Unit rhs)
	{
		return MatchIfEqual ^ (owner != rhs.Owner);
	}

	public static OwnerMask GetFriends(OwnerId owner) => new OwnerMask()
	{
		owner = owner,
		MatchIfEqual = true
	};

	public static OwnerMask GetEnemiesOf(OwnerId owner) => new OwnerMask()
	{
		owner = owner,
		MatchIfEqual = false
	};
}
