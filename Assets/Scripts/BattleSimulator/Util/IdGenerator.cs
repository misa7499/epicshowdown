public class IdGenerator
{
	int nextId = 1;

	public IdGenerator(int initialValue) { nextId = initialValue; }
	public int NextId() => nextId++;
}