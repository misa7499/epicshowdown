using System;
using System.Collections.Generic;

/// <summary>
/// Class which syncs randomness over server/clients.
/// </summary>
public class RandomnessProvider
{
	private readonly SimpleRandom implementation;

	public RandomnessProvider(int seed)
	{
		implementation = new SimpleRandom(seed);
	}

	public int Range(int min, int max)
	{
		return implementation.Range(min, max);
	}

	public NFloat Range(NFloat min, NFloat max)
	{
		return implementation.Range(min, max);
	}

	public bool Proc(NFloat procChance)
	{
		return implementation.nextFloat() <= procChance;
	}

	public bool NextBoolean()
	{
		return implementation.nextBoolean();
	}

	public void Shuffle<T>(List<T> list)
	{
		int count = list.Count;
		for (int index = 0; index < count; index++)
		{
			int chosenOne = Range(index, count - 1);
			T temp = list[index];
			list[index] = list[chosenOne];
			list[chosenOne] = temp;
		}
	}

	public T GetRandom<T>(List<T> list)
	{
		if (list == null || list.Count == 0) return default(T);

		int index = Range(0, list.Count - 1);
		return list[index];
	}

	public class SimpleRandom
	{
		private static readonly long multiplier = 0x5DEECE66DL;
		private static readonly long addend = 0xBL;
		private static readonly long mask = (1L << 48) - 1;
		private long seed;

		public SimpleRandom(long seed)
		{
			this.seed = seed;
		}

		private int next(int bits)
		{
			long oldseed = seed;
			long nextseed = (oldseed * multiplier + addend) & mask;
			seed = nextseed;
			return (int)(nextseed >> (48 - bits));
		}

		public int nextInt()
		{
			return next(32);
		}

		public int nextInt(int n)
		{
			if (n <= 0)
			{
				throw new Exception("n must be positive");
			}

			if ((n & -n) == n) // i.e., n is a power of 2
			{
				return (int)((n * (long)next(31)) >> 31);
			}

			int bits,
				val;
			do
			{
				bits = next(31);
				val = bits % n;
			}
			while (bits - val + (n - 1) < 0);
			return val;
		}

		public long nextLong()
		{
			// it's okay that the bottom word remains signed.
			return ((long)(next(32)) << 32) + next(32);
		}

		public bool nextBoolean()
		{
			return next(1) != 0;
		}

		public NFloat nextFloat()
		{
			return next(12) / (1 << 12).ToNFloat();
		}

		public int Range(int min, int max)
		{
			if (min == max)
			{
				return min;
			}

			return min + nextInt(max - min);
		}

		public NFloat Range(NFloat min, NFloat max)
		{
			var resolution = 756.ToNFloat();
			return Range((int)(min * resolution), (int)(max * resolution)) / resolution;
		}
	}
}