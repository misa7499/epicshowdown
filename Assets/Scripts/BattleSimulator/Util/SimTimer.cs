public struct SimTimer
{
	public NFloat Duration;
	public NFloat TimeUntilNextTick;

	public bool Tick(NFloat dT)
	{
		TimeUntilNextTick -= dT;
		if (TimeUntilNextTick <= 0)
		{
			TimeUntilNextTick += Duration;
			return true;
		}
		else
		{
			return false;
		}
	}
}