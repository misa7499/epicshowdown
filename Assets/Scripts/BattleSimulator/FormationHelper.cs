using System;
using System.Collections.Generic;
using UnityEngine;
using static MathNFloat;

public static class FormationHelper
{
	/// <summary>
	/// Issue move orders for selected units in a formation.
	/// </summary>
	public static void MoveAsFormation(Unit[] selected, NVector2 targetPos)
	{
		var positions = FormationHelper.GetPositions(selected, targetPos);
		for(int i = 0; i < positions.Length && i < selected.Length; i++)
		{
			//selected[i].OnRightClick(positions[i], forceAttack: false);
		}
	}

	public static NVector2[] GetPositions(Unit[] selected, NVector2 groundPos)
	{
		NFloat sparseness = new NFloat(1.2f);
		NFloat selectedCount = selected.Length;
		int numberOfRows = selectedCount.Sqrt().FloorToInt();
		int numberOfColumns = (selectedCount / numberOfRows).CeilToInt();

		var height = (numberOfRows - 1) * sparseness;

		var middle = selected.AveragePosition();
		var vertical = (groundPos - middle).normalized;
		var horizontal = new NVector2(vertical.y, -vertical.x);

		/* SORT UNITS */
		Array.Sort(selected, (a, b) =>
			NVector2.Dot(a.position - middle, vertical) > NVector2.Dot(b.position - middle, vertical) ? 1 : -1);

		for(int i = 0; i < numberOfRows; i++)
		{
			var start = i * numberOfColumns;
			var length = Mathf.Min(numberOfColumns, selected.Length - i * numberOfColumns);

			Array.Sort(selected, start, length, Comparer<Unit>.Create((a, b) =>
				NVector2.Dot(a.position - middle, horizontal) > NVector2.Dot(b.position - middle, horizontal) ? 1 : -1));
		}

		/* CREATE POSITIONS */
		var positions = new NVector2[selected.Length];
		for(int i = 0; i < selected.Length; i++)
		{
			var row = i / numberOfColumns;
			var column = i - row * numberOfColumns;
			var totalInRow = Mathf.Min(numberOfColumns, selected.Length - row * numberOfColumns);
			var rowWidth = (totalInRow - 1) * sparseness;

			var y = height > 0 ? height * (row / (numberOfRows - 1) - NFloat.Half) : 0;
			var x = rowWidth > 0 ? rowWidth * (column / (totalInRow - 1) - NFloat.Half) : 0;
			positions[i] = groundPos + vertical * y + horizontal * x;
			selected[i].Simulator.PlayableArea.Clamp(ref positions[i], selected[i].radius);
		}
		return positions;
	}
}