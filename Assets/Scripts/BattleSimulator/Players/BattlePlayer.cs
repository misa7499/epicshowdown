using System;
using UnityEngine;
using static Validation;

public class BattlePlayer : SimObject
{
	public Resource Gold;
	public readonly BattlePlayerDeck Deck;
	public readonly AIPlayer AIBrain;
	public Nexus Nexus;

	/* Data */
	public readonly BattlePlayerData Data;

	/* Properties */
	public string Name => Data.Name;
	public BattlePlayerType PlayerType => Data.PlayerType;

	public BattlePlayer(BattleSimulator sim, BattlePlayerData data) : base(sim, data.Owner)
	{
		Data = data;
		Deck = new BattlePlayerDeck(this);
		Gold.Give(data.InitialGold);

		// init AI Brain if needed.
		if (data.PlayerType == BattlePlayerType.AI)
		{
			AIBrain = new AIPlayer(this, data.AIKernel);
		}
	}

	protected override void OnDeactivate()
	{
	}

	public override string ToString() => Name;
}