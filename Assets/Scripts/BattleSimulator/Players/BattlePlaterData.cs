using System;
using UnityEngine;

[Serializable]
public class BattlePlayerData
{
	public string Name;
	public BattlePlayerType PlayerType;
	public Color Color;
	public Color UIColor;
	public OwnerId Owner;
	public int InitialGold;
	public int x, y;
	public AIKernel AIKernel;

	public bool IsAI => PlayerType == BattlePlayerType.AI;
}

public enum BattlePlayerType
{
	Human = 0,
	AI = 1,
}