using System;

public struct Resource
{
	public event Action OnAmountChanged;
	public int Amount { get; private set; }

	public void Give(int amount)
	{
		Amount += amount;
		OnAmountChanged.Dispatch();
	}

	public void Spend(int amount)
	{
		Amount -= amount;
		OnAmountChanged.Dispatch();
	}

	public Validation CanAfford(int price)
	{
		return Validation.validate(Amount >= price, "Not enough gold!");
	}
}