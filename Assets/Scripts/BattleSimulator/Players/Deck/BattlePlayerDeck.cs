using System;
using System.Collections.Generic;
using static Validation;

public class BattlePlayerDeck
{
	public readonly BattlePlayer Player;
	public readonly List<CardInfo> Cards = new List<CardInfo>();

	public BattlePlayerDeck(BattlePlayer player)
	{
		Player = player;

		// add cards for placing minions.
		Cards.Add(new PlaceUnitCardInfo(player, "Warrior"));
		Cards.Add(new PlaceUnitCardInfo(player, "Archer"));
		Cards.Add(new PlaceUnitCardInfo(player, "Mage"));
		Cards.Add(new PlaceUnitCardInfo(player, "Wall"));
		Cards.Add(new PlaceUnitCardInfo(player, "Obstacle"));
	}
}