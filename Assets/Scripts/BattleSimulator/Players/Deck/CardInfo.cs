/// <summary>
/// Player's action.
/// </summary>
public abstract class CardInfo
{
	public readonly BattlePlayer Player;
	public readonly string Name;

	public OwnerId Owner => Player.Owner;
	public BattleSimulator Simulator => Player.Simulator;

	public CardInfo(BattlePlayer player, string name)
	{
		Name = name;
		Player = player;
	}

	public abstract Validation Validate(CastData info);
	public abstract void OnPlayed(CastData info);
}