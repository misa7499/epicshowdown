/// <summary>
/// Standardized, immutable cast data.
/// </summary>
public readonly struct CastData
{
	public readonly NVector2 targetPosition;
	public readonly Tile Tile;
	public readonly Unit targetUnit;

	public CastData(BattleSimulator simulator, NVector2 position)
	{
		targetPosition = position;
		Tile = simulator.Board.GetTile(position);
		targetUnit = null;
	}

	public CastData(Tile tile)
	{
		targetPosition = tile?.Position ?? new NVector2();
		Tile = tile;
		targetUnit = null;
	}

	public CastData(Unit unit)
	{
		targetPosition = unit?.position ?? new NVector2();
		Tile = unit?.Simulator.Board.GetTile(unit.position);
		targetUnit = unit;
	}
}