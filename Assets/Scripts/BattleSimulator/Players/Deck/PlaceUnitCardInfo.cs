using static Validation;

public class PlaceUnitCardInfo : CardInfo
{
	public readonly UnitSettings Settings;

	public PlaceUnitCardInfo(BattlePlayer player, UnitSettings settings) : base(player, settings.Name)
	{
		Settings = settings;
	}

	public override Validation Validate(CastData castData) =>
		Player.Gold.CanAfford(Settings.GoldCost) &&
		validate(castData.Tile.IsEmpty, "Obstacles on tile!") &&
		validate(UnitPlacementRules.CanPlace(Player, castData.Tile, Settings), "Tried to place unit on an illegal location!");

	public override void OnPlayed(CastData castData)
	{
		Player.Gold.Spend(Settings.GoldCost);
		Simulator.AddUnits(Settings, castData.targetPosition, Owner, Settings.SpawnCount);
	}
}

