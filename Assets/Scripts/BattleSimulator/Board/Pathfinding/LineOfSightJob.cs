using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

public static class LineOfSightJobUtil
{
	static List<Tile> path = new List<Tile>();

	public static NativeArray<NVector2> GetDirections(this NativeArray<SteeringJob.SteeringRequestData> requests, BattleSimulator simulator)
	{
		var job = new LineOfSightJob()
		{
			requests = requests,
			obstacles = GetObstacles(simulator),
			results = new NativeArray<int>(requests.Length, Allocator.TempJob, NativeArrayOptions.UninitializedMemory),
		};

		try
		{
			job.Run(requests.Length);

			// apply line of sight results to the
			var results = new NativeArray<NVector2>(requests.Length, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
			{
				for(int i = 0; i < job.results.Length; i++)
				{
					if (job.results[i] == 0 && GetNextPathPoint(requests[i], simulator.Board) is NVector2 pathfindedDirection)
					{
						results[i] = pathfindedDirection;
					}
					else
					{
						results[i] = requests[i].DirectionToTarget;
					}
				}
			}
			return results;
		}
		finally
		{
			job.obstacles.Dispose();
			job.results.Dispose();
		}
	}

	static NVector2? GetNextPathPoint(in SteeringJob.SteeringRequestData request, GameBoard board)
	{
		var fromTile = board.GetTile(request.position);
		if (fromTile == null) return null;

		var goalTile = board.GetTile(request.targetPosition);
		if (goalTile == null) return null;

		// try get path from the cache
		var (pathInCache, resultTile) = board.PathfinindingCache.GetNextTile(fromTile, goalTile);
		if (!pathInCache)
		{
			// cache miss, do the pathfinding
			board.Pathfinding.FindPath(fromTile, goalTile, board.GamePathfinder, path);

			if (path.Count > 1)
			{
				// if path is valid, find next point in path unit can go straight ahead
				resultTile = path[1];
				for (int pathIndex = 2; pathIndex < path.Count; pathIndex++)
				{
					var nextTile = path[pathIndex];
					if (!LineOfSightCheck.HasLineOfSight(board.Simulator, request.position, NFloat.Zero, nextTile.Position))
						break;
					else
						resultTile = nextTile;
				}
			}

			board.PathfinindingCache.SetNextTile(fromTile, goalTile, resultTile);
		}

		// if path found, go there.
		if (resultTile != null)
			return NVector2.FromTo(fromTile.Position, resultTile.Position);
		else
			return null;
	}

	[BurstCompile]
	public struct LineOfSightJob : IJobParallelFor
	{
		[ReadOnly] public NativeArray<SteeringJob.SteeringRequestData> requests;
		[ReadOnly] public NativeArray<LineOfSightObstacle> obstacles;

		[WriteOnly] public NativeArray<int> results;

		public void Execute(int index)
		{
			var request = requests[index];
			var x = request.position.x;
			var y = request.position.y;
			var target_x = request.targetPosition.x;
			var target_y = request.targetPosition.y;

			for(int i = 0; i < obstacles.Length; i++)
			{
				var rhs = obstacles[i];
				if (MathNFloat2D.PathObstructed(x, y, target_x, target_y, rhs.position.x, rhs.position.y, rhs.radius + request.radius))
				{
					results[index] = 0;
					return;
				}
			}

			results[index] = 1;
		}
	}

	public struct LineOfSightObstacle
	{
		public int id;
		public NVector2 position;
		public NFloat radius;
	}

	static NativeArray<LineOfSightObstacle> GetObstacles(BattleSimulator simulator)
	{
		var allStructures = simulator.AllStructures;
		var obstacles = new NativeArray<LineOfSightObstacle>(allStructures.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

		var allStructures_bfr = allStructures.buffer;
		for(int i = 0; i < allStructures.Count; i++)
		{
			var obstacle = allStructures_bfr[i];
			obstacles[i] = new LineOfSightObstacle()
			{
				id = obstacle.Id,
				position = obstacle.position,
				radius = obstacle.radius
			};
		}

		return obstacles;
	}
}