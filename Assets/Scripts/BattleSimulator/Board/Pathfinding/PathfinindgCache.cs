using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class PathfindingCache
{
	public readonly GameBoard Board;
	public readonly int[,] cachedDirections;
	bool isDirty = true;

	public PathfindingCache(GameBoard board)
	{
		Board = board;
		cachedDirections = new int[board.Tiles.Count, board.Tiles.Count];
	}

	public void SetDirty()
	{
		isDirty = true;
	}

	public (bool found, Tile nextTile) GetNextTile(Tile from, Tile target)
	{
		// clear cache if needed.
		if (isDirty)
		{
			isDirty = false;
			for(int i = 0; i < Board.Tiles.Count; i++)
				for(int j = 0; j < Board.Tiles.Count; j++)
					cachedDirections[i, j] = -1;
		}

		var nextTileIndex = cachedDirections[from.Index, target.Index];
		var nextTile = nextTileIndex > 0 ? Board.Tiles[nextTileIndex] : null;
		return (nextTileIndex != -1, nextTile);
	}

	public void SetNextTile(Tile from, Tile target, Tile nextTile)
	{
		cachedDirections[from.Index, target.Index] = nextTile?.Index ?? -2;
	}
}