using System.Collections.Generic;
using System.Linq;

public class Tile : GameBoard.Tile
{
	public NFloat Height;
	public readonly List<BoardStructureUnit> Objects = new List<BoardStructureUnit>();

	public BattlePlayer MyPlayer => owner != OwnerId.Undefined ? Board.Simulator.AllPlayers.Get(owner) : null;

	private OwnerId owner;
	public OwnerId Owner
	{
		get { return owner; }
		set
		{
			if (owner != value)
			{
				owner = value;
			}
		}
	}

	public void OnObjectEnter(BoardStructureUnit boardObject)
	{
		if (Objects.Count == 0) { boardObject.Simulator.Board.PathfinindingCache.SetDirty(); }
		Objects.Add(boardObject);
	}

	public void OnObjectLeft(BoardStructureUnit boardObject)
	{
		if (Objects.Count == 1) { boardObject.Simulator.Board.PathfinindingCache.SetDirty(); }
		Objects.Remove(boardObject);
	}

	public bool IsEmpty => Objects.Count == 0;
}

public class Vertex : GameBoard.Vertex
{
}