public class GamePathfinder : GameBoard.Pathfinder
{
	public (bool legal, NFloat cost) EvaluateMovement(Tile from, Tile to, Tile goal)
	{
		return (to.IsEmpty || to == goal, GameBoard.HexHeight2);
	}

	public bool IsLegalPosition(Tile position)
	{
		return true; //position.IsEmpty;
	}
}