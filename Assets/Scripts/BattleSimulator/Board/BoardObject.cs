﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Unit rooted on a hexagon.
/// </summary>
public class BoardStructureUnit : Unit
{
	Tile _tile;

	/// <summary>
	/// Tile which this object currently occupies.
	/// </summary>
	public Tile Tile
	{
		get { return _tile; }

		protected set
		{
			if (value == null)
			{
				Debug.LogError("Tried to set null tile for object " + this);
				return;
			}

			if (!value.IsEmpty)
			{
				Debug.LogError($"Tried to set illegal tile {value} for object " + this);
				return;
			}

			SetTileInternal(value);
		}
	}

	/// <summary>
	/// Unit constructor + initial tile.
	/// </summary>
	public BoardStructureUnit(UnitSettings settings, BattleSimulator simulator, OwnerId owner, Tile tile) : base(simulator, settings, owner)
	{
		Tile = tile;
	}

	/// <summary>
	/// Internal call that skips all the safeguards.
	/// </summary>
	void SetTileInternal(Tile value)
	{
		if (_tile != value)
		{
			if (_tile != null)
			{
				OnTileLeft(_tile);
				_tile.OnObjectLeft(this);
				EventHappened(ViewEvents.TileLeft, _tile);
			}

			_tile = value;

			if (_tile != null)
			{
				position = _tile.Position;
				OnTileEnter(_tile);
				_tile.OnObjectEnter(this);
				EventHappened(ViewEvents.TileEnter, _tile);
			}
		}
	}

	/// <summary>
	/// Handler invoked when unit has left a previously occupied tile.
	/// </summary>
	protected virtual void OnTileLeft(Tile tile) {}

	/// <summary>
	/// Handler invoked when object has entered a new tile.
	/// </summary>
	protected virtual void OnTileEnter(Tile tile) {}

	/// <summary>
	/// Remove me from the tile on dispose.
	/// </summary>
	public override void OnDispose() => SetTileInternal(null);
}