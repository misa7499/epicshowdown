using System.Collections.Generic;
using UnityEngine;

public static class GameBoardExtensions
{
	public static Color GetColor(this OwnerId owner)
	{
		return Game.Simulator.AllPlayers.Get(owner)?.Data.Color ?? Color.white;
	}

	public static List<Tile> GetMyTilesList(this SimObject obj)
	{
		var list = new List<Tile>();
		foreach(var tile in obj.Simulator.Board.Tiles)
			if (tile.Owner == obj.Owner) { list.Add(tile); }
		return list;
	}
}