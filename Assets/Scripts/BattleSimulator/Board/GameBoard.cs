﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTile = Tile;

public class GameBoard : HexBoard<GameTile, Vertex, GameBoard>
{
	public readonly BattleSimulator Simulator;
	public readonly GamePathfinder GamePathfinder;
	public readonly PathfindingCache PathfinindingCache;

	public GameBoard(BattleSimulator simulator, BattleSettings battle) : base(battle.BoardSettings.Width, battle.BoardSettings.Height)
	{
		Simulator = simulator;
		GamePathfinder = new GamePathfinder();
		PathfinindingCache = new PathfindingCache(this);

		for(int i = 0; i < battle.BoardSettings.Heights.Length; i++)
		{
			if (Tiles[i] is GameTile tile)
			{
				tile.Height = battle.BoardSettings.Heights[i];
			}
		}
	}

	/// <summary>
	/// Update occupied flags.
	/// </summary>
	public void OnPreUpdate()
	{

	}

	public bool PlayerOwnsATile(OwnerId owner)
	{
		for(var i = 0; i < Width; i++)
			for(var j = 0; j < Height; j++)
				if (Tiles[i, j].Owner == owner)
					return true;

		return false;
	}

	public int GetTileCount(OwnerId owner)
	{
		var count = 0;
		for(var i = 0; i < Width; i++)
			for(var j = 0; j < Height; j++)
				if (Tiles[i, j].Owner == owner) count++;
		return count;
	}
}