using UnityEngine;

public partial class Unit
{
	public override float Height() => (float)height;
	public override Vector3 Position3D() => position.Position3D();
	public override Vector3 Direction3D() => lookDirection.Direction3D();

	public NVector3 center3D
		=> new NVector3() { x = position.x, y = position.y, z = height / 2 };

	public bool IsAggroTarget
		=> !IsInvulnerable && IsActive;

	public void Push(NVector2 force)
		=> velocity += force / mass;
	public void Push(NVector2 forceDir, NFloat strength)
		=> Push(forceDir.normalized * strength);
	public void Push(Unit rhs, NFloat strength)
		=> rhs.Push((rhs.position - position).normalized * strength);

	public bool IsInRange(Unit rhs, NFloat range) =>
		rhs != null && IsTouchingInternal(rhs.position, radius.value + rhs.radius.value + range.value);
	public bool IsInRange(NVector2 rhs_position, NFloat range)
		=> IsTouchingInternal(rhs_position, radius.value + range.value);

	public bool IsTouching(Unit rhs)
		=> IsTouchingInternal(rhs.position, radius.value + rhs.radius.value);
	public bool IsTouching(in NVector2 rhs_position)
		=> IsTouchingInternal(rhs_position, radius.value);
	public bool IsTouching(in NVector2 rhs_position, NFloat radius)
		=> IsTouchingInternal(rhs_position, radius.value);

	bool IsTouchingInternal(in NVector2 rhs_position, long radiusValue)
	{
		var dist_x = rhs_position.x.value - position.x.value;
		var dist_y = rhs_position.y.value - position.y.value;

		return dist_x*dist_x + dist_y*dist_y <= (radiusValue + NFloat.Tenth.value)*(radiusValue + NFloat.Tenth.value);
	}

	public NFloat DistanceTo(Unit rhs)
		=> NFloat.Distance(position.x, position.y, rhs.position.x, rhs.position.y);

	public NFloat DistanceTo(in NVector2 rhs_position)
		=> NFloat.Distance(position.x, position.y, rhs_position.x, rhs_position.y);

	public NFloat SqrDistanceTo(in NVector2 rhs_position)
		=> NFloat.DistanceSqr(position.x, position.y, rhs_position.x, rhs_position.y);

	public NFloat SqrDistanceTo(Unit rhs)
		=> NFloat.DistanceSqr(position.x, position.y, rhs.position.x, rhs.position.y);
}