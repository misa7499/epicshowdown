﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

/// <summary>
/// Participates in battle.
/// </summary>
[Serializable]
public partial class Unit : SimObject2D
{
	public readonly UnitSettings Settings;

	/* Transform */
	public NVector2 position;
	public NVector2 lookDirection;
	public NVector2 velocity;

	/* Recort last position diff */
	public NVector2 lastPosition;
	public NFloat lastPositionDiff;

	/* State */
	public Stat MovementSpeed;

	public Unit(BattleSimulator simulator, UnitSettings settings, OwnerId owner) : base(simulator, owner)
	{
		Settings = settings;
		Name = settings.Name;
		PrefabPath = ViewPaths.Units;
		InitializeBody(settings);
		initializeBrain(settings);
		initializeHealth(settings);
		initializeAttack(settings);
	}

	public override void PreUpdate()
	{
		lastPosition = position;
		attackState.PreUpdate();
		PreUpdateCollision();
		ThinkAboutNextAction();
	}

	protected override void OnUpdate(NFloat dT)
	{
		ExecuteAction(dT);
	}

	public override void LateUpdate(NFloat dT)
	{
		base.LateUpdate(dT);
		ValidateCurrentAction();
	}

	public void ApplyMove(in SteeringJob.Result moveResult, NFloat dT)
	{
		DirectionUpdate(moveResult.newLookDirection);
		lookDirection = moveResult.newLookDirection;
		position = moveResult.newPosition;

		if (MovementSpeed.Value > 0 && moveResult.positionDelta > NFloat.Tenth * dT)
		{
			actionType = ActionType.Move;
		}
		else
		{
			actionType = ActionType.Idle;
		}
	}

	public void LookAt(NVector2 targetPosition)
	{
		var dir = targetPosition - position;
		var m = NFloat.Distance(dir.x, dir.y);

		if (m.value > NFloat.Tousandth.value)
		{
			lookDirection = dir / m;
		}
	}
}