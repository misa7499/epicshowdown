using System;

public partial class Unit
{
	public NFloat height;
	public NFloat radius;
	public NFloat influenceRadius;
	public NFloat directionChangeFatigue;
	public NFloat mass;

	public NFloat colliderMass;
	public NFloat colliderRadius;

	public bool IsStructure => Settings.CollisionType == ColliderType.Hexagon;
	public bool HasCollision => Settings.CollisionType != ColliderType.None;

	void InitializeBody(UnitSettings settings)
	{
		height = settings.Height;
		colliderRadius = radius = settings.Radius;
		colliderMass = mass = settings.Mass;
	}

	public void PreUpdateCollision()
	{
		// get target radius/mass
		mass = Settings.Mass;
		var collisionSettings = Simulator.CollisionJob.Settings;
		var targetRadius = Settings.Radius;
		var targetMass = mass;

		if (IsAttacking)
		{
			targetRadius *= collisionSettings.AttackingRadius;
			targetMass *= collisionSettings.AttackingMass;
		}
		else if (IsMoving)
		{
			targetRadius *= collisionSettings.MovingRadius;
		}

		// change real values smoothly
		colliderRadius = targetRadius;
		colliderMass = targetMass;

		// recover direction change fatigue.
		directionChangeFatigue = directionChangeFatigue.ApplyDrag
		(
			collisionSettings.DirectionChangeFatigueRecoveryRelative,
			collisionSettings.DirectionChangeFatigueRecoveryFlat,
			Game.dT
		);

		// calculate influence radius
		if (IsStructure)
		{
			influenceRadius = radius * collisionSettings.InfluenceZoneStructure;
		}
		else
		{
			influenceRadius = radius * collisionSettings.InfluenceZone;
		}
	}

	public void DirectionUpdate(NVector2 newDirection)
	{
		directionChangeFatigue += (1 - NVector2.Dot(newDirection, lookDirection)) * Simulator.CollisionJob.Settings.DirectionChangeFatigue;
	}
}