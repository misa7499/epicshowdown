using UnityEngine;

public partial class Unit // Unit.Health
{
	public bool IsInvulnerable;
	public Stat MaxHealth;

	[SerializeField] private NFloat currentHealthPercent;

	void initializeHealth (UnitSettings settings)
	{
		currentHealthPercent = 1;
		MaxHealth.BaseValue = settings.Health;
		IsInvulnerable = settings.Invulnarable;
	}

	public NFloat CurrentHealthPercent
	{
		get { return currentHealthPercent; }
		set { currentHealthPercent = MathNFloat.Min(value, 1); }
	}

	public NFloat CurrentHealth
	{
		get { return MaxHealth.Value * currentHealthPercent; }
		set { CurrentHealthPercent = value / MaxHealth; }
	}

	public bool IsEliminated
	{
		get { return currentHealthPercent <= 0; }
	}

	public void TakeDamage(NFloat damage, Unit damageSource, Projectile projectile = null)
	{
		if (IsInvulnerable)
		{
			return;
		}

		CurrentHealth -= damage;
		EventHappened(ViewEvents.DamageRecieved, damageSource);
		//ShowFloatingText(damage.ToString());
	}
}