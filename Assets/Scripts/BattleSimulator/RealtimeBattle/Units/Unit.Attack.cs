using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class Unit // Unit.Attack
{
	/* State */
	private ActionState attackState;

	/* Stats */
	public Stat AttackDamage;
	public Stat AttackSpeed;
	public Stat AttackRange;
	public Stat AggroRange;
	public Stat SplashRange;

	void initializeAttack(UnitSettings settings)
	{
		MovementSpeed.BaseValue = settings.Speed;
		AttackRange.BaseValue = settings.AttackSettings.AttackRange;
		AggroRange.BaseValue = settings.AttackSettings.AggroRange;
		AttackSpeed.BaseValue = settings.AttackSettings.AttackSpeed;
		AttackDamage.BaseValue = settings.AttackSettings.Damage;
		SplashRange.BaseValue = settings.AttackSettings.Splash ? settings.AttackSettings.SplashRadius : NFloat.Zero;
	}

	public bool IsRanged => Settings.AttackSettings.AttackType == AttackType.Range;
	public bool CanAttack => Settings.AttackSettings.AttackType != AttackType.None;
	public bool IsFinishingAttack => attackState.isDownswing;
	public Unit ActiveTarget => attackState.activeTarget.targetUnit;
	public bool IsValidAttackTarget(Unit target) => target != null && target.Owner != Owner && target.IsAggroTarget;

	/// <summary>
	/// Check if the specified target is in unit's attack range.
	/// </summary>
	public bool IsInAttackRange(Unit target)
	{
		if (!IsInRange(target, AttackRange))
		{
			return false;
		}

		if (IsRanged && !this.HasLineOfSight(target))
		{
			return false;
		}

		return true;
	}

	/// <summary>
	/// Check if the specified target is in unit's aggro range.
	/// </summary>
	public bool IsInAggroRange(Unit target)
	{
		if (!IsInRange(target, AggroRange))
		{
			return false;
		}

		if (IsRanged && !this.HasLineOfSight(target))
		{
			return false;
		}

		return true;
	}

	/// <summary>
	/// Returns true if attack action can be ticked at the given target.
	/// </summary>
	public bool CanAttackNow(TargetInfo target)
	{
		// just a downswing - no need to check condition, let the animation finish.
		if (attackState.isDownswing)
		{
			return true;
		}

		// if I can't attack at all - abort
		if (Settings.AttackSettings.AttackType == AttackType.None)
		{
			return false;
		}

		// if target is not a valid attack target - abort!
		if (target.targetUnit != null && !IsValidAttackTarget(target))
		{
			return false;
		}

		// if in range - just do it.
		// if not in range but animation started earlier, let one attack happen.
		return IsInAttackRange(target) || target == attackState.activeTarget;
	}

	/// <summary>
	/// Ticks attack animation, calls PerformAttack on the right time.
	/// </summary>
	public ActionType TickAttack(TargetInfo target, NFloat dT)
	{
		if (!CanAttackNow(target))
		{
			return ActionType.Idle;
		}

		LookAt(target.position);

		if (attackState.Tick(target, dT, AttackSpeed, Settings.AttackSettings.UpswingTime))
		{
			PerformAttack(target, Settings.AttackSettings);
		}

		return ActionType.Attack;
	}

	/// <summary>
	/// Shots projectile if ranged, or lands damage immediately if melee.
	/// </summary>
	public void PerformAttack(TargetInfo target, AttackSettings attackSettings)
	{
		switch(attackSettings.AttackType)
		{
			case AttackType.Melee:
				ApplyAttack(target);
				break;

			case AttackType.Range:
				Simulator.FireProjectile(this, AttackRange, attackSettings.ProjectileSettings, target,
					onReached: (projectile, reachedTarget) => ApplyAttack(reachedTarget, projectile));
				break;
		}

		if (attackSettings.Recoil)
		{
			this.Push(position - target.position, attackSettings.RecoilForce);
		}
	}

	/// <summary>
	/// Actual damage land. Happens when the project lands, or immediately if unit is melee.
	/// </summary>
	public void ApplyAttack(TargetInfo target, Projectile projectile = null)
	{
		if (SplashRange.Value > 0)
		{
			foreach(var hitTarget in this.AllEnemiesInRadius(target.position, SplashRange))
				ApplyDamageEffect(hitTarget);
		}
		else if (target.targetUnit is Unit hitTarget)
		{
			ApplyDamageEffect(hitTarget);
		}

		void ApplyDamageEffect(Unit hit)
		{
			hit.TakeDamage(AttackDamage, this, projectile);

			// push away targets if needed.
			if (Settings.AttackSettings.PushTargets)
			{
				NVector2 pushDirection = projectile != null ? (NVector2)projectile.lookDirection : position - hit.position;
				hit.Push(pushDirection, Settings.AttackSettings.PushForce);
			}
		}
	}

	/// <summary>
	/// Keeps attack ticking at the last target.
	/// </summary>
	public ActionType FinishAttack(NFloat dT)
	{
		// invalidate current attack target.
		attackState.activeTarget.ValidateTargetUnit();

		return TickAttack(attackState.activeTarget, dT);
	}
}

/// <summary>
/// Generаlized action state.
/// </summary>
public struct ActionState
{
	public bool keepAlive;
	public NFloat progress;
	public bool isDownswing;
	public TargetInfo activeTarget;

	public void PreUpdate()
	{
		if (!keepAlive)
		{
			progress = 0;
			isDownswing = false;
			activeTarget = new TargetInfo();
		}
		keepAlive = false;
	}

	public bool Tick(TargetInfo target, NFloat dT, NFloat speed, NFloat upswingTime)
	{
		var executed = false;
		keepAlive = true;
		activeTarget = target;
		progress += dT * speed;

		// reached upswing? mark action as executed.
		if (!isDownswing && progress >= upswingTime)
		{
			executed = true;
			isDownswing = true;
		}

		// end animation when needed.
		if (progress > NFloat.One)
		{
			progress -= NFloat.One;
			isDownswing = false;
			activeTarget = new TargetInfo();
		}

		return executed;
	}
}