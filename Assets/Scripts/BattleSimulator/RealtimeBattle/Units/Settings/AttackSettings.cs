using System;
using UnityEngine;

[Serializable]
public class AttackSettings
{
	public AttackType AttackType;
	public NFloat Damage;
	public NFloat AttackRange = 0.5f;
	public NFloat AttackSpeed = 0.75f;
	public NFloat UpswingTime = 0.5f;
	public NFloat AggroRange = 10f;

	[Flatten, ShowIf("AttackType", "Range")]
	public ProjectileSettings ProjectileSettings;

	[Header("Splash")]
	public bool Splash;
	[Indent, ShowIf("Splash")] public NFloat SplashRadius;


	[Header("Recoil")]
	public bool PushTargets;
	[Indent, ShowIf("PushTargets")] public NFloat PushForce;
	public bool Recoil;
	[Indent, ShowIf("Recoil")] public NFloat RecoilForce;
}

public enum AttackType
{
	None = 0,
	Melee = 1,
	Range = 2
}