/// <summary>
/// Used when you want to pass around either concrete unit, either unit settings only.
/// </summary>
public struct UnitOrSettings
{
	public Unit Unit;
	public UnitSettings Settings;

	public static implicit operator UnitOrSettings(UnitSettings settings) => new UnitOrSettings() { Settings = settings };
	public static implicit operator UnitOrSettings(Unit unit) => new UnitOrSettings() { Unit = unit, Settings = unit?.Settings };

	public static implicit operator Unit(UnitOrSettings obj) => obj.Unit;
	public static implicit operator UnitSettings(UnitOrSettings obj) => obj.Settings;
}