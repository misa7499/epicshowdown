using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitSettings : GameSettings
{
	[Header("Body")]
	public NFloat Height;
	public NFloat Radius = 1;
	public NFloat Speed;
	public ColliderType CollisionType;
	[Indent, HideIf("CollisionType", "None")] public NFloat Density = 1;


	[Header("Health")]
	public bool Invulnarable;
	public NFloat Health;

	[Flatten]
	public AttackSettings AttackSettings;

	[Header("Casting")]
	public int GoldCost;
	public int SpawnCount = 1;

	public NFloat Mass
	{
		get
		{
			var mass = Radius * Radius * Density;
			return CollisionType == ColliderType.Hexagon ? mass * 10 : mass;
		}
	}

	public static implicit operator UnitSettings(string name) => Game.Settings.UnitSettings.Load(name);
}