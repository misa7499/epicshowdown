using System;
using UnityEngine;

public partial class Unit // Unit.Brain
{
	UnitOrder order;
	UnitCommand action;
	[SerializeField] ActionType actionType;

	public bool IsMoving => actionType == ActionType.Move;
	public bool IsAttacking	=> actionType == ActionType.Attack;
	public ActionType ActionType => actionType;

	void initializeBrain(UnitSettings settings)
	{
		order = new HoldOrder(this);
	}

	public void ThinkAboutNextAction()
	{
		action = order.Think();
		actionType = action.actionType;
	}

	public void NotifyNewAggroTarget(Unit aggroTarget)
	{
		var newAggroTarget = IsInAggroRange(aggroTarget) ? aggroTarget : null;
		action = order.NotifyNewAggroTarget(newAggroTarget);
		actionType = action.actionType;
	}

	public void ExecuteAction(NFloat dT)
	{
		actionType = action.Execute(this, dT);
	}

	public void ValidateCurrentAction()
	{
		// check if current action is still valid
		if (!order.Validate())
			order = new HoldOrder(this);
	}
}