using System;
using UnityEngine;
using static UnityEngine.Profiling.Profiler;

public class HoldOrder : UnitOrder
{
	public Unit aggroTarget;
	public NVector2? holdPosition;

	public HoldOrder(Unit unit) : base(unit) {}

	public override UnitCommand Think()
	{
		// for people with no attack, just chill
		if (!unit.CanAttack)
		{
			aggroTarget = null;
			return Idle();
		}

		// validate current target
		if (!unit.IsValidAttackTarget(aggroTarget))
		{
			aggroTarget = null;
		}

		// check for target nearby
		if (aggroTarget == null || (!unit.IsFinishingAttack && !unit.IsInAttackRange(aggroTarget)))
		{
			unit.Simulator.AggroJob.RequestAggroJob(unit);
			return default(UnitCommand);
		}
		else
		{
			return NotifyNewAggroTarget(aggroTarget);
		}
	}

	public override sealed UnitCommand NotifyNewAggroTarget(Unit newAggroTarget)
	{
		// if you have nothing better, just go for the nexus
		if (newAggroTarget == null && unit.CanAttack && unit.EnemyPlayer?.Nexus is Unit enemyNexus && unit.IsValidAttackTarget(enemyNexus))
		{
			newAggroTarget = enemyNexus;
		}

		aggroTarget = newAggroTarget;

		// remember hold position if started aggroing stuff.
		if (holdPosition == null && aggroTarget != null)
		{
			holdPosition = unit.position;
		}
		else if (holdPosition != null && aggroTarget == null && unit.IsTouching(holdPosition.Value))
		{
			holdPosition = null;
		}

		// issue command.
		if (unit.IsFinishingAttack)
		{
			return FinishAttack();
		}
		else if (aggroTarget != null)
		{
			return Attack(aggroTarget);
		}
		else if (holdPosition != null)
		{
			return Move(holdPosition.Value, false);
		}
		else
		{
			return Idle();
		}
	}
}