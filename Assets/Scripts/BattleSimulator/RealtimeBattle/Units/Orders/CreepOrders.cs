﻿using System;
using UnityEngine;

public abstract class UnitOrder
{
	public readonly Unit unit;
	public readonly TargetInfo target;
	public readonly Unit targetObject;

	public UnitOrder(Unit unit, TargetInfo target)
	{
		this.unit = unit;
		this.target = target;
		this.targetObject = target.targetUnit;
	}

	public UnitOrder(Unit unit) : this(unit, unit.position) {}
	public abstract UnitCommand Think();
	public virtual bool Validate() => true;

	public virtual UnitCommand NotifyNewAggroTarget(Unit aggroTarget) => Attack(aggroTarget);
	protected UnitCommand Idle() => new UnitCommand(null, unit.position);
	protected UnitCommand FinishAttack() => new UnitCommand((unit, dT, target) => unit.FinishAttack(dT), unit.position, ActionType.Attack);

	protected UnitCommand Move(TargetInfo target, bool isAttack = false)
	{
		if (unit.MovementSpeed.Value > 0)
		{
			unit.Simulator.SteeringJob.RequestSteering(unit, target, isAttack);
		}
		return new UnitCommand(null, target, ActionType.Move);
	}

	protected UnitCommand Attack(Unit target)
	{
		if (!unit.CanAttackNow(target)) { return Move(target, true); }
		return new UnitCommand((unit, dT, targ) => unit.TickAttack(targ, dT), target, ActionType.Attack);
	}
}

/// <summary>
/// What action should unit execute on update loop?
/// </summary>
public struct UnitCommand
{
	public delegate ActionType UnitAction(Unit unit, NFloat dT, TargetInfo target);
	public UnitAction action;
	public TargetInfo target;
	public ActionType actionType;

	public UnitCommand(UnitAction action, TargetInfo target, ActionType actionType = ActionType.Idle)
	{
		this.action = action;
		this.actionType = actionType;
		this.target = target;
	}

	public ActionType Execute(Unit unit, NFloat dT)
	{
		return action != null ? action(unit, dT, target) : actionType;
	}
}