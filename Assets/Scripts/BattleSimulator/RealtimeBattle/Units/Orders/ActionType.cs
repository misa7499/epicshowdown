public enum ActionType
{
	Idle = 0,
	Attack = 1,
	Move = 2,
	Dead = 3,
	Casting = 4,
}