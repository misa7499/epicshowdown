using UnityEngine;

public class MoveOrder : UnitOrder
{
	public NVector2 targetPosition;

	public MoveOrder(Unit unit, TargetInfo target) : base(unit)
	{
		this.targetPosition = target;
	}

	public override UnitCommand Think() => Move(targetPosition);

	public override bool Validate() => !unit.IsTouching(targetPosition, NFloat.Tenth);
}