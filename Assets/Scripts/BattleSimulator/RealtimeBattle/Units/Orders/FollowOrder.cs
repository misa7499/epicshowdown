using UnityEngine;

public class FollowOrder : UnitOrder
{
	public FollowOrder(Unit unit, Unit target) : base(unit, target) {}
	public override UnitCommand Think() => Move(target, false);
	public override bool Validate() => targetObject.IsActive && !targetObject.IsEnemyOf(unit);
}