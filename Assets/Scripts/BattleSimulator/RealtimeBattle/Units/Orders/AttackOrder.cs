using UnityEngine;

public class AttackOrder : UnitOrder
{
	public AttackOrder(Unit unit, Unit target) : base(unit, target) {}
	public override UnitCommand Think() => Attack(target);
	public override bool Validate() => unit.CanAttack && unit.IsValidAttackTarget(target);
}