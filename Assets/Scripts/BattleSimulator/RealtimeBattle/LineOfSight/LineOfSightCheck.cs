using UnityEngine;
using static MathNFloat2D;

public static class LineOfSightCheck
{
	public static bool HasLineOfSight(this Unit lhs, TargetInfo target)
	{
		return HasLineOfSight(lhs.Simulator, lhs.position, lhs.height, target);
	}

	public static bool HasLineOfSight(BattleSimulator simulator, NVector2 position, NFloat height, TargetInfo target)
	{
		var x = position.x;
		var y = position.y;

		var target_x = target.position.x;
		var target_y = target.position.y;

		var cnt = simulator.AllStructures.Count;
		var list = simulator.AllStructures.buffer;

		for(int i = 0; i < cnt; i++)
		{
			var rhs = list[i];
			if (rhs.height.value < height.value) { continue; }
			if (rhs.height.value < target.height.value) { continue; }
			if (rhs == target.targetUnit) { continue; }

			if (PathObstructed(x, y, target_x, target_y, rhs.position.x, rhs.position.y, rhs.radius))
			{
				return false;
			}
		}

		return true;
	}
}