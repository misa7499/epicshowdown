using System;
using UnityEngine;

/// <summary>
/// Target can be a unit or position on the ground.
/// </summary>
public struct TargetInfo
{
	public Unit targetUnit;
	NVector2 targetPos;
	NFloat targetHeight;

	public NVector2 position
	{
		get
		{
			if (targetUnit != null) { targetPos = targetUnit.position; }
			return targetPos;
		}
	}

	public NVector3 position3D
	{
		get
		{
			if (targetUnit != null) { return targetUnit.center3D; }
			return new NVector3(targetPos.x, targetPos.y, targetHeight);
		}
	}

	public NFloat height => targetUnit != null ? targetUnit.height : targetHeight;
	public NFloat radius => targetUnit != null ? targetUnit.radius : new NFloat();
	public int targetId => targetUnit != null ? targetUnit.Id : 0;

	public static implicit operator TargetInfo (Unit target) => new TargetInfo
	{
		targetUnit = target,
		targetPos = target != null ? target.position : new NVector2(),
		targetHeight = target != null ? target.height / 2 : 0
	};

	public static implicit operator TargetInfo (NVector2 targetPos) => new TargetInfo
	{
		targetPos = targetPos
	};

	public static implicit operator TargetInfo (NVector3 targetPos) => new TargetInfo
	{
		targetPos = targetPos,
		targetHeight = targetPos.z,
	};

	public void ValidateTargetUnit()
	{
		if (targetUnit != null && !targetUnit.IsActive) { targetUnit = null; }
	}

	public static implicit operator NVector2 (TargetInfo target) => target.position;
	public static implicit operator NVector3 (TargetInfo target) => target.position3D;
	public static implicit operator Unit (TargetInfo targetInfo) => targetInfo.targetUnit;

	public static bool operator ==(TargetInfo lhs, TargetInfo rhs) => (lhs.targetUnit != null && lhs.targetUnit == rhs.targetUnit) || lhs.targetPos == rhs.targetPos;
	public static bool operator !=(TargetInfo lhs, TargetInfo rhs) => (lhs.targetUnit != null && lhs.targetUnit != rhs.targetUnit) || lhs.targetPos != rhs.targetPos;

	public override int GetHashCode() => base.GetHashCode();
	public override bool Equals(object obj) => base.Equals(obj);
}