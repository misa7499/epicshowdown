using System.Collections.Generic;
using UnityEngine;

public static class UnitExtensions
{
	public static T ClosestEnemy<T>(this Unit lhs) where T : Unit
	{
		return lhs.ClosestEnemy<T>(NFloat.MaxValue);
	}

	public static T ClosestEnemy<T>(this Unit lhs, NFloat radius) where T : Unit
	{
		T closest = null;
		var minDist = radius;
		var allUnits_bfr = lhs.Simulator.AllUnits.buffer;
		var allUnits_cnt = lhs.Simulator.AllUnits.Count;
		for(int i = 0; i < allUnits_cnt; i++)
		{
			var unit = allUnits_bfr[i];
			if (unit.Owner != lhs.Owner && unit is T)
			{
				CheckIfCloser(lhs.position, unit, ref closest, ref minDist);
			}
		}
		return closest;
	}

	public static T Closest<T>(this List<T> allObjs, NVector2 position, NFloat radius, OwnerId owner = OwnerId.Undefined) where T : Unit
	{
		T closest = null;
		var minDist = radius;
		for(int i = 0; i < allObjs.Count; i++)
		{
			var obj = allObjs[i];
			if (obj is T && (owner == OwnerId.Undefined || owner == obj.Owner))
			{
				CheckIfCloser(position, obj, ref closest, ref minDist);
			}
		}
		return closest;
	}

	public static T Closest<T>(this Unit lhs, NVector2 position, NFloat radius, OwnerId owner = OwnerId.Undefined) where T : Unit
	{
		T closest = null;
		var minDist = radius;
		var allObjs = lhs.Simulator.AllUnits;
		for(int i = 0; i < allObjs.Count; i++)
		{
			var obj = allObjs[i];
			if (obj is T && obj != lhs && (owner == OwnerId.Undefined || obj.Owner == owner))
			{
				CheckIfCloser(position, obj, ref closest, ref minDist);
			}
		}
		return closest;
	}

	public static void CheckIfCloser<T>(in NVector2 position, Unit obj, ref T closest, ref NFloat minDist) where T : Unit
	{
		var dist = NFloat.DistanceSqr(position.x, position.y, obj.position.x, obj.position.y);
		if (dist.value < minDist.value)
		{
			minDist = dist;
			closest = obj as T;
		}
	}

	public static NVector2 AveragePosition(this IList<Unit> objs) => objs.Average(x => x.position);
}