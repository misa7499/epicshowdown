using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

public class AggroJob
{
	public struct AggroJobRequest
	{
		public NFloat x;
		public NFloat y;
		public NFloat aggroRadius;
		public OwnerId Owner;
	}

	public struct AggroTargetData
	{
		public int id;
		public NFloat x;
		public NFloat y;
		public NFloat radius;
		public OwnerId Owner;
	}

	ArrayBuffer<Unit> activeRequesters = new ArrayBuffer<Unit>();
	ArrayBuffer<Unit> aggroTargets = new ArrayBuffer<Unit>();

	public void RequestAggroJob(Unit requester)
	{
		activeRequesters.Add(
			requester
		);
	}

	public void Run(BattleSimulator simulator)
	{
		// sanity check
		if (activeRequesters.Count == 0)
			return;

		// add aggro targets
		for(int i = 0; i < simulator.AllUnits.Count; i++)
		{
			var unit = simulator.AllUnits.buffer[i];
			if (unit.IsAggroTarget) { aggroTargets.Add( unit ); }
		}

		// add aggro targets data
		var aggroData = new NativeArray<AggroTargetData>(aggroTargets.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		for(int i = 0; i < aggroData.Length; i++)
		{
			var unit = aggroTargets.buffer[i];
			aggroData[i] = new AggroTargetData()
			{
				id = i,
				x = unit.position.x,
				y = unit.position.y,
				radius = unit.radius,
				Owner = unit.Owner
			};
		};

		// add aggro requests
		var aggroRequests = new NativeArray<AggroJobRequest>(activeRequesters.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		for(int i = 0; i < aggroRequests.Length; i++)
		{
			var unit = activeRequesters.buffer[i];
			aggroRequests[i] = new AggroJobRequest()
			{
				x = unit.position.x,
				y = unit.position.y,
				Owner = unit.Owner,
				aggroRadius = unit.AggroRange,
			};
		};

		// allocate memory for results
		var aggroResults = new NativeArray<int>(aggroRequests.Length, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

		// run the job
		var job = new Job()
		{
			AggroRequests = aggroRequests,
			AggroData = aggroData,
			AggroResults = aggroResults,
		};
		job.Run(aggroRequests.Length);

		// notify requesters
		for(int i = 0; i < activeRequesters.Count; i++)
		{
			var newAggroTargetIndex = job.AggroResults[i];
			var newAggroTarget = newAggroTargetIndex >= 0 ? aggroTargets[newAggroTargetIndex] : null;
			activeRequesters[i].NotifyNewAggroTarget(newAggroTarget);
		}

		// dispose native memory
		aggroRequests.Dispose();
		aggroResults.Dispose();
		aggroData.Dispose();

		// clear managed memory
		aggroTargets.Clear();
		activeRequesters.Clear();
	}

	[BurstCompile]
	public struct Job : IJobParallelFor
	{
		[ReadOnly] public NativeArray<AggroJobRequest> AggroRequests;
		[ReadOnly] public NativeArray<AggroTargetData> AggroData;

		[WriteOnly] public NativeArray<int> AggroResults;

		public void Execute(int index)
		{
			var request = AggroRequests[index];
			int bestTarget = -1;
			NFloat bestDist = (request.aggroRadius + NFloat.One).Squared;

			for(int i = 0; i < AggroData.Length; i++)
			{
				if (request.Owner == AggroData[i].Owner)
					continue;

				var dist_x = request.x - AggroData[i].x;
				var dist_y = request.y - AggroData[i].y;
				var dist_sqr = NFloat.SqrSum(dist_x, dist_y);
				if (dist_sqr > bestDist) { continue; }

				bestTarget = i;
				bestDist = dist_sqr;
			}

			AggroResults[index] = bestTarget;
		}
	}
}