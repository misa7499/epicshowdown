using UnityEngine;
using static FixedFloatMath;

public struct LineColliderData
{
	public NVector2 center;
	public NFloat length;
	public NVector2 lengthAxis;
	public NFloat width;
	public NVector2 widthAxis;
}

public class LineColliderList : ArrayBuffer<LineColliderData>
{
	public void AddLineCollider(NVector2 from, NVector2 to, NFloat width)
	{
		if (from == to)
			return;

		var lhs = new LineColliderData();
		lhs.length = NVector2.Distance(from, to) / 2;
		lhs.center = (from + to) / 2;
		lhs.lengthAxis = (lhs.center - to) / lhs.length;
		lhs.width = width;

		// rotate length axis 90 degrees.
		lhs.widthAxis.x = lhs.lengthAxis.y;
		lhs.widthAxis.y = -lhs.lengthAxis.x;

		base.Add(lhs);
	}

	public bool LineOfSightCheck(NVector2 from, NVector2 to, NFloat radius)
	{
		if (Count == 0 || from == to)
			return true;

		long x21 = from.x.value;
		long x22 = from.y.value;

		var dist = NVector2.Distance(from, to);
		var dir = (to - from) / dist;
		long y21 = dir.x.value;
		long y22 = dir.y.value;

		for(int i = 0; i < Count; i++)
		{
			ref var line = ref buffer[i];

			long x11 = line.center.x.value;
			long x12 = line.center.y.value;
			long y11 = line.lengthAxis.x.value;
			long y12 = line.lengthAxis.y.value;

			long det = (y22*y11 - y12*y21) >> DecimalBits;
			if (det == 0)
			{
				// lines are parallel
				continue;
			}

			var intersection = ( (x21 - x11) * y22 + (x12 - x22) * y21) / det;
			if (abs(intersection) > line.length.value + radius.value)
			{
				// line doesnt intersects with rectangle
				continue;
			}

			intersection = ( (x11 - x21) * y12 + (x22 - x12) * y11) / det;
			if (intersection <= dist.value + radius.value)
			{
				// line intersects with rectangle and intersection is between from and to.
				return false;
			}
		}

		return true;
	}
}