using System;
using UnityEngine;
using UnityEngine.Profiling;
using ColliderData = CollisionJob.ColliderData;

public class CollisionList
{
	public ColliderData[] buffer = new ColliderData[128];
	public int Count;

	public void InsertionSort()
	{
		Profiler.BeginSample("RunCollision.InsertSort");
			InsertionSort(0, Count - 1);
		Profiler.EndSample();
	}

	public void InsertionSort(int lo, int hi)
	{
		int i, j;
		long t;
		ColliderData tValue;
		for (i = lo; i < hi; i++)
		{
			t = buffer[i + 1].leftEdge.value;

			// it should usually be sorted
			if (t >= buffer[i].leftEdge.value)
			{
				continue;
			}

			j = i;
			tValue = buffer[i + 1];
			while (j >= lo && t < buffer[j].leftEdge.value)
			{
				buffer[j + 1] = buffer[j];
				j--;
			}

			buffer[j + 1] = tValue;
		}
	}

	public void InsertBinarySort(in ColliderData newColliderData)
	{
		var insertIndex = Count;

		// allocate more memory if needed.
		if (insertIndex == buffer.Length)
		{
			Array.Resize(ref buffer, Count * 2);
		}

		// find place which doesn't break sort.
		if (insertIndex > 0)
		{
			insertIndex = BinarySearch(newColliderData.leftEdge.value, 0, insertIndex);
		}

		// shift others if not placed on the end.
		if (insertIndex < Count)
		{
			Array.Copy(buffer, insertIndex, buffer, insertIndex + 1, Count - insertIndex);
		}
		buffer[insertIndex] = newColliderData;
		Count++;
	}

	int BinarySearch(long key, int min, int max)
	{
		if (buffer[min].leftEdge.value >= key)
		{
			return min;
		}

		var index = max;
		while (min <= max)
		{
			int mid = (min + max) / 2;
			long midValue = buffer[mid].leftEdge.value;
			if (key > midValue)
			{
				min = mid + 1;
			}
			else if (key < midValue)
			{
				index = mid;
				max = mid - 1;
			}
			else
			{
				return mid;
			}
		}
		return index;
	}
}