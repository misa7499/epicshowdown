using UnityEngine;

public class CollisionSettings : ScriptableObject<CollisionSettings>
{
	[Header("LineTest")]
	public bool UseTestLine;
	[ShowIf("UseTestLine"), Indent] public NVector2 LineTestTo;
	[ShowIf("UseTestLine"), Indent] public NVector2 LineTestFrom;

	[Header("Collision")]
	public NFloat CollisionStrength = 0.99f;
	public int CollisionPasses = 1;
	public NFloat Drag = 0.85f;
	public NFloat ConstantDrag = 0.02f;
	public NFloat DragPerPass = 0.85f;
	public NFloat MaxPushVelocityGain = 1.2f;

	[Header("Battle Params")]
	public NFloat MovingRadius = 0.5f;
	public NFloat AttackingRadius = 0.5f;
	public NFloat AttackingMass = 2;
	public NFloat UnitAvoidanceRatio = 10;

	[Header("Steering")]
	public NFloat MoveThreshold;
	public NFloat AwayFromTargetPenalty = 1;
	public NFloat GoodEnoughThreshold = 1;
	public NFloat TooHardThreshold = 10;
	public NFloat MaxRaycastDist = 3;
	public NFloat InfluenceZone = 3;
	public NFloat InfluenceZoneStructure = 3;
	public NFloat SteeringRadiusFactor = 0.95f;
	public NFloat EnemyAttractionScore = -2;

	[Header("Direction Change penalty")]
	public NFloat ChangeDirectionPenalty = 2;
	public NFloat DirectionChangeFatigue;
	public NFloat DirectionChangeFatigueRecoveryRelative;
	public NFloat DirectionChangeFatigueRecoveryFlat;

	[Header("Aggro")]
	public NFloat AggroSwitchPenalty;

	public void OnValidate()
	{
		if (CollisionPasses > 0)
		{
			DragPerPass = Mathf.Pow((float)Drag, 1f / CollisionPasses);
		}
	}
}