using System;
using System.Collections.Generic;

public enum ColliderType

{
	None = 0,
	Circle = 1,
	Hexagon = 2,
}