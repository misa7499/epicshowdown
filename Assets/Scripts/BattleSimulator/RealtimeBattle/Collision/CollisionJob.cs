﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Profiling;

public class CollisionJob
{
	public readonly CollisionSettings Settings;
	public readonly NRect PlayableArea;

	public CollisionJob(CollisionSettings settings, NRect playableArea)
	{
		Settings = settings;
		PlayableArea = playableArea;
	}

	readonly CollisionList allColliders = new CollisionList();
	public readonly LineColliderList allLineColliders = new LineColliderList();

	public struct ColliderData
	{
		public int id;
		public NFloat leftEdge;
		public NVector2 position;
		public NFloat radius;
		public NVector2 velocity;
		public NFloat Mass;
		public int isFixed;
	}

	public struct ColliderDataTransient
	{
		public NVector2 position;
		public NFloat push_x;
		public NFloat push_y;
	}

	public void Run(ArrayBuffer<Unit> allObjects, NFloat dT)
	{
		if (Settings.CollisionPasses <= 0)
		{
			// collision turned off.
			return;
		}

		Profiler.BeginSample("RunCollision.Prep");
			BuildColliderList(allObjects);
		Profiler.EndSample();

		Profiler.BeginSample("RunCollision.N2");
			RunCollision(dT);
		Profiler.EndSample();

		Profiler.BeginSample("RunCollision.Apply");
			ApplyColliderList(allObjects);
		Profiler.EndSample();
	}

	public void BuildColliderList(ArrayBuffer<Unit> allObjects)
	{
		if (Settings.UseTestLine)
		{
			allLineColliders.Count = 0;
			allLineColliders.AddLineCollider(Settings.LineTestFrom, Settings.LineTestTo, 0.25f);
			Debug.DrawLine(Settings.LineTestFrom.Position3D(0.5f), Settings.LineTestTo.Position3D(0.5f), Color.red);
		}

		// clear collision buffer.
		allColliders.Count = 0;

		// rebuild collider list.
		for(int i = 0; i < allObjects.Count; i++)
		{
			var obj = allObjects[i];

			switch(obj.Settings.CollisionType)
			{
				case ColliderType.Hexagon:
				case ColliderType.Circle:

					allColliders.InsertBinarySort(new ColliderData()
					{
						id = i,
						position = obj.position,
						radius = obj.colliderRadius,
						velocity = obj.velocity,
						isFixed = obj.IsStructure ? 1 : 0,
						leftEdge = obj.position.x - obj.colliderRadius,
						Mass = obj.colliderMass,
					});
					break;
			}
		}
	}

	#region Run collision

	public void RunCollision(NFloat dT)
	{
		var collisionStrengthPerPass = Settings.CollisionStrength / Settings.CollisionPasses;
		var job = new RunN2CollisionJob()
		{
			PlayableArea = PlayableArea,
			strength = collisionStrengthPerPass,
			maxPushVelocityGain = Settings.MaxPushVelocityGain * collisionStrengthPerPass,
			constantDrag = Settings.ConstantDrag * dT * collisionStrengthPerPass,
			dragPerPass = Settings.DragPerPass,
			colliders = new NativeArray<ColliderData>(allColliders.Count, Allocator.TempJob, NativeArrayOptions.ClearMemory),
			collisionResults = new NativeArray<CollisionResult>(allColliders.Count, Allocator.TempJob, NativeArrayOptions.ClearMemory),
			lineColliders = new NativeArray<LineColliderData>(allLineColliders.Count, Allocator.TempJob, NativeArrayOptions.ClearMemory),
			colliderPush = new NativeArray<NVector2>(allColliders.Count, Allocator.TempJob, NativeArrayOptions.ClearMemory),
			colliderPositions = new NativeArray<NVector2>(allColliders.Count, Allocator.TempJob, NativeArrayOptions.ClearMemory),
			dT = dT,
		};

		// multiple passes
		for(int i = 0; i < Settings.CollisionPasses; i++)
		{
			if (i > 0)
				allColliders.InsertionSort();

			if (allColliders.Count > 0)
				NativeArray<ColliderData>.Copy(allColliders.buffer, job.colliders, allColliders.Count);

			if (allLineColliders.Count > 0)
				NativeArray<LineColliderData>.Copy(allLineColliders.buffer, job.lineColliders, allLineColliders.Count);

			job.Run();

			for(int j = 0; j < allColliders.Count; j++)
			{
				ref var lhs = ref allColliders.buffer[j];
				if (lhs.isFixed > 0) { continue; }

				lhs.position = job.collisionResults[j].position;
				lhs.velocity = job.collisionResults[j].velocity;
			}
		}

		job.lineColliders.Dispose();
		job.colliders.Dispose();
		job.collisionResults.Dispose();

		job.colliderPush.Dispose();
		job.colliderPositions.Dispose();
	}

	public struct CollisionResult
	{
		public NVector2 position;
		public NVector2 velocity;
	}

	[BurstCompile]
	public struct RunN2CollisionJob : IJob
	{
		[ReadOnly] public NRect PlayableArea;
		[ReadOnly] public NFloat strength;
		[ReadOnly] public NFloat maxPushVelocityGain;
		[ReadOnly] public NFloat dT;
		[ReadOnly] public NFloat dragPerPass;
		[ReadOnly] public NFloat constantDrag;

		[ReadOnly] public NativeArray<ColliderData> colliders;
		[ReadOnly] public NativeArray<LineColliderData> lineColliders;
		public NativeArray<NVector2> colliderPositions;
		public NativeArray<NVector2> colliderPush;

		[WriteOnly] public NativeArray<CollisionResult> collisionResults;

		public void Execute()
		{
			var timeAdjustedStrength = strength * dT;
			var inverseTimeAdjustedStrength = 1 / timeAdjustedStrength;

			// update velocity
			for(int i = 0; i < colliders.Length; i++)
			{
				var lhs = colliders[i];
				if (lhs.isFixed > 0)
				{
					colliderPositions[i] = lhs.position;
				}
				else
				{
					colliderPositions[i] = lhs.position + lhs.velocity * timeAdjustedStrength;
				}
			}

			// run N^2
			for(int i = 0; i < colliders.Length; i++)
			{
				// cache left side
				var lhs = colliders[i];
				var lhs_radius = lhs.radius;
				var lhs_right = colliderPositions[i].x + lhs_radius;

				for(int j = i + 1; j < colliders.Length; j++)
				{
					// check x
					var rhs = colliders[j];
					if (lhs_right <= rhs.leftEdge) { break; }
					if (lhs.isFixed > 0 && rhs.isFixed > 0) { continue; }

					// check y
					var sum_r = lhs_radius + rhs.radius;
					var dist_y = colliderPositions[i].y - colliderPositions[j].y;
					if (dist_y >= sum_r || -dist_y >= sum_r) { continue; }

					// check complete
					var dist_x = colliderPositions[i].x - colliderPositions[j].x;
					var dist_sqr = dist_x*dist_x + dist_y*dist_y;
					if (dist_sqr >= sum_r*sum_r) { continue; }

					// check for zero - do something random
					var dist = dist_sqr.Sqrt();
					if (dist < NFloat.Tousandth)
					{
						var rotation = new NVector2(1, 0); // simple pseudo random
						dist = NFloat.Tousandth;
						dist_x = dist * rotation.x;
						dist_y = dist * rotation.y;
					}

					// calculate force
					var normalized_force = (sum_r - dist) * strength / dist;
					var lhs_force = normalized_force;
					var rhs_force = normalized_force;

					// distribute force according to relative mass of the objects.
					var totalMass = lhs.Mass + rhs.Mass;
					if (totalMass != 0)
					{
						lhs_force = lhs_force * rhs.Mass / totalMass;
						rhs_force = rhs_force * lhs.Mass / totalMass;
					}

					// update positions.
					colliderPush[i] += new NVector2(dist_x * lhs_force, dist_y * lhs_force);
					colliderPush[j] -= new NVector2(dist_x * rhs_force, dist_y * rhs_force);

					//Debug.Log(i + " " + j + " " + dir_x + " " + dir_y + " " + lhs_force + " " + rhs_force);
				}

				// apply line colliders
				if (lhs.isFixed > 0) { continue; }

				for(int j = 0; j < lineColliders.Length; j++)
				{
					var rhs = lineColliders[j];

					var dist_x = rhs.center.x - colliderPositions[i].x;
					var dist_y = rhs.center.y - colliderPositions[i].y;

					var length_dist = rhs.lengthAxis.x * dist_x + rhs.lengthAxis.y * dist_y; // project on length axis
					var length_intersection = rhs.length - MathNFloat.Abs(length_dist);
					if (length_intersection + lhs_radius < 0) // no intersection on width axis - no collision
						continue;

					var width_dist = rhs.widthAxis.x * dist_x + rhs.widthAxis.y * dist_y; // project on width axis
					var width_intersection = rhs.width - MathNFloat.Abs(width_dist);
					if (width_intersection + lhs_radius < 0) // no intersection on height axis - no collision
						continue;

					if (width_intersection < 0 && length_intersection < 0)
					{
						// final check in case circle is near the corner
						var total_intersection = NFloat.Distance(width_intersection, length_intersection);
						if (total_intersection < lhs_radius)
							continue;
					}

					if (width_intersection <= length_intersection)
					{
						// push out on width axis
						colliderPush[i] -= strength * (width_intersection + lhs.radius) * MathNFloat.Sign(width_dist) * rhs.widthAxis;
					}
					else
					{
						// push out on length axis
						colliderPush[i] -= strength * (length_intersection + lhs.radius) * MathNFloat.Sign(length_dist) * rhs.lengthAxis;
					}
				}
			}

			/* update positions on end of N2 */
			for(int i = 0; i < colliders.Length; i++)
			{
				var old = colliders[i];
				if (old.isFixed > 0) { continue; }

				// update push.

				var result = new CollisionResult();
				result.position = colliderPositions[i] + colliderPush[i];
				colliderPush[i] = new NVector2();

				// check level bounds.
				PlayableArea.Clamp(ref result.position, old.radius);

				// velocity can be modified in case body was pushed around. determine maximum velocity body can gain from the pushing.
				result.velocity = (result.position - old.position) * inverseTimeAdjustedStrength;
				var newVelocityMagnitude = result.velocity.magnitude;

				// apply drag, cap with max velocity
				var newVelocityMagnitudeUpdated = MathNFloat.Min
				(
					newVelocityMagnitude * dragPerPass - constantDrag,
					old.velocity.magnitude + maxPushVelocityGain
				);

				// constant drag may push updated velocity below zero, cap.
				if (newVelocityMagnitudeUpdated < 0) { newVelocityMagnitudeUpdated = 0; }

				// modify velocity if needed.
				if (newVelocityMagnitude != newVelocityMagnitudeUpdated)
				{
					result.velocity *= newVelocityMagnitudeUpdated / newVelocityMagnitude;
				}

				collisionResults[i] = result;
			}
		}
	}

	#endregion Run collision

	/// <summary>
	/// Apply new collider data to objects in the simulator.
	/// </summary>
	void ApplyColliderList(ArrayBuffer<Unit> allObjects)
	{
		var cnt = allColliders.Count;
		var colliders = allColliders.buffer;
		var objects = allObjects.buffer;

		for(int i = 0; i < cnt; i++)
		{
			ref var collider = ref colliders[i];
			if (collider.isFixed > 0) { continue; }

			var obj = objects[collider.id];
			obj.position = collider.position;
			obj.velocity = collider.velocity;
		}
	}
}

