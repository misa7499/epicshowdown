using System.Collections.Generic;
using Unity.Collections;
using static NFloat;

public static class Rotations
{
	public const int count = 64;
	static readonly NVector2[] rotations;

	static Rotations()
	{
		rotations = new NVector2[count];
		rotations[0] = new NVector2(1, 0);
		rotations[count-1] = new NVector2(-1, 0);

		for (int i = 1; i < count - 1; i += 2)
		{
			var angle = i * MathNFloat.PI / (count - 2);
			rotations[i] = new NVector2(MathNFloat.Cos(angle), MathNFloat.Sin(angle));
			rotations[i+1] = new NVector2(MathNFloat.Cos(-angle), MathNFloat.Sin(-angle));
		}
	}

	public static NVector2 GetRotation(int i)
	{
		return rotations[i];
	}

	public static NVector2 GetRandomRotation(int i)
	{
		return rotations[i % count];
	}

	public static NVector2 GetRotation(in NVector2 dir, int i)
	{
		var rotated = new NVector2();
		rotated.x.value = (dir.x.value * rotations[i].x.value - dir.y.value * rotations[i].y.value) >> DecimalBits;
		rotated.y.value = (dir.x.value * rotations[i].y.value + dir.y.value * rotations[i].x.value) >> DecimalBits;
		return rotated;
	}

	public static NVector2 GetRandomDirection()
	{
		var dir = rotations[UnityEngine.Random.Range(0, count - 1)];
		return new NVector2(dir.x, dir.y);
	}

	public static IEnumerable<NVector2> GetRadial(this NVector2 pos, int count, NFloat dist, NFloat offset)
	{
		offset = offset * MathNFloat.PI / 180; // convert from angles to radins.

		for (int i = 0; i < count; i++)
		{
			var angle = i * 2 * MathNFloat.PI / count + offset;
			yield return new NVector2(pos.x + MathNFloat.Cos(angle) * dist, pos.y + MathNFloat.Sin(angle) * dist);
		}
	}
}