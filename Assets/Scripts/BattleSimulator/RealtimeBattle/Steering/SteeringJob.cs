using System.Diagnostics;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Profiling;

public class SteeringJob
{
	public struct SteeringRequest
	{
		public Unit unit;
		public TargetInfo target;
		public bool MoveToAttack;
	}

	public struct SteeringRequestData
	{
		public int id;
		public NFloat speed;
		public NFloat radius;
		public NFloat mass;
		public NVector2 position;
		public NVector2 lookDirection;
		public NFloat changeDirectionPenalty;
		public OwnerId owner;
		public NVector2 targetPosition;
		public NFloat targetRadius;
		public int hasCollision;
		public int targetId;

		public NVector2 DirectionToTarget => NVector2.FromTo(position, targetPosition);
	}

	public struct ObstacleData
	{
		public int id;
		public NFloat x, y;
		public NFloat radius;
		public NFloat influenceRadius;
		public NFloat influenceRadiusSquared;
		public NFloat score;
		public OwnerId owner;
		public int isAggroTarget;
	}

	public struct ObstacleDataParsed
	{
		public NFloat x, y;
		public NFloat radius;
		public NFloat radiusSquared;
		public NFloat f_x;
		public NFloat f_y;
		public NFloat f_sqr;
		public NFloat score;
	}

	public struct Result
	{
		public NFloat positionDelta;
		public NVector2 newLookDirection;
		public NVector2 newPosition;
	}

	public struct SteeringDirectionData
	{
		public NFloat x;
		public NFloat y;
		public NFloat penalty;
	}

	readonly ArrayBuffer<SteeringRequest> recievedRequests = new ArrayBuffer<SteeringRequest>();
	readonly ArrayBuffer<Unit> steerAwayUnits = new ArrayBuffer<Unit>();

	public void RequestSteering(Unit unit, TargetInfo target, bool moveToAttack)
	{
		recievedRequests.Add(new SteeringRequest()
		{
			target = target,
			unit = unit,
			MoveToAttack = moveToAttack
		});
	}

	public void Run(BattleSimulator simulator, NFloat dT)
	{
		// sanity check.
		if (recievedRequests.Count == 0)
			return;

		// facts
		Profiler.BeginSample("GetRequestsData");
		var collisionSettings = simulator.CollisionJob.Settings;
		var requests = GetRequestsData(collisionSettings.ChangeDirectionPenalty);
		var obstaclesData = GetObstaclesData(collisionSettings.MoveThreshold);
		var obstaclesDataParsed = new NativeArray<ObstacleDataParsed>(requests.Length * obstaclesData.Length, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		var obstaclesDataEnd = new NativeArray<int>(requests.Length, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

		// prepare for main job by parsing
		var parseJob = new ParseObstaclesJob()
		{
			combinedRadiusFactor = collisionSettings.SteeringRadiusFactor,
			enemyAttractionScore = collisionSettings.EnemyAttractionScore,
			requests = requests,
			obstaclesData = obstaclesData,
			output = obstaclesDataParsed,
			obstaclesDataEnd = obstaclesDataEnd
		};
		Profiler.EndSample();

		parseJob.Run();

		// do the pathfinding
		Profiler.BeginSample("Pathfinding");
			var moveDirections = requests.GetDirections(simulator);
		Profiler.EndSample();

		// run the main job!
		var job = new Job()
		{
			dT = dT,
			requests = requests,
			moveDirections = moveDirections,
			PlayableArea = simulator.PlayableArea.Expand(2),
			obstacleCount = obstaclesData.Length,
			goodEnoughThreshold = collisionSettings.GoodEnoughThreshold,
			tooHardThreshold = collisionSettings.TooHardThreshold,
			maxRaycastDist = collisionSettings.MaxRaycastDist,
			parsedObstacleData = parseJob.output,
			rotations = GetDirectionsData(collisionSettings.AwayFromTargetPenalty),
			results = new NativeArray<Result>(recievedRequests.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory),
			parsedObstacleDataEnd = obstaclesDataEnd,
		};

		job.Run(job.requests.Length);

		// apply results
		for(int i = 0; i < job.results.Length; i++)
		{
			recievedRequests[i].unit.ApplyMove(job.results[i], dT);
		}

		// dispose native memory
		job.requests.Dispose();
		job.results.Dispose();
		job.parsedObstacleData.Dispose();
		job.rotations.Dispose();
		job.moveDirections.Dispose();
		job.parsedObstacleDataEnd.Dispose();
		parseJob.obstaclesData.Dispose();

		// dispose managed memory
		recievedRequests.Clear();
		steerAwayUnits.Clear();

		// local methods for parsing requests into data
		NativeArray<SteeringRequestData> GetRequestsData(NFloat changeDirectionPanelty)
		{
			var requestsParsed = new NativeArray<SteeringRequestData>(recievedRequests.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
			for(int i = 0; i < requestsParsed.Length; i++)
			{
				var request = recievedRequests[i];
				requestsParsed[i] = new SteeringRequestData()
				{
					id = request.unit.Id,
					hasCollision = request.unit.HasCollision ? 1 : 0,
					speed = request.unit.MovementSpeed,
					radius = request.unit.radius,
					mass = request.unit.mass,
					lookDirection = request.unit.lookDirection,
					changeDirectionPenalty = request.unit.directionChangeFatigue + changeDirectionPanelty,
					position = request.unit.position,
					owner = request.unit.Owner,

					targetId = request.target.targetId,
					targetRadius = request.MoveToAttack ? request.target.radius + request.unit.AttackRange : request.target.radius,
					targetPosition = request.target.position,
				};
			}
			return requestsParsed;
		}

		NativeArray<ObstacleData> GetObstaclesData(NFloat moveThreshold)
		{
			// first, get units to steer away from
			var units_cnt = simulator.AllUnits.Count;
			var units_bfr = simulator.AllUnits.buffer;
			for(int i = 0; i < units_cnt; i++)
			{
				var rhs = units_bfr[i];
				if (rhs.influenceRadius <= NFloat.Zero) { continue; }
				if (rhs is Minion && rhs.TimeAlive == 0) { continue; }
				if (rhs.lastPositionDiff < moveThreshold) { steerAwayUnits.Add(rhs); }
			}

			// allocate memory for stuff to steer away from.
			var steerAwayData = new NativeArray<ObstacleData>(steerAwayUnits.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
			for(int i = 0; i < steerAwayUnits.Count; i++)
			{
				var unit = steerAwayUnits[i];
				steerAwayData[i] = new ObstacleData()
				{
					id = unit.Id,
					x = unit.position.x,
					y = unit.position.y,
					radius = unit.radius,
					influenceRadius = unit.influenceRadius,
					influenceRadiusSquared = unit.influenceRadius.Squared,
					owner = unit.Owner,
					isAggroTarget = unit.IsAggroTarget ? 1 : 0,
					score = unit.mass * collisionSettings.UnitAvoidanceRatio * (1 - unit.lastPositionDiff / moveThreshold) / unit.influenceRadius,
				};
			}

			return steerAwayData;
		}

		NativeArray<SteeringDirectionData> GetDirectionsData(NFloat awayFromTargetPenalty)
		{
			var directions = new NativeArray<SteeringDirectionData>(Rotations.count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
			for(int i = 0; i < directions.Length; i++)
			{
				var dir = Rotations.GetRotation(i);
				directions[i] = new SteeringDirectionData()
				{
					x = dir.x,
					y = dir.y,
					penalty = (1 - dir.x) * awayFromTargetPenalty,
				};
			}
			return directions;
		}
	}

	[BurstCompile]
	public struct Job : IJobParallelFor
	{
		[ReadOnly] public NFloat dT;
		[ReadOnly] public NRect PlayableArea;

		[ReadOnly] public NFloat goodEnoughThreshold;
		[ReadOnly] public NFloat tooHardThreshold;
		[ReadOnly] public NFloat maxRaycastDist;
		[ReadOnly] public int obstacleCount;

		[ReadOnly] public NativeArray<SteeringRequestData> requests;
		[ReadOnly] public NativeArray<NVector2> moveDirections;
		[ReadOnly] public NativeArray<ObstacleDataParsed> parsedObstacleData;
		[ReadOnly] public NativeArray<SteeringDirectionData> rotations;
		[ReadOnly] public NativeArray<int> parsedObstacleDataEnd;

		[WriteOnly] public NativeArray<Result> results;

		public void Execute(int index)
		{
			var position = requests[index].position;
			var target = requests[index].targetPosition;

			// practically on the same spot, don't do anything
			var dist = NVector2.Distance(target, position);
			if (dist <= NFloat.Tousandth)
			{
				results[index] = new Result()
				{
					newPosition = position,
					newLookDirection = requests[index].lookDirection,
					positionDelta = 0
				};
				return;
			}

			// already reached, look at the target, don't move
			var approach = requests[index].targetRadius;
			var moveDirection = moveDirections[index];

			if (dist < approach)
			{
				results[index] = new Result()
				{
					newPosition = position,
					newLookDirection = moveDirection,
					positionDelta = 0
				};
				return;
			}

			// get clamped speed
			var speed = requests[index].speed * dT;
			if (dist - approach < speed) { speed = dist - approach; }

			// steer away from obstacles if needed.
			if (requests[index].hasCollision > 0)
			{
				moveDirection = SteerAwayFromObstacles(index, dist, moveDirection);
			}

			// final result!
			results[index] = new Result()
			{
				newPosition = position + moveDirection * speed,
				newLookDirection = moveDirection,
				positionDelta = speed
			};
		}

		NVector2 SteerAwayFromObstacles(int index, NFloat dist, NVector2 direction)
		{
			var changeDirectionPenalty = requests[index].changeDirectionPenalty;

			//my data
			var lhs = requests[index];
			var position = lhs.position;
			var x = position.x;
			var y = position.y;
			var myRadius = lhs.radius;
			var myMass = lhs.mass;
			var myId = lhs.id;
			var targetId = lhs.targetId;
			var myDirection = lhs.lookDirection * changeDirectionPenalty;

			var startIndex = index == 0 ? 0 : parsedObstacleDataEnd[index - 1];
			var endIndex = parsedObstacleDataEnd[index];

			// set up facts
			var raycastDist = MathNFloat.Min(maxRaycastDist, dist);
			var raycastDistInv = raycastDist.Inverse;
			var d_sqr = raycastDist.Squared;

			// check all rotations and find the least obstructed one.
			var bestDir = direction;
			var bestScore = tooHardThreshold;
			for(int i = 0; i < rotations.Length; i++)
			{
				// away from target penaly
				NFloat penalty = rotations[i].penalty;
				if (penalty >= bestScore) { continue; }
				// --

				var rotDir = new NVector2()
				{
					x = direction.x * rotations[i].x - direction.y * rotations[i].y,
					y = direction.y * rotations[i].x + direction.x * rotations[i].y,
				};

				// change direction penalty
				penalty += changeDirectionPenalty - NVector2.Dot(myDirection, rotDir);
				if (penalty >= bestScore) { continue; }
				// --

				// check if out of bounds.
				var raycastTarget = position + rotDir * raycastDist;
				if (!PlayableArea.IsInside(raycastTarget))
				{
					continue;
				}

				// cached distance to target for loop for loop
				var d_x = raycastTarget.x - x;
				var d_y = raycastTarget.y - y;

				// score obstacles on this direction
				for(int j = startIndex; j < endIndex; j++)
				{
					var obstacle = parsedObstacleData[j];
					if (d_sqr <= obstacle.f_sqr)
						continue;

					// forms quadratic eq, checks if results exist_
					var b = obstacle.f_x * d_x + obstacle.f_y * d_y;
					if (b >= 0)
						continue;

					// final check if the line and circle intersect in the given interval.
					var c = obstacle.f_sqr - obstacle.radiusSquared;
					if (d_sqr * c >= b*b)
						continue;

					// line and circle intersect, just get the distance.
					var intersection = (d_y*obstacle.x - d_x*obstacle.y + raycastTarget.x*y - raycastTarget.y*x) * raycastDistInv;
					if (intersection < 0) { intersection = -intersection; }

					penalty += obstacle.score * (1 - intersection / obstacle.radius);
					if (penalty >= bestScore) { break; }
				}

				if (penalty >= bestScore) { continue; }
				// --

				// this direction is best so far!
				bestDir = rotDir;
				bestScore = penalty;

				// if this was good enough, abort the search.
				if (penalty <= goodEnoughThreshold)
				{
					break;
				}
			}

			return bestDir;
		}
	}

	[BurstCompile]
	public struct ParseObstaclesJob : IJob
	{
		[ReadOnly] public NFloat combinedRadiusFactor;
		[ReadOnly] public NFloat enemyAttractionScore;
		[ReadOnly] public NativeArray<SteeringRequestData> requests;
		[ReadOnly] public NativeArray<ObstacleData> obstaclesData;

		[WriteOnly] public NativeArray<ObstacleDataParsed> output;
		[WriteOnly] public NativeArray<int> obstaclesDataEnd;

		public void Execute()
		{
			int writeIndex = 0;
			for(int index = 0; index < requests.Length; index++)
			{
				var lhs = requests[index];
				var lhs_mass_inv = lhs.mass.Inverse;

				var d_sqr = NVector2.SqrDistance(lhs.position, lhs.targetPosition);

				for(int i = 0; i < obstaclesData.Length; i++)
				{
					var obstacle = obstaclesData[i];
					var data = new ObstacleDataParsed();
					data.f_x = lhs.position.x - obstacle.x;
					data.f_y = lhs.position.y - obstacle.y;
					data.f_sqr = NFloat.SqrSum(data.f_x, data.f_y);

					if (data.f_sqr >= d_sqr) { continue; }
					if (data.f_sqr >= obstacle.influenceRadiusSquared) { continue; }
					if (obstacle.id == lhs.id || obstacle.id == lhs.targetId) { continue; }

					var dist = data.f_sqr.Sqrt();
					data.radius = (lhs.radius + obstacle.radius) * combinedRadiusFactor;
					data.radiusSquared = data.radius.Squared;
					data.x = obstacle.x;
					data.y = obstacle.y;
					data.score = (obstacle.influenceRadius - dist) * obstacle.score * lhs_mass_inv;

					if (lhs.owner != obstacle.owner && obstacle.isAggroTarget > 0)
					{
						data.score *= enemyAttractionScore;
					}

					output[writeIndex++] = data;
				}

				// delimiter for the next unit
				obstaclesDataEnd[index] = writeIndex;
			}
		}
	}
}