using System;

[Serializable]
public class ProjectileSettings
{
	public string ProjectileName = "Arrow";

	/// <summary>
	/// Projectile speed.
	/// </summary>
	public NFloat Speed = 10;

	/// <summary>
	/// From which height is this projectile fired?
	/// </summary>
	public NFloat InitialHeight;

	/// <summary>
	/// Can this projectile damage multiple units by passing though them?
	/// </summary>
	public bool PassThrough;

	/// <summary>
	/// Width of the projectile when doing the pass through.
	/// </summary>
	[Indent, ShowIf(nameof(PassThrough))] public NFloat PassThroughRadius;

	/// <summary>
	/// Additional distance traveled beyond normal attack range, if needed.
	/// </summary>
	[Indent, ShowIf(nameof(PassThrough))] public NFloat PassThroughRange;
}