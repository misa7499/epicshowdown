using System;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : SimObject2D
{
	public NVector3 position;
	public NVector3 lookDirection;
	public NVector3 StartPosition;
	public Stat MovementSpeed;
	public NFloat Range;

	public bool PassThrough;
	public Stat PassThroughRadius;
	HashSet<Unit> passThroughTargets;

	public TargetInfo Target;
	public Action<Projectile, TargetInfo> OnReached;
	public NFloat DistanceTraveled;
	public Stat Damage;

	// take evalation into acount
	public override Vector3 Position3D() => position.Position3D();
	public override Vector3 Direction3D() => lookDirection.Direction3D();
	public override float Height() => 0;

	public Projectile(Unit parent, NFloat range, ProjectileSettings settings, TargetInfo target, Action<Projectile, TargetInfo> onReached) : base(parent)
	{
		MovementSpeed.BaseValue = settings.Speed;
		Name = settings.ProjectileName;
		PrefabPath = ViewPaths.Projectiles;

		OnReached = onReached;
		PassThrough = settings.PassThrough;
		Range = range;

		// 3d start position
		StartPosition = new NVector3(parent.position.x, parent.position.y, settings.InitialHeight);

		if (settings.PassThrough)
		{
			// go straigh ahead at position
			Target = StartPosition + (target.position - parent.position).normalized * (range + settings.PassThroughRange);
			PassThroughRadius.BaseValue = settings.PassThroughRadius;
			passThroughTargets = new HashSet<Unit>();
		}
		else
		{
			// follow target
			Target = target;
		}

		// initial state.
		position = StartPosition;
		lookDirection = StartPosition - target;
	}

	/// <summary>
	/// Move linearly to target and check approach.
	/// </summary>
	protected override void OnUpdate(NFloat dT)
	{
		base.OnUpdate(dT);

		// skip first frame
		if (TimeAlive == dT) { return; }

		// move clamped by target's body.
		var speed = MovementSpeed * dT;
		var movement = MoveRaw(Target, speed);
		DistanceTraveled += movement;

		// handle pass through
		if (PassThrough)
		{
			foreach(var creep in this.AllEnemiesInRadius(position, PassThroughRadius))
				if (passThroughTargets.Add(creep))
					OnReached.Dispatch(this, creep);
		}

		// abort when reaching target
		if (movement < speed * NFloat.Half)
		{
			OnReached.Dispatch(this, Target);
			Deactivate();
		}
	}

	public NFloat MoveRaw(TargetInfo target, NFloat speed)
	{
		var approach = target.radius + NFloat.Tenth;

		var dist = NVector3.Distance(target, position);
		if (dist > NFloat.Tousandth)
		{
			// look at the target
			lookDirection = (target - position) / dist;

			if (dist < approach)
			{
				return 0;
			}
			// reached approach limit
			else if (dist - speed > approach)
			{
				position += lookDirection * speed;
				return speed;
			}
			else
			{
				position += lookDirection * (dist - approach);
				return dist - approach;
			}
		}

		return NFloat.Zero;
	}
}