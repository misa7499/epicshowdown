using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Actions extended from this have 'executed' flag and can be used in 'yield return' instructions.
/// </summary>
public abstract class BattleAction : IEnumerator
{
	public readonly ActionId Id;
	bool executed;

	public BattleAction(ActionId id)
	{
		Id = id;
	}

	public void Apply(BattleController battle)
	{
		try { OnApply(battle); }
		catch(Exception e) { Debug.LogException(e); }

		executed = true;
	}

	protected abstract void OnApply(BattleController battle);

	/* Wait until executed. */
	bool IEnumerator.MoveNext() => !executed;
	object IEnumerator.Current => null;
	void IEnumerator.Reset() {}
}