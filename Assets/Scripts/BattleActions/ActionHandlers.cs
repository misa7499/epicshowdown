using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collection of active handlers which can recieve AnonymousActions.
/// </summary>
public abstract class IActionHandler
{
	/// <summary>
	/// Human readable Id of the action.
	/// </summary>
	public readonly ActionId ActionId;

	/// <summary>
	/// Unique id of the action handler, given in the order of initialization.
	/// </summary>
	protected readonly long handlerId;

	protected IActionHandler(ActionId actionId)
	{
		ActionId = actionId;
		handlerId = AllHandlers.AddHandler(this);
	}

	/// <summary>
	/// Derived classes can restrict data objects.
	/// </summary>
	public abstract AnonymousAction CreateAndScheduleAction(IBattleServer server, object target);

	/// <summary>
	/// Derived classes can restrict data objects.
	/// </summary>
	protected abstract bool Execute(object target);

	/// <summary>
	/// Return human readable id of the action.
	/// </summary>
	public override string ToString() => ActionId.ToString();

	/// <summary>
	/// Simple data carrier with handlerID and target object.
	/// </summary>
	public class AnonymousAction : BattleAction
	{
		readonly object target;
		readonly long handlerId;
		public IActionHandler Handler => AllHandlers.FindHandler(handlerId);

		public AnonymousAction(ActionId id, object target, long handlerId) : base(id)
		{
			this.target = target;
			this.handlerId = handlerId;
		}

		/// <summary>
		/// Find handler with the specified id and execute action.
		/// </summary>
		protected override void OnApply(BattleController battle)
		{
			Handler?.Execute(target);
		}
	}

	/// <summary>
	/// Static collection of all created handlers.
	/// </summary>
	static class AllHandlers
	{
		public static readonly IdGenerator idGenerator = new IdGenerator(1);
		public static readonly Dictionary<long, IActionHandler> all = new Dictionary<long, IActionHandler>();

		public static long AddHandler(IActionHandler handler)
		{
			var newId = AllHandlers.idGenerator.NextId();
			all.Add(newId, handler);
			return newId;
		}

		public static IActionHandler FindHandler(long handlerId)
		{
			return AllHandlers.all.TryGetValue(handlerId, out var handler)
				? handler : null;
		}
	}
}

/// <summary>
/// For actionHandlers which take no targes.
/// </summary>
public class ActionHandler : IActionHandler
{
	public event Action OnApplied
	{
		add { onApplied += value; }
		remove { onApplied -= value; }
	}

	Action onApplied;
	readonly Func<Validation> evaluate;
	readonly Action execute;

	public ActionHandler(ActionId id, Action execute, Func<Validation> evaluate = null) : base(id)
	{
		this.evaluate = evaluate;
		this.execute = execute;
	}

	public AnonymousAction CreateAndScheduleAction(IBattleServer server)
	{
		var validationResult = CanBeExecuted();
		if (!validationResult)
		{
			Debug.LogWarning($"Failed to execute {this}: {validationResult}");
			return null;
		}

		return server.ScheduleAction(
			new AnonymousAction(ActionId, null, handlerId)
		);
	}

	public Validation CanBeExecuted()
	{
		return evaluate == null || evaluate();
	}

	public bool Execute()
	{
		if (CanBeExecuted())
		{
			execute();
			onApplied.Dispatch();
			return true;
		}

		return false;
	}

	protected override bool Execute(object target) => Execute();
	public override AnonymousAction CreateAndScheduleAction(IBattleServer server, object target) => CreateAndScheduleAction(server);
}

public class ActionHandler<T> : IActionHandler
{
	public event Action<T> OnApplied
	{
		add { onApplied += value; }
		remove { onApplied -= value; }
	}

	Action<T> onApplied;
	readonly Func<T, Validation> evaluate;
	readonly Action<T> execute;

	public ActionHandler(ActionId id, Action<T> execute, Func<T, Validation> evaluate = null) : base(id)
	{
		this.evaluate = evaluate;
		this.execute = execute;
	}

	public AnonymousAction CreateAndScheduleAction(IBattleServer server, T target)
	{
		var validationResult = CanBeExecuted(target);
		if (!validationResult)
		{
			Debug.LogWarning($"Failed to execute {ActionId}: {validationResult}");
			return null;
		}

		return server.ScheduleAction(
			new AnonymousAction(ActionId, target, handlerId)
		);
	}

	public Validation CanBeExecuted(T target)
	{
		return evaluate(target);
	}

	public bool ExecuteMultiple(IEnumerable<T> targets)
	{
		var success = false;
		foreach(var target in targets)
		{
			if (Execute(target))
			{
				success = true;
			}
			else
			{
				break;
			}
		}
		return success;
	}

	public bool Execute(T target)
	{
		if (CanBeExecuted(target))
		{
			execute(target);
			onApplied.Dispatch(target);
			return true;
		}

		return false;
	}

	protected override bool Execute(object target) => target is T typedTarget ? Execute(typedTarget) : false;
	public override AnonymousAction CreateAndScheduleAction(IBattleServer server, object target) =>
		target is T typedTarget ? CreateAndScheduleAction(server, typedTarget) : null;
}

public static class ActionHandlerUtil
{
	public static BattleAction ScheduleAction(this IBattleServer server, ActionHandler handler) =>
		handler.CreateAndScheduleAction(server);

	public static BattleAction ScheduleAction<T>(this IBattleServer server, ActionHandler<T> handler, T target) =>
		handler.CreateAndScheduleAction(server, target);
}