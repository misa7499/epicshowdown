using System.Collections;

public struct Validation
{
	public bool Value;
	public ValidationMessage Message;

	public override string ToString() => Message.ToString();

	public static Validation validate(bool value, string msg)
		=> new Validation { Value = value, Message = new ValidationMessage(msg) };

	public static Validation validate(bool value, string msg, object obj1)
		=> new Validation { Value = value, Message = new ValidationMessage(msg, obj1) };

	public static Validation validate(bool value, string msg, object obj1, object obj2)
		=> new Validation { Value = value, Message = new ValidationMessage(msg, obj1, obj2) };

	public static Validation validate(bool value, string msg, object obj1, object obj2, object obj3)
		=> new Validation { Value = value, Message = new ValidationMessage(msg, obj1, obj2, obj3) };

	public static implicit operator Validation(bool value) => validate(value, value ? "Success" : "Error message missing");
	public static implicit operator bool(Validation validation) => validation.Value;

	public static Validation operator &(Validation lhs, Validation rhs) => lhs.Value ? rhs : lhs;
	public static Validation operator |(Validation lhs, Validation rhs) => lhs.Value ? lhs : rhs;

	public static bool operator true(Validation lhs) => lhs.Value;
	public static bool operator false(Validation lhs) => !lhs.Value;

	/* Common validations */
	public static Validation validateNotNull(object value) => validate(value != null, "Argument is null");
	public static Validation validateNotEmpty(ICollection list) => validate(list != null && list.Count > 0, "Collection is null or empty!");
}

public struct ValidationMessage
{
	string msg;
	int paramCount;
	object param1, param2, param3;

	public ValidationMessage(string msg) { this.msg = msg; paramCount = 0; param3 = param2 = param1 = null; }
	public ValidationMessage(string msg, object obj1) { this.msg = msg; paramCount = 1; param3 = param2 = null; this.param1 = obj1; }
	public ValidationMessage(string msg, object obj1, object obj2) { this.msg = msg; paramCount = 2; param3 = null; this.param2 = obj2; this.param1 = obj1; }
	public ValidationMessage(string msg, object obj1, object obj2, object obj3) { this.msg = msg; paramCount = 3; this.param3 = obj3; this.param2 = obj2; this.param1 = obj1; }

	public override string ToString()
	{
		switch(paramCount)
		{
			default:
				return msg;
			case 1:
				return string.Format(msg, param1);
			case 2:
				return string.Format(msg, param1, param2);
			case 3:
				return string.Format(msg, param1, param2, param3);
		}
	}
}