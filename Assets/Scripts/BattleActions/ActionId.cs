public enum ActionId
{
	Undefined = 0,
	Time = 1,
	TurnStarted = 2,
	TurnEnded = 3,
	PlayerTurnStarted = 4,
	PlayerTurnEnded = 5,
	MoveUnit = 6,
	PlaceUnit = 7,
	Sell = 8,
}