using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SelectionSet<T> : IEnumerable<T> where T : class
{
	/* State */
	HashSet<T> set = new HashSet<T>();

	/* Events */
	readonly Action<T> OnSelect;
	readonly Action<T> OnDeselect;

	public SelectionSet(Action<T> onSelect = null, Action<T> onDeselect = null)
	{
		OnSelect = onSelect;
		OnDeselect = onDeselect;
	}

	public int Count => set.Count;
	public List<T> ToList() => new List<T>(set);
	public T[] ToArray()
	{
		var newArray = new T[set.Count];
		set.CopyTo(newArray);
		return newArray;
	}

	public void Clear()
	{
		foreach(var obj in set)
		{
			NotifyDeselection(obj);
		}
		set.Clear();
	}

	public bool Contains(T obj)
	{
		return obj != null && set.Contains(obj);
	}

	public void Add(T obj)
	{
		if (obj != null && !set.Contains(obj))
		{
			NotifySelection(obj);
			set.Add(obj);
		}
	}

	public void Add(IEnumerable<T> addSet)
	{
		foreach(var obj in addSet)
		{
			if (obj != null && set.Add(obj)) { NotifySelection(obj); }
		}
	}

	public void Remove(IEnumerable<T> removeSet)
	{
		foreach(var obj in removeSet)
		{
			if (set.Remove(obj)) { NotifyDeselection(obj); }
		}
	}

	public void Remove(T obj)
	{
		if (obj != null && set.Remove(obj))
		{
			NotifyDeselection(obj);
		}
	}

	public void ReplaceWith(IEnumerable<T> newSet)
	{
		var deselection = new List<T>(set);

		foreach(var obj in newSet)
		{
			if (obj != null && !set.Contains(obj)) 
			{ 
				NotifySelection(obj); 
				set.Add(obj);
			}
			deselection.Remove(obj);
		}

		foreach(var obj in deselection)
		{
			NotifyDeselection(obj);
			set.Remove(obj);
		}
	}

	public void ReplaceWith(T selected)
	{
		foreach(var obj in set)
		{
			if (obj != selected) { NotifyDeselection(obj); }
		}
		set.Clear();

		if (selected != null && !set.Contains(selected)) 
		{ 
			NotifySelection(selected); 
			set.Add(selected);
		}
	}

	private void NotifySelection(T obj) => OnDeselect.Dispatch(obj); 
	private void NotifyDeselection(T obj) => OnSelect.Dispatch(obj);

	public IEnumerator<T> GetEnumerator()
	{
		return set.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return set.GetEnumerator();
	}
}