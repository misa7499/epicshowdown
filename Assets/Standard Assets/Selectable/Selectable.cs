﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Selectable<T> where T : class
{
	/* Events */
	public event Action<T> SelectionChanged;
	readonly Action<T> OnSelect;
	readonly Action<T> OnDeselect;
	public Selectable(Action<T> onSelect = null, Action<T> onDeselect = null)
	{
		OnSelect = onSelect;
		OnDeselect = onDeselect;
	}

	/* State */
	T selected;
	public T Value
	{
		get { return selected; }
		set
		{
			if (selected != value)
			{
				var deselected = selected;
				selected = value;

				/* Notify all */
				if (deselected != null) { OnDeselect.Dispatch(deselected); }
				if (selected != null) { OnSelect.Dispatch(selected); }
				SelectionChanged.Dispatch(selected);
			}
		}
	}

	/* Convertable to selected value */
	public static implicit operator T (Selectable<T> sel) => sel.selected;
	public static bool operator ==(Selectable<T> sel, T val) => sel.Value == val;
	public static bool operator !=(Selectable<T> sel, T val) => sel.Value != val;
	public override bool Equals(object obj) => base.Equals(obj);
	public override int GetHashCode() => base.GetHashCode();
}