using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public abstract class Script<T> : SerializedScript where T : ScriptObject
{
	static readonly Dictionary<string, string> compilationOutput = new Dictionary<string, string>();
	static readonly Dictionary<string, string> sentForCompilation = new Dictionary<string, string>();
	static readonly Dictionary<string, T> compilationCache = new Dictionary<string, T>();

	private T script;
	public T ScriptObject
	{
		get
		{
			if (script == null)
			{
				string typeName;
				if (sentForCompilation.TryGetValue(Source, out typeName) && !compilationCache.TryGetValue(Source, out script))
				{
					script = ScriptCompiler.CreateInstanceOf<T>(typeName);
					compilationCache.Add(Source, script);
				}
			}
			return script;
		}
	}

	public override sealed void Recompile()
	{
		if (string.IsNullOrEmpty(Source))
		{
			Out = "";
			script = null;
			return;
		}

		if (!sentForCompilation.ContainsKey(Source))
		{
			script = null;
			var typeName = $"{typeof(T).Name}_{GetTypeName(Source)}";
			var scr = $@"public class {typeName} : {typeof(T).Name}
						{{ 
							public override void Execute() 
							{{
								try {{ {Source} }}
								catch(System.Exception e) {{ Debug.LogException(e); }}
							}} 
						}}";
					

			ScriptCompiler.Instance.Execute(scr, out Out);
			sentForCompilation.Add(Source, typeName);
			compilationOutput.Add(Source, Out);
		}
		else
		{
			compilationOutput.TryGetValue(Source, out Out);
			compilationCache.TryGetValue(Source, out script);
		}
	}	

	static readonly HashAlgorithm hasher = new MD5CryptoServiceProvider();
	public static string GetTypeName(string src)
	{
		var hash = new StringBuilder();
		byte[] bytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(src));
		for (int i = 0; i < bytes.Length; i++) { hash.Append(bytes[i].ToString("x2")); }
		return hash.ToString();
	}
}

/// <summary>
/// Data part, separated to implement property drawer.
/// </summary>
public abstract class SerializedScript : ISerializationCallbackReceiver
{
	public string Source;
	public string Out;

	public void OnBeforeSerialize() {}
	public void OnAfterDeserialize() => Recompile();

	public abstract void Recompile();
}

public class ScriptObject
{
	public virtual void Execute() {}
}