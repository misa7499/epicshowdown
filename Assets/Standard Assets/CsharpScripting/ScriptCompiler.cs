﻿using Mono.CSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

public class ScriptCompiler : Singleton<ScriptCompiler>
{
    const string DefaultUsingsPath = "Assets/Scripts/ScriptDefaults.cs";
    private Evaluator _evaluator;
    private CompilerContext _context;
    private StringBuilder _report;

    public int ErrorsCount => _context.Report.Printer.ErrorsCount;
    public int WarningsCount => _context.Report.Printer.WarningsCount;
    public string GetReport () => _report.ToString();

    public ScriptCompiler () 
    {
        // create new settings that will *not* load up all of standard lib by default
        // see: https://github.com/mono/mono/blob/master/mcs/mcs/settings.cs

        CompilerSettings settings = new CompilerSettings { LoadDefaultReferences = false, StdLib = false };
        this._report = new StringBuilder();
        this._context = new CompilerContext(settings, new StreamReportPrinter(new StringWriter(_report)));

        this._evaluator = new Evaluator(_context);
        this._evaluator.ReferenceAssembly(Assembly.GetExecutingAssembly());

        ImportAllowedTypes(ScriptCompilerDefaults.BuiltInTypes, ScriptCompilerDefaults.AdditionalTypes, ScriptCompilerDefaults.QuestionableTypes);

        Execute(ScriptCompilerDefaults.DefaultUsings);

        if (File.Exists(DefaultUsingsPath))
        {
            Execute(File.ReadAllText(DefaultUsingsPath));
        }
    }

    /// <summary> Loads user code. Returns true on successful evaluation, or false on errors. </summary>
    public bool ExecuteFile(string path) 
	{
        return Execute(File.ReadAllText(path));
    }

    /// <summary> Loads user code. Returns true on successful evaluation, or false on errors. </summary>
    public bool Execute (string code) 
	{
        _report.Length = 0;
        try
        {
            return _evaluator.Run(code);
        }
        catch(System.Exception e)
        {
            Debug.LogException(e);
            return false;
        }
    }

    public bool Execute (string code, out string report) 
	{
        _report.Length = 0;
        try
        {
            var run = _evaluator.Run(code);
            report = GetReport();
            return run;
        }
        catch(System.Exception e)
        {
            Debug.LogException(e);
            report = e.ToString();
            return false;
        }
    }    

    public T Evaluate<T>(string code)
	{
        _report.Length = 0;
        try
        {
            return (T)_evaluator.Evaluate(code);
        }
        catch(System.Exception e)
        {
            Debug.LogException(e);
            return default(T);
        }
    }

    /// <summary> Creates new instances of types that are children of the specified type. </summary>
    public static IEnumerable<T> CreateInstancesOf<T>() => 
        AllTypes.Where(type => !(type.IsAbstract || type.IsInterface) && typeof(T).IsAssignableFrom(type)).
        Select(type => (T)Activator.CreateInstance(type));

    public static T CreateInstanceOf<T> (string name) 
    {
        try
        {
            foreach(var type in AllTypes.Where(type => !(type.IsAbstract || type.IsInterface) && typeof(T).IsAssignableFrom(type) && type.Name == name))
            {
                return (T)Activator.CreateInstance(type);
            }
        }
        catch(System.Exception e)
        {
            Debug.LogException(e);
        }

        return default(T);
    }

    public static IEnumerable<Type> AllTypes => AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => 
    {
        try { return a.GetTypes(); } catch(Exception) { return new Type[0]; } 
    });

    private void ImportAllowedTypes (params Type[][] allowedTypeArrays) {
        // expose Evaluator.importer and Evaluator.module
        var evtype = typeof(Evaluator);
        var importer = (ReflectionImporter)evtype
            .GetField("importer", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_evaluator);
        var module = (ModuleContainer)evtype
            .GetField("module", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_evaluator);

        // expose MetadataImporter.ImportTypes(Type[], RootNamespace, bool)
        var importTypes = importer.GetType().GetMethod(
            "ImportTypes", BindingFlags.Instance | BindingFlags.NonPublic, null, CallingConventions.Any,
            new Type[] { typeof(Type[]), typeof(Namespace), typeof(bool) }, null);

        foreach (Type[] types in allowedTypeArrays) {
            importTypes.Invoke(importer, new object[] { types, module.GlobalRootNamespace, false });
        }
    }
}