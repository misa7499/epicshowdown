using System;

public static class ScriptCompilerDefaults
{
	public const string DefaultUsings = 
		@"using UnityEngine;
		  using static UnityEngine.Debug;
		  using static UnityEngine.Mathf;
		  using static UnityEngine.Vector2;
		  using static UnityEngine.Vector3;
		  using static UnityEngine.Random;";

    public static readonly Type[] BuiltInTypes = new Type[] 
	{
        typeof(void),
        typeof(System.Type),
        typeof(System.Object),
        typeof(System.ValueType),
        typeof(System.Array),
        typeof(System.SByte),
        typeof(System.Byte),
        typeof(System.Int16),
        typeof(System.UInt16),
        typeof(System.Int32),
        typeof(System.UInt32),
        typeof(System.Int64),
        typeof(System.UInt64),
        typeof(System.Single),
        typeof(System.Double),
        typeof(System.Char),
        typeof(System.String),
        typeof(System.Boolean),
        typeof(System.Decimal),
        typeof(System.IntPtr),
        typeof(System.UIntPtr),
        typeof(System.Enum),
        typeof(System.Attribute),
        typeof(System.Delegate),
        typeof(System.MulticastDelegate),
        typeof(System.IDisposable),
        typeof(System.Exception),
        typeof(System.RuntimeFieldHandle),
        typeof(System.RuntimeTypeHandle),
        typeof(System.ParamArrayAttribute),
        typeof(System.Runtime.InteropServices.OutAttribute),
    };

    /// <summary>
    /// These types may be useful in scripts but they're not strictly necessary and 
    /// should be edited as desired.
    /// </summary>
    public static readonly Type[] AdditionalTypes = new Type[] 
	{
        // mscorlib System

        typeof(System.Action),
        typeof(System.Action<>),
        typeof(System.Action<,>),
        typeof(System.Action<,,>),
        typeof(System.Action<,,,>),
        typeof(System.ArgumentException),
        typeof(System.ArgumentNullException),
        typeof(System.ArgumentOutOfRangeException),
        typeof(System.ArithmeticException),
        typeof(System.ArraySegment<>),
        typeof(System.ArrayTypeMismatchException),
        typeof(System.Comparison<>),
        typeof(System.Convert),
        typeof(System.Converter<,>),
        typeof(System.DivideByZeroException),
        typeof(System.FlagsAttribute),
        typeof(System.FormatException),
        typeof(System.Func<>),
        typeof(System.Func<,>),
        typeof(System.Func<,,>),
        typeof(System.Func<,,,>),
        typeof(System.Func<,,,,>),
        typeof(System.Guid),
        typeof(System.IAsyncResult),
        typeof(System.ICloneable),
        typeof(System.IComparable),
        typeof(System.IComparable<>),
        typeof(System.IConvertible),
        typeof(System.ICustomFormatter),
        typeof(System.IEquatable<>),
        typeof(System.IFormatProvider),
        typeof(System.IFormattable),
        typeof(System.IndexOutOfRangeException),
        typeof(System.InvalidCastException),
        typeof(System.InvalidOperationException),
        typeof(System.InvalidTimeZoneException),
        typeof(System.Math),
        typeof(System.MidpointRounding),
        typeof(System.NonSerializedAttribute),
        typeof(System.NotFiniteNumberException),
        typeof(System.NotImplementedException),
        typeof(System.NotSupportedException),
        typeof(System.Nullable),
        typeof(System.Nullable<>),
        typeof(System.NullReferenceException),
        typeof(System.ObjectDisposedException),
        typeof(System.ObsoleteAttribute),
        typeof(System.OverflowException),
        typeof(System.Predicate<>),
        typeof(System.Random),
        typeof(System.RankException),
        typeof(System.SerializableAttribute),
        typeof(System.StackOverflowException),
        typeof(System.StringComparer),
        typeof(System.StringComparison),
        typeof(System.StringSplitOptions),
        typeof(System.SystemException),
        typeof(System.TimeoutException),
        typeof(System.TypeCode),
        typeof(System.Version),
        typeof(System.WeakReference),
        
        // mscorlib System.Collections
        
        typeof(System.Collections.BitArray),
        typeof(System.Collections.ICollection),
        typeof(System.Collections.IComparer),
        typeof(System.Collections.IDictionary),
        typeof(System.Collections.IDictionaryEnumerator),
        typeof(System.Collections.IEqualityComparer),
        typeof(System.Collections.IList),

        // mscorlib System.Collections.Generic

        typeof(System.Collections.IEnumerator),
        typeof(System.Collections.IEnumerable),
        typeof(System.Collections.Generic.Comparer<>),
        typeof(System.Collections.Generic.Dictionary<,>),
        typeof(System.Collections.Generic.EqualityComparer<>),
        typeof(System.Collections.Generic.ICollection<>),
        typeof(System.Collections.Generic.IComparer<>),
        typeof(System.Collections.Generic.IDictionary<,>),
        typeof(System.Collections.Generic.IEnumerable<>),
        typeof(System.Collections.Generic.IEnumerator<>),
        typeof(System.Collections.Generic.IEqualityComparer<>),
        typeof(System.Collections.Generic.IList<>),
        typeof(System.Collections.Generic.KeyNotFoundException),
        typeof(System.Collections.Generic.KeyValuePair<,>),
        typeof(System.Collections.Generic.List<>),
        
        // mscorlib System.Collections.ObjectModel

        typeof(System.Collections.ObjectModel.Collection<>),
        typeof(System.Collections.ObjectModel.KeyedCollection<,>),
        typeof(System.Collections.ObjectModel.ReadOnlyCollection<>),

        // System System.Collections.Generic

        typeof(System.Collections.Generic.LinkedList<>),
        typeof(System.Collections.Generic.LinkedListNode<>),
        typeof(System.Collections.Generic.Queue<>),
        typeof(System.Collections.Generic.SortedDictionary<,>),
        typeof(System.Collections.Generic.SortedList<,>),
        typeof(System.Collections.Generic.Stack<>),

        // System System.Collections.Specialized

        typeof(System.Collections.Specialized.BitVector32),

        // System.Core System.Collections.Generic

        typeof(System.Collections.Generic.HashSet<>),

        // System.Core System.Linq

        typeof(System.Linq.Enumerable),
        typeof(System.Linq.IGrouping<,>),
        typeof(System.Linq.ILookup<,>),
        typeof(System.Linq.IOrderedEnumerable<>),
        typeof(System.Linq.IOrderedQueryable),
        typeof(System.Linq.IOrderedQueryable<>),
        typeof(System.Linq.IQueryable),
        typeof(System.Linq.IQueryable<>),
        typeof(System.Linq.IQueryProvider),
        typeof(System.Linq.Lookup<,>),
        typeof(System.Linq.Queryable),
        
        // UnityEngine
        typeof(UnityEngine.Random),
        typeof(UnityEngine.Debug),
        typeof(UnityEngine.Vector3),
    };

    /// <summary>
    /// These types probably shouldn't be exposed, because they allow filesystem access,
    /// or because they provide more advanced functionality that mods shouldn't depend on.
    /// Proceed with caution.
    /// </summary>
    public static readonly Type[] QuestionableTypes = new Type[] {
        
        //// mscorlib System
        
        //typeof(System.AsyncCallback),
        //typeof(System.BitConverter),
        //typeof(System.Buffer),
        //typeof(System.DateTime),
        //typeof(System.DateTimeKind),
        //typeof(System.DateTimeOffset),
        //typeof(System.DayOfWeek),
        //typeof(System.EventArgs),
        //typeof(System.EventHandler),
        //typeof(System.EventHandler<>),
        //typeof(System.TimeSpan),
        //typeof(System.TimeZone),
        //typeof(System.TimeZoneInfo),
        //typeof(System.TimeZoneNotFoundException),

        //// mscorlib System.IO
        
        //typeof(System.IO.BinaryReader),
        //typeof(System.IO.BinaryWriter),
        //typeof(System.IO.BufferedStream),
        //typeof(System.IO.EndOfStreamException),
        //typeof(System.IO.FileAccess),
        //typeof(System.IO.FileMode),
        //typeof(System.IO.FileNotFoundException),
        //typeof(System.IO.IOException),
        //typeof(System.IO.MemoryStream),
        //typeof(System.IO.Path),
        //typeof(System.IO.PathTooLongException),
        //typeof(System.IO.SeekOrigin),
        //typeof(System.IO.Stream),
        //typeof(System.IO.StringReader),
        //typeof(System.IO.StringWriter),
        //typeof(System.IO.TextReader),
        //typeof(System.IO.TextWriter),

        //// mscorlib System.Text
        
        //typeof(System.Text.ASCIIEncoding),
        //typeof(System.Text.Decoder),
        //typeof(System.Text.Encoder),
        //typeof(System.Text.Encoding),
        //typeof(System.Text.EncodingInfo),
        //typeof(System.Text.StringBuilder),
        //typeof(System.Text.UnicodeEncoding),
        //typeof(System.Text.UTF32Encoding),
        //typeof(System.Text.UTF7Encoding),
        //typeof(System.Text.UTF8Encoding),

        //// mscorlib System.Globalization
        
        //typeof(System.Globalization.CharUnicodeInfo),
        //typeof(System.Globalization.CultureInfo),
        //typeof(System.Globalization.DateTimeFormatInfo),
        //typeof(System.Globalization.DateTimeStyles),
        //typeof(System.Globalization.NumberFormatInfo),
        //typeof(System.Globalization.NumberStyles),
        //typeof(System.Globalization.RegionInfo),
        //typeof(System.Globalization.StringInfo),
        //typeof(System.Globalization.TextElementEnumerator),
        //typeof(System.Globalization.TextInfo),
        //typeof(System.Globalization.UnicodeCategory),
       
        //// System System.IO.Compression
        
        //typeof(System.IO.Compression.CompressionMode),
        //typeof(System.IO.Compression.DeflateStream),
        //typeof(System.IO.Compression.GZipStream),
        
        //// System System.Text.RegularExpressions

        //typeof(System.Text.RegularExpressions.Capture),
        //typeof(System.Text.RegularExpressions.CaptureCollection),
        //typeof(System.Text.RegularExpressions.Group),
        //typeof(System.Text.RegularExpressions.GroupCollection),
        //typeof(System.Text.RegularExpressions.Match),
        //typeof(System.Text.RegularExpressions.MatchCollection),
        //typeof(System.Text.RegularExpressions.MatchEvaluator),
        //typeof(System.Text.RegularExpressions.Regex),
        //typeof(System.Text.RegularExpressions.RegexCompilationInfo),
        //typeof(System.Text.RegularExpressions.RegexOptions),
    };		
}