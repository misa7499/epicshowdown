using System;
using UnityEngine;
using System.Collections;
using UnityEditor;
using Object = UnityEngine.Object;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(SerializedScript), true)]
public class SerializedScriptPropertyDrawer : PropertyDrawer
{
	public const float titleHeight = 18f;
	public const float lineHeight = 14f;
	public const float errorBoxHeight = 20f;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		position.height = titleHeight;
			property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, label);
		position.y += titleHeight;

		if (property.isExpanded)
		{
			var script = (SerializedScript)property.Parse().Value;

			position = EditorGUI.IndentedRect(position);

			var sourceHeight = SourceHeight(script);
			position.height = sourceHeight;
				var newSource = EditorGUI.TextArea(position, script.Source);
				if (newSource != script.Source)
				{
					property.FindPropertyRelative("Source").stringValue = newSource;
					foreach(var scripts in property.GetValues<SerializedScript>()) { script.Recompile(); }
					property.serializedObject.ApplyModifiedProperties();
				}
			position.y += sourceHeight;

			foreach(var line in ParsedErrors(script))
			{
				position.height = errorBoxHeight;
					GUI.Label(position, line);
				position.y += errorBoxHeight;
			}

			script.Recompile();
		}
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		var height = titleHeight;
		if (property.isExpanded)
		{
			var script = (SerializedScript)property.Parse().Value;

			height += SourceHeight(script);
			foreach(var line in ParsedErrors(script))
			{
				height += errorBoxHeight;
			}
		}
		return height;
	}

	public static List<string> ParsedErrors(SerializedScript script)
	{
		var errors = new List<string>();
		if (!string.IsNullOrEmpty(script.Out))
		{
			var lines = script.Out.Split('\n', '\r');
			foreach(var line in lines)
			{
				if (string.IsNullOrWhiteSpace(line)) continue;
				if (!line.StartsWith("(")) { continue; }
				errors.Add(line);
			}
		}
		return errors;
	}

	public static float SourceHeight(SerializedScript script)
	{
		var lines = string.IsNullOrEmpty(script.Source) ? 0 : script.Source.Split('\n').Length;
		return Mathf.Clamp(lines * lineHeight + 10f, 40f, 200f);
	}
}