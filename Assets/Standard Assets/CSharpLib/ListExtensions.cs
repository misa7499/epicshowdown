using System;
using System.Collections;
using System.Collections.Generic;

public static class ListExtensions
{
	public static T Last<T>(this List<T> list)
	{
		return list != null && list.Count > 0 ? list[list.Count - 1] : default(T);
	}

	public static bool IsEmpty<T>(this IList<T> list)
	{
		return list == null || list.Count == 0;
	}

	public static bool IsEmpty(this IList list)
	{
		return list == null || list.Count > 0;
	}

	public static int CountExcept<T>(this List<T> list, T except) where T : class
	{
		int count = 0;
		for(int i = 0; i < list.Count; i++)
		{
			if (list[i] != except) { count++; }
		}
		return count;
	}

	/// <summary>
	/// Pick random from element from list, with each element having assigned weights.
	/// </summary>
	public static T PickRandomFromWeightedList<T>(this IEnumerable<T> enumerable, Func<T, int> weightFunc)
	{
		var random = new System.Random(Guid.NewGuid().GetHashCode());
		int totalWeight = 0; // this stores sum of weights of all elements before current
		T selected = default; // currently selected element
		foreach (var data in enumerable)
		{
			int weight = weightFunc(data); // weight of current element
			int r = random.Next(totalWeight + weight); // random value
			if (r >= totalWeight) // probability of this is weight/(totalWeight+weight)
				selected = data; // it is the probability of discarding last selected element and selecting current one instead
			totalWeight += weight; // increase weight sum
		}

		return selected; // when iterations end, selected is some element of sequence.
	}

	/// <summary>
	/// Pick random from element from list.
	/// </summary>
	public static T PickRandom<T>(this IList<T> list)
	{
		return list[UnityEngine.Random.Range(0, list.Count)];
	}
}