using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
public static class SceneLoaderHelper
{
	private const string LastEditedSceneKey = "UnityLibs_LastEditedScene";

#if UNITY_EDITOR
	static SceneLoaderHelper()
	{
		EditorApplication.playModeStateChanged += (x) =>
		{
			var oldScene = EditorPrefs.GetString(LastEditedSceneKey, "");
			if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode && !string.IsNullOrEmpty(oldScene))
			{
				EditorSceneManager.OpenScene(oldScene);
				EditorPrefs.DeleteKey(LastEditedSceneKey);
			}
		};
	}
#endif

	public static void RunScene(string sceneName)
	{
#if !UNITY_EDITOR
		Debug.LogWarning("RunScene is available only in Unity Editor!");
#else
		if (!EditorApplication.isPlaying && SaveScene())
		{
			EditorPrefs.SetString(LastEditedSceneKey, SceneManager.GetActiveScene().path);
			LoadScene(sceneName);
			EditorApplication.isPlaying = true;
		}
#endif
	}

	public static void LoadScene(string sceneName)
	{
		if (Application.isPlaying)
        {
            SceneManager.LoadScene(sceneName);
        }
        else
        {
#if UNITY_EDITOR
            EditorSceneManager.OpenScene(Path.Combine("Assets/Scenes/", sceneName) + ".unity");
#endif
        }
	}

	public static bool SaveScene()
	{
#if !UNITY_EDITOR
		Debug.LogWarning("Save scene is available only in Editor!");
		return true;
#else
		if (EditorSceneManager.GetActiveScene().name != "Untitled")
		{
			return EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
		}
		else
		{
			return true;
		}
#endif
	}
}
