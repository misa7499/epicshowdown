﻿using System;
using UnityEngine;

/// <summary>
/// - Fires ScreenSizeChanged event when resolution gets changed
/// - Faster reading of screen params than Screen
/// </summary>
public static class NScreen
{
	public static event Action ScreenSizeChanged;
	public static int width { get; private set; }
	public static int height { get; private set; }
	public static float aspectRatio { get; private set; }

	[RuntimeInitializeOnLoadMethod]
	static void CheckIfResolutionChanged()
	{
		UpdateParams(Screen.width, Screen.height);

		Invoker.Update += () =>
		{
			var newWidth = Screen.width;
			var newHeight = Screen.height;
			if (newWidth != width || newHeight != height)
			{
				UpdateParams(newWidth, newHeight);
				ScreenSizeChanged?.Invoke();
			}
		};
	}

	static void UpdateParams(int newWidth, int newHeight)
	{
		width = newWidth;
		height = newHeight;
		aspectRatio = (float) width / height;
	}
}