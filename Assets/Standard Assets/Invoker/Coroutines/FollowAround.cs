using System;
using System.Collections;
using UnityEngine;

public static class FollowAround
{
	public static Coroutine Follow(this Transform lhs, Transform rhs)
	{
		return Invoker.StartCoroutine(FollowImpl(lhs, rhs));
	}

	public static IEnumerator FollowImpl(Transform lhs, Transform rhs)
	{
		while(lhs != null && rhs != null)
		{
			lhs.position = rhs.position;
			yield return null;
		}
	}
}