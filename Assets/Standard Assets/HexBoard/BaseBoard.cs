﻿using UnityEngine;
using UnityEngine.Profiling;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using static System.Math;

/// <summary>
/// Each board type defines:
/// - tile type
/// - vertex type
/// - pathfinder for vertices
/// - pathfinder for tiles
/// </summary>
public abstract partial class BaseBoard<GameTile, GameVertex, GameBoard>
	where GameTile : BaseBoard<GameTile, GameVertex, GameBoard>.Tile, new()
	where GameVertex : BaseBoard<GameTile, GameVertex, GameBoard>.Vertex, new()
	where GameBoard : BaseBoard<GameTile, GameVertex, GameBoard>
{
	public readonly BoardArray<GameTile> Tiles;
	public readonly BoardArray<GameVertex> Verticies;
	public readonly int Width;
	public readonly int Height;
	public readonly int TileSides;
	public readonly NFloat LeftEdge, RightEdge, TopEdge, BottomEdge;
	public readonly NVector2 CenterOffset;

	protected abstract void ConnectBoard();
	protected abstract GameTile CreateTile(int x, int y);
	protected abstract GameVertex CreateVertex(int x, int y);
	public abstract GameTile GetTile(NFloat x, NFloat y);

	protected BaseBoard(int sides, int tilesRows, int tilesColumns, int vertexRows, int vertexColumns)
	{
		TileSides = sides;
		Width = tilesRows;
		Height = tilesColumns;

		// Set up Tiles initial
		Tiles = new BoardArray<GameTile>(tilesRows, tilesColumns);
		for (int x = 0; x < tilesRows; x++)
			for (int y = 0; y < tilesColumns; y++)
				Tiles[x, y] = CreateTile(x, y);

		// Set up Verticies initial
		Verticies = new BoardArray<GameVertex>(vertexRows, vertexColumns);
		for (int x = 0; x < Verticies.Width; x++)
			for (int y = 0; y < Verticies.Height; y++)
				Verticies[x, y] = CreateVertex(x, y);

		// Invoke specific connection algorithm
		ConnectBoard();

		// calculate boundaries
		LeftEdge = Verticies.Min(x => x.Position.x);
		RightEdge = Verticies.Max(x => x.Position.x);
		TopEdge = Verticies.Min(x => x.Position.y);
		BottomEdge = Verticies.Max(x => x.Position.y);

		CenterOffset = Tiles[Width / 2, Height / 2].Position;

		// initialize neihbours references.
		for (int x = 0; x < tilesRows; x++)
			for (int y = 0; y < tilesColumns; y++)
			{
				Tiles[x, y].Neighbours = Tiles[x, y].Tiles.TrimNulls();
				Tiles[x, y].Position -= CenterOffset;
			}

		for (int x = 0; x < Verticies.Width; x++)
			for (int y = 0; y < Verticies.Height; y++)
			{
		 		Verticies[x, y].Neighbours = Verticies[x, y].Verticies.TrimNulls();
		 		Verticies[x, y].Position -= CenterOffset;
			}

		LeftEdge -= CenterOffset.x;
		RightEdge -= CenterOffset.x;
		TopEdge -= CenterOffset.y;
		BottomEdge -= CenterOffset.y;
	}

	/// <summary>
	/// Common data for vertices and tiles.
	/// </summary>
	public abstract partial class BoardElement
	{
		// board indicies
		public int x;
		public int y;
		public int Index;

		// actual world position
		public NVector2 Position;
		public Vector3 Position3D => new Vector3((float)Position.x, 0f, (float)Position.y);

		// neighbour collections
		public GameVertex[] Verticies;
		public GameTile[] Tiles;
		public GameBoard Board;

		// API
		public override string ToString() => $"{GetType().Name} ({x}, {y})";
		public bool IsNeighbour(GameVertex vertex) => vertex != null && (vertex == this || Array.IndexOf(Verticies, vertex) != -1);
		public bool IsNeighbour(GameTile tile) => tile != null && (tile == this || Array.IndexOf(Tiles, tile) != -1);
		public NFloat GetDist(BoardElement elem) => NFloat.Distance(Position.x, Position.y, elem.Position.x, elem.Position.y);

		public int GridDist(BoardElement elem)
		{
			if (Tiles.Length == 6)
			{
				// hexagon grid distance can be a little quirky...
				var dx = Abs(elem.x - x);
				var dy = Abs(elem.y - y);

				if (dx == 0)
					return dy;
				if (dy == 0)
					return dx;
				if (y < elem.y)
					return dx + dy - (int)Ceiling(dx * 0.5);
				else
					return dx + dy - (int)Floor(dx / 2.0);
			}
			else
			{
				return Abs(elem.x - x) + Abs(elem.y - y);
			}
		}
	}

	public abstract partial class BoardElement<T> : BoardElement where T : BoardElement<T>
	{
		public T[] Neighbours;
	}
}