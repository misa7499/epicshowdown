using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Supplies pathfinding and geometric data for a hex board, while allowing .
/// </summary>
public partial class TileBoard<GameTile, GameVertex, GameBoard> : BaseBoard<GameTile, GameVertex, GameBoard>
	where GameTile : BaseBoard<GameTile, GameVertex, GameBoard>.Tile, new()
	where GameVertex : BaseBoard<GameTile, GameVertex, GameBoard>.Vertex, new()
	where GameBoard : TileBoard<GameTile, GameVertex, GameBoard>
{
	public TileBoard(int width, int height) : base(
		sides: 4,
		tilesRows: width,
		tilesColumns: height,
		vertexRows: width + 1,
		vertexColumns: height + 1)
	{
	}

	protected override void ConnectBoard()
	{
		// set up tile's neighbours
		for (int x = 0; x < Width; x++)
			for (int y = 0; y < Height; y++)
			{
				var tile = Tiles[x, y];
				tile.Tiles[0] = Tiles[x, y + 1];
				tile.Tiles[1] = Tiles[x + 1, y];
				tile.Tiles[2] = Tiles[x, y - 1];
				tile.Tiles[3] = Tiles[x - 1, y];
				tile.Verticies[0] = Verticies[x, y];
				tile.Verticies[1] = Verticies[x + 1, y];
				tile.Verticies[2] = Verticies[x + 1, y + 1];
				tile.Verticies[3] = Verticies[x, y + 1];
			}

		// set up vertex neighbours
		for (int x = 0; x < Verticies.Width; x++)
			for (int y = 0; y < Verticies.Height; y++)
			{
				var vertex = Verticies[x, y];
				vertex.Tiles[0] = Tiles[x - 1, y - 1];
				vertex.Tiles[1] = Tiles[x - 1, y];
				vertex.Tiles[2] = Tiles[x, y - 1];
				vertex.Tiles[3] = Tiles[x, y];
				vertex.Verticies[0] = Verticies[x, y + 1];
				vertex.Verticies[1] = Verticies[x + 1, y];
				vertex.Verticies[2] = Verticies[x, y - 1];
				vertex.Verticies[3] = Verticies[x - 1, y];
			}
	}

	protected override GameTile CreateTile(int x, int y)
	{
		return new GameTile
		{
			x = x,
			y = y,
			Index = x + y * Width,
			Position = GetTilePosition(x, y),
			Verticies = new GameVertex[4],
			Tiles = new GameTile[4],
			Board = (GameBoard)this
		};
	}

	protected override GameVertex CreateVertex(int x, int y)
	{
		return new GameVertex
		{
			x = x,
			y = y,
			Index = x + y * Verticies.Width,
			Position = GetVertexPosition(x, y),
			Verticies = new GameVertex[4],
			Tiles = new GameTile[4],
			Board = (GameBoard)this
		};
	}

	/// <summary>
	/// Gets tile on a board which covers given world position.
	/// </summary>
	public override GameTile GetTile(NFloat x, NFloat y)
	{
		var i = Mathf.Clamp((x - NFloat.Half).RoundToInt(), 0, Width - 1);
		var j = Mathf.Clamp((y - NFloat.Half).RoundToInt(), 0, Height - 1);
		return Tiles[i, j];
	}

	private static NVector2 GetVertexPosition(int x, int y)
	{
		return new NVector2(x, y);
	}

	private static NVector2 GetTilePosition(int x, int y)
	{
		return new NVector2(x + 0.5f, y + 0.5f);
	}
}