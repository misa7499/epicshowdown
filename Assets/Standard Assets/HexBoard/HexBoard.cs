﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Supplies pathfinding and geometric data for a hex board, while allowing .
/// </summary>
public class HexBoard<GameTile, GameVertex, GameBoard> : BaseBoard<GameTile, GameVertex, GameBoard>
	where GameTile : BaseBoard<GameTile, GameVertex, GameBoard>.Tile, new()
	where GameVertex : BaseBoard<GameTile, GameVertex, GameBoard>.Vertex, new()
	where GameBoard : HexBoard<GameTile, GameVertex, GameBoard>
{
	public HexBoard(int width, int height) : base(
		sides: 6,
		tilesRows : width,
		tilesColumns : height,
		vertexRows: 2 + width + (width - 1) / 2,
		vertexColumns: height * 2 + 2)
	{}

	protected override sealed void ConnectBoard()
	{
		// second pass - set up tile's neighbours
		for (int x = 0; x < Width; x++)
			for (int y = 0; y < Height; y++)
			{
				var tile = Tiles[x, y];
				tile.Tiles[0] = Tiles[x, y + 1];
				tile.Tiles[1] = Tiles[x + 1, y + x % 2];
				tile.Tiles[2] = Tiles[x + 1, y - 1 + x % 2];
				tile.Tiles[3] = Tiles[x, y - 1];
				tile.Tiles[4] = Tiles[x - 1, y + x % 2];
				tile.Tiles[5] = Tiles[x - 1, y - 1 + x % 2];

				// get index of LeftUp vertex
				var leftUpX = x + (x + 1) / 2;
				var leftUpY = y * 2 + x % 2;

				// follow others vertices clockwise
				tile.Verticies[0] = Verticies[leftUpX, leftUpY];
				tile.Verticies[1] = Verticies[leftUpX + 1, leftUpY];
				tile.Verticies[2] = Verticies[leftUpX + 2 - x % 2, leftUpY + 1];
				tile.Verticies[3] = Verticies[leftUpX + 1, leftUpY + 2];
				tile.Verticies[4] = Verticies[leftUpX, leftUpY + 2];
				tile.Verticies[5] = Verticies[leftUpX - x % 2, leftUpY + 1];
			}

		// second pass - set up vertex neighbours
		for (int x = 0; x < Verticies.Width; x++)
			for (int y = 0; y < Verticies.Height; y++)
			{
				var vertex = Verticies[x, y];
				var vertexMagicNumber = x % 3 + y % 2;
				switch (vertexMagicNumber)
				{
					case 2:
						// this vertex is tile center!
						continue;
					case 0:
					case 3:
						vertex.Verticies[0] = Verticies[x + 1, y];
						vertex.Verticies[1] = Verticies[x - y % 2, y + 1];
						vertex.Verticies[2] = Verticies[x - y % 2, y - 1];
						break;
					case 1:
						vertex.Verticies[0] = Verticies[x - 1, y];
						vertex.Verticies[1] = Verticies[x + 1 - y % 2, y + 1];
						vertex.Verticies[2] = Verticies[x + 1 - y % 2, y - 1];
						break;
				}

				var leftTileX = (x + 1) / 3 * 2 - y % 2;
				switch (vertexMagicNumber)
				{
					case 0:
					case 3:
						vertex.Tiles[0] = Tiles[leftTileX, y / 2];
						vertex.Tiles[1] = Tiles[leftTileX, y / 2 - 1];
						vertex.Tiles[2] = Tiles[leftTileX - 1, y / 2 - 1 + y % 2];
						break;
					case 1:
						vertex.Tiles[0] = Tiles[leftTileX, y / 2];
						vertex.Tiles[1] = Tiles[leftTileX, y / 2 - 1];
						vertex.Tiles[2] = Tiles[leftTileX + 1, y / 2 - 1 + y % 2];
						break;
				}
			}
	}

	/// <summary>
	/// Create tile method.
	/// </summary>
	protected override sealed GameTile CreateTile(int x, int y)
	{
		var elem = new GameTile();
		elem.x = x;
		elem.y = y;
		elem.Index = x + y * Width;
		elem.Position = GetTilePosition(x, y);
		elem.Verticies = new GameVertex[6];
		elem.Tiles = new GameTile[6];
		elem.Board = (GameBoard)this;
		return elem;
	}

	/// <summary>
	/// Create vertex method.
	/// </summary>
	protected override sealed GameVertex CreateVertex(int x, int y)
	{
		var elem = new GameVertex();
		elem.x = x;
		elem.y = y;
		elem.Index = x + y * Verticies.Width;
		elem.Position = GetVertexPosition(x, y);
		elem.Verticies = new GameVertex[3];
		elem.Tiles = new GameTile[3];
		elem.Board = (GameBoard)this;
		return elem;
	}

	/// <summary>
	/// Gets tile on a board which covers given world position.
	/// </summary>
	public override GameTile GetTile(NFloat x, NFloat y)
	{
		x += CenterOffset.x;
		y += CenterOffset.y;

		var leftX = ((x - NFloat.Half) * TwoThirds).FloorToInt();
		var leftY = (y / HexHeight2 - leftX % 2 * NFloat.Half).FloorToInt();

		var rightX = (x * TwoThirds).FloorToInt();
		// rightX == leftX is happy path case - we didn't click on strip between columns and can determine tile easily
		if (leftX == rightX) { return Tiles[leftX, leftY]; }

		var rightY = (y / HexHeight2 - rightX % 2 * NFloat.Half).FloorToInt();
		var rightTile = GetTilePosition(rightX, rightY) - CenterOffset;
		var leftTile = GetTilePosition(leftX, leftY) - CenterOffset;

		// situation unclear - get closer tile
		var pos = new NVector2(x, y);
		var leftDist = (pos - leftTile).sqrMagnitude;
		var rightDist = (pos - rightTile).sqrMagnitude;
		return leftDist < rightDist ? Tiles[leftX, leftY] : Tiles[rightX, rightY];
	}
	static readonly NFloat TwoThirds = new NFloat(2f / 3f);
	static readonly NFloat ThreeHalves = new NFloat(3f / 2f);

	public static NVector2 GetTilePosition(int x, int y) => new NVector2()
	{
		x = 1 + x * ThreeHalves,
		y = HexHeight * (1 + y * 2 + x % 2)
	};

	public static NVector2 GetVertexPosition(int x, int y) => new NVector2()
	{
		x = x + (y % 2 == 0 ? NFloat.Half : 0),
		y = y * HexHeight
	};

	public static readonly NFloat HexHeight = new NFloat(0.866025f);
	public static readonly NFloat HexHeight2 = HexHeight * 2;
}