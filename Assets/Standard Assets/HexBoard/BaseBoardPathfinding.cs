using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// AStar algorithm for base board
/// </summary>
public abstract partial class BaseBoard<GameTile, GameVertex, GameBoard>
{
	/// <summary>
	/// Base class for game-specified tile class. Has pathfinding and geometric data.
	/// </summary>
	public class Tile : BoardElement<GameTile>
	{
		public List<GameTile> FindPath(GameTile tile, Pathfinder agent = null, List<GameTile> path = null) =>
			Board.Pathfinding.FindPath( (GameTile)this, tile, agent, path);
	}

	/// <summary>
	/// Base class for game-specified vertex class. Has pathfinding and geometric data.
	/// </summary>
	public class Vertex : BoardElement<GameVertex>
	{
		public List<GameVertex> FindPath(GameVertex tile, VertexPathfinder agent = null, List<GameVertex> path = null) =>
			Board.VertexPathfinding.FindPath( (GameVertex)this, tile, agent, path);
	}

	/// <summary>
	/// Agent for tiles pathfinding.
	/// </summary>
	public interface Pathfinder : AStar<GameTile>.Agent {}

	/// <summary>
	/// Agent for vertices pathfinding.
	/// </summary>
	public interface VertexPathfinder : AStar<GameVertex>.Agent {}

	/// <summary>
	/// AStar list for tiles pathfinding. Call Pathfind.FindPath(Tile A, Tile B) to get a list of tiles.
	/// </summary>
	public AStar<GameTile> Pathfinding
	{
		get
		{
			if (_pathfinding == null)
			{
				// build node graph lazily.
				_pathfinding = new AStar<GameTile>(Tiles);
			}
			return _pathfinding;
		}
	}
	AStar<GameTile> _pathfinding;

	/// <summary>
	/// AStar list for vertex pathfinding. Call VertexPathfind.FindPath(Vertex A, Vertex B) to get a list of vertices.
	/// </summary>
	public AStar<GameVertex> VertexPathfinding
	{
		get
		{
			if (_vertexPathfinding == null)
			{
				// build node graph lazily.
				_vertexPathfinding = new AStar<GameVertex>(Verticies);
			}
			return _vertexPathfinding;
		}
	}
	AStar<GameVertex> _vertexPathfinding;

	/// <summary>
	/// Interface to setup and run the AStar algorithm.
	/// K is type of board element (tiles/vertices)
	/// </summary>
	public class AStar<T> where T : BoardElement<T>, new()
	{
		/// <summary>
		/// This parameter controls how much is distance considered a good heuristic for actual path cost.
		/// </summary>
		public NFloat DistanceHeuristicBias = 1;

		/// <summary>
		/// Pool of path lists. You can return paths returns by this when you no longer need them to relieve pressure on the GC.
		/// </summary>
		readonly Stack<List<T>> pathPool = new Stack<List<T>>();

		/// <summary>
		/// Graph of pathfinding node cached in a struct array.
		/// </summary>
		readonly Node[] allNodes;

		/// <summary>
		/// Build and store a pathfinding graph.
		/// </summary>
		public AStar(BoardArray<T> board)
		{
			allNodes = new Node[board.Count];
			for(int i = 0; i < board.Width; i++)
				for(int j = 0; j < board.Height; j++)
				{
					var element = board[i, j];
					allNodes[element.Index] = new Node(element);
				}
		}

		/// <summary>
		/// Pathfinding info on one Tile/Vertex.
		/// </summary>
		private struct Node
		{
			public readonly T BoardElement;
			public readonly int Index;
			public readonly int[] Neighbours;

			public bool IsClosedList, IsOpenList;
			public NFloat TotalCost, CostFromStart, EstimatedCost;
			public int Parent;

			public Node(T element)
			{
				BoardElement = element;
				Neighbours = element.Neighbours.Select(x => x.Index).ToArray();
				Index = element.Index;

				IsOpenList = IsClosedList = false;
				TotalCost = EstimatedCost = CostFromStart = 0;
				Parent = -1;
			}

			public void ResetAStar()
			{
				IsOpenList = IsClosedList = false;
				TotalCost = EstimatedCost = CostFromStart = 0;
				Parent = -1;
			}
		}

		/// <summary>
		/// Defines specific logic for movement, if needed.
		/// </summary>
		public interface Agent
		{
			bool IsLegalPosition(T position);
			(bool legal, NFloat cost) EvaluateMovement(T from, T to, T goal);
		}

		/// <summary>
		/// Used in case no agent is set.
		/// </summary>
		class NullAgent : Singleton<NullAgent>, Agent
		{
			public bool IsLegalPosition(T position) => true;
			public (bool legal, NFloat cost) EvaluateMovement(T from, T to, T goal) => (true, 1);
		}

		/// <summary>
		/// Steps the AStar algorithm forward until it either fails or finds the goal node.
		/// </summary>
		public List<T> FindPath(T start, T goal, List<T> path = null)
		{
			return FindPath(start, goal, NullAgent.Instance, path);
		}

		/// <summary>
		/// Put path into a pool for usage later. Returning produced paths to pool when you no longer need them is advised to relieve pressure from the GC.
		/// </summary>
		public void ReturnToPool(List<T> path)
		{
			path.Clear();
			pathPool.Push(path);
		}

		/// <summary>
		/// Steps the AStar algorithm forward until it either fails or finds the goal node.
		/// </summary>
		public List<T> FindPath(T startNode, T goalNode, Agent agent, List<T> path = null)
		{
			// prepare list for the resulting path.
			if (path == null)
			{
				path = pathPool.Count > 0 ? pathPool.Pop() : new List<T>();
			}
			else
			{
				path.Clear();
			}

			// sanity check.
			if (!agent.IsLegalPosition(goalNode))
			{
				return path;
			}

			// prepare state.
			ClearOpenList();

			// initialize start
			ref Node current = ref allNodes[startNode.Index];
			AddToOpenList(current);
			current.EstimatedCost = current.TotalCost = startNode.GetDist(goalNode);
			current.IsOpenList = true;

			// Search until either failure or the goal node has been found.
			while (true)
			{
				do // get next candidate from the open list
				{
					// There are no more nodes to search, return failure.
					if (end == start)
					{
						return null;
					}

					// Check the next best node in the graph by TotalCost.
					var nextBestNode = buffer[start++];
					current = ref allNodes[nextBestNode];
				}
				while(current.IsClosedList);

				// Remove from the open list and place on the closed list
				// since this node is now being searched.
				current.IsOpenList = false;
				current.IsClosedList = true;

				// Found the goal, stop searching.
				if (current.Index == goalNode.Index)
				{
					path.Add(current.BoardElement);
					while (current.Index != startNode.Index && path.Count < 1000)
					{
						current = ref allNodes[current.Parent];
						path.Add(current.BoardElement);
					}
					path.Reverse();
					return path;
				}

				// Node was not the goal so add all children nodes to the open list.
				// Each child needs to have its movement cost set and estimated cost.
				foreach (var childIndex in current.Neighbours)
				{
					// If the child has already been searched (closed list) or is on
					// the open list to be searched then do not modify its movement cost
					// or estimated cost since they have already been set previously.
					ref Node child = ref allNodes[childIndex];
					if (child.IsOpenList || child.IsClosedList) { continue; }

					var (legal, cost) = agent.EvaluateMovement(current.BoardElement, child.BoardElement, goalNode);
					if (!legal) { continue; }

					child.Parent = current.Index;
					child.EstimatedCost = child.BoardElement.GetDist(goalNode);
					child.CostFromStart = cost + current.CostFromStart;
					child.TotalCost = child.EstimatedCost * DistanceHeuristicBias + child.CostFromStart;
					AddToOpenList(child);
					child.IsOpenList = true;
				}
			}
		}

		/// <summary>
		/// Open list implementation with ints, optimized for max performance.
		/// </summary>

		int start, end;
		int capacity = 8;
		int[] buffer = new int[8];

		void AddToOpenList(in Node value)
		{
			if (end == capacity)
			{
				capacity *= 2;
				Array.Resize(ref buffer, capacity);
			}

			var insertIndex = end;
			if (insertIndex > start)
			{
				insertIndex = BinarySearch(buffer, value.TotalCost, start, end);
			}
			Insert(insertIndex, value.Index);

			int BinarySearch(int[] inputArray, NFloat key, int min, int max)
			{
				if (allNodes[inputArray[min]].TotalCost >= key)
				{
					return min;
				}

				var index = max;
				while (min <= max)
				{
					int mid = (min + max) / 2;
					var midValue = allNodes[inputArray[mid]].TotalCost;
					if (key > midValue)
					{
						min = mid + 1;
					}
					else if (key < midValue)
					{
						index = mid;
						max = mid - 1;
					}
					else
					{
						return mid;
					}
				}
				return index;
			}

			void Insert(int index, int valueIndex)
			{
				if (index < end)
				{
					Array.Copy(buffer, index, buffer, index + 1, end - index);
				}
				buffer[index] = valueIndex;
				end++;
			}
		}

		void ClearOpenList()
		{
			for (int i = 0; i < end; i++)
			{
				var tile = buffer[i];
				if (tile >= 0) { allNodes[tile].ResetAStar(); }
			}
			start = 0;
			end = 0;
		}
	}

}