using System;
using System.Collections.Generic;

/// <summary>
/// "Extension method" for BoardElement<T> - find common neighbours with specified tile.
/// </summary>
public abstract partial class BaseBoard<GameTile, GameVertex, GameBoard>
{
	public abstract partial class BoardElement<T>
	{
		public IEnumerator<T> CommonNeighboursWith(T tile)
		{
			return CommonNeighbours.Fetch((T)this, tile);
		}

		/// <summary>
		/// Poolable enumerator used to get a list of common neighbours of two elements.
		/// </summary>
		class CommonNeighbours : ReusableEnumerator<T, CommonNeighbours>
		{
			int i;
			T lhs, rhs;

			public static CommonNeighbours Fetch(T lhs, T rhs)
			{
				var fetched = Fetch();
				fetched.lhs = lhs;
				fetched.rhs = rhs;
				if (lhs == rhs) { fetched.i = int.MaxValue; }
				return fetched;
			}

			public override void OnDisposed()
			{
				i = 0;
				lhs = rhs = null;
			}

			public override bool MoveNext()
			{
				while(i < lhs.Neighbours.Length)
				{
					for(int j = 0; j < rhs.Neighbours.Length; j++)
					{
						if (lhs.Neighbours[i] == rhs.Neighbours[j])
						{
							Current = lhs.Neighbours[i++];
							return true;
						}
					}
					i++;
				}

				return false;
			}
		}
	}
}