using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// "Extension method" for BoardElement<T> - flood algorithms
/// </summary>
public abstract partial class BaseBoard<GameTile, GameVertex, GameBoard>
{
	public abstract partial class BoardElement<T>
	{
		/// <summary>
		/// Flooding algorithm from an initial set of tiles.
		/// </summary>
		public static List<T> Flood(List<T> list, Func<T, T, bool> condition)
		{
			var returningList = new List<T>(list);

			foreach(var elem in list)
				elem._flood_flag = true;

			foreach(var elem in list)
				FloodInternal(elem, condition, returningList);

			foreach(var elem in returningList)
				elem._flood_flag = false;

			return returningList;
		}

		/// <summary>
		/// Flooding algorithm from one tile.
		/// </summary>
		public List<T> Flood(Func<T, T, bool> condition)
		{
			var returningList = new List<T>();

			FloodInternal((T)this, condition, returningList);

			foreach(var elem in returningList)
				elem._flood_flag = false;

			return returningList;
		}

		/// <summary>
		/// Recursively build flood list.
		/// _flood_flag must be cleared by the calling API on the end.
		/// </summary>
		static void FloodInternal(T from, Func<T, T, bool> condition, List<T> list)
		{
			if (!from._flood_flag)
			{
				from._flood_flag = true;
				list.Add(from);
			}

			foreach(var neighbour in from.Neighbours)
			{
				try
				{
					if (!neighbour._flood_flag && condition(from, neighbour))
					{
						FloodInternal(neighbour, condition, list);
					}
				}
				catch(System.Exception e)
				{
					Debug.LogException(e);
				}
			}
		}

		internal bool _flood_flag;
	}
}