using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Useful Utility methods go here.
/// </summary>
public abstract partial class BaseBoard<GameTile, GameVertex, GameBoard>
{
	/// <summary>
	/// Gets tile on the specified position in the world.
	/// </summary>
	public GameTile GetTile(NVector2 pos) => GetTile(pos.x, pos.y);

	/// <summary>
	/// Center of the board in 2d space.
	/// </summary>
	public NVector2 Center
	{
		get { return new NVector2((LeftEdge + RightEdge) * 0.5f, (TopEdge + BottomEdge) * 0.5f); }
	}

	/// <summary>
	/// Center of the board in 2d space.
	/// </summary>
	public Vector3 Center3D
	{
		get { return new Vector3((float)(LeftEdge + RightEdge) * 0.5f, 0, (float)(TopEdge + BottomEdge) * 0.5f); }
	}


	/// <summary>
	/// Clamps input position to the board boundaries.
	/// </summary>
	public void Clamp(ref NVector2 position)
	{
		if (position.x < LeftEdge) { position.x = LeftEdge; } else if (position.x > RightEdge) { position.x = RightEdge; }
		if (position.y < TopEdge) { position.y = TopEdge; } else if (position.y > BottomEdge) { position.y = BottomEdge; }
	}

	/// <summary>
	/// Is input position inside board boundaries?
	/// </summary>
	public bool IsOutside(Vector2 position) => position.x < LeftEdge || position.x > RightEdge || position.y < TopEdge || position.y > BottomEdge;

	/// <summary>
	/// Utility
	/// </summary>
	public abstract partial class BoardElement
	{
		public GameTile GetRandomNeighbour()
		{
			int index = BoardUtil.RandomIndex(Tiles.Length);
			for (int i = 0; i < Tiles.Length; i++) { if (Tiles[(index + i) % 6] != null) { return Tiles[(index + i) % 6]; } }
			return null;
		}

		public GameTile GetRandomNeighbour(Predicate<GameTile> condition)
		{
			int index = BoardUtil.RandomIndex(Tiles.Length);
			for (int i = 0; i < Tiles.Length; i++)
			{
				var tile = Tiles[(index + i) % 6];
				if (tile != null && condition(tile)) { return Tiles[(index + i) % 6]; }
			}
			return null;
		}

		public int NeighbourTilesCount
		{
			get
			{
				int count = 0;
				for (int i = 0; i < Tiles.Length; i++) { if (Tiles[i] != null) { count++; } }
				return count;
			}
		}
	}
}

public static class BoardUtil
{
	public static int RandomIndex(int count)
	{
		return UnityEngine.Random.Range(0, count - 1); // replace this if you want some different kind of random.
	}
}

/// <summary>
/// Wrapper around jagged array.
/// </summary>
public class BoardArray<T> : IEnumerable<T> where T : class
{
	readonly T[,] elems;
	public readonly int Width;
	public readonly int Height;
	public readonly int Count;

	public BoardArray(int width, int height)
	{
		Height = height;
		Width = width;
		Count = Width * Height;
		elems = new T[width, height];
	}

	/// <summary>
	/// True if coordinates in range.
	/// </summary>
	public bool IsValid(int x, int y) => x >= 0 && y >= 0 && x < Width && y < Height;

	/// <summary>
	/// Jagged array access. Returns null if not in range.
	/// </summary>
	public T this[int x, int y]
	{
		get { return IsValid(x, y) ? elems[x, y] : null; }
		set { elems[x, y] = value; }
	}

	/// <summary>
	/// Jagged array access. Returns null if not in range.
	/// </summary>
	public T this[int index]
	{
		get { return IsValid(index % Width, index / Width) ? elems[index % Width, index / Width] : null; }
		set { elems[index % Width, index / Width] = value; }
	}

	public T GetClamped(int x, int y)
	{
		x = Mathf.Clamp(x, 0, Width - 1);
		y = Mathf.Clamp(y, 0, Height - 1);
		return elems[x, y];
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return elems.GetEnumerator();
	}

	public IEnumerator<T> GetEnumerator()
	{
		for(int i = 0; i < Width; i++)
			for (int j = 0; j < Height; j++)
				yield return elems[i, j];
	}

	public T GetRandom()
	{
		var x = BoardUtil.RandomIndex(Width);
		var y = BoardUtil.RandomIndex(Height);
		return elems[x, y];
	}

	public T GetRandom(Predicate<T> condition)
	{
		if (condition == null)
			return GetRandom();

		for(int i = 0; i < Width * Height; i++)
		{
			var potential = GetRandom();
			if (condition(potential)) { return potential; }
		}

		/* nothing found ._. */
		return null;
	}
}