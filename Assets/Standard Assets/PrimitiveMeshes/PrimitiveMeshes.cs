﻿using System.Collections.Generic;
using UnityEngine;

public static class PrimitiveMeshes
 {
     private static Dictionary<int, Mesh> primitiveMeshes = new Dictionary<int, Mesh>();

     public static Mesh GetMesh(this PrimitiveType type)
     {
		if (!primitiveMeshes.TryGetValue((int)type, out Mesh mesh))
		{
			GameObject gameObject = GameObject.CreatePrimitive(type);
			mesh = gameObject.GetComponent<MeshFilter>().sharedMesh;
			GameObject.DestroyImmediate(gameObject);
			primitiveMeshes[(int)type] = mesh;
		}

		return mesh;
     }
 }