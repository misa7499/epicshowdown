using System;
using System.Text;
using GameConsole;
using UnityEngine;

public static class HilbertCurve
{
	[ExecutableCommand]
	public static string HilberCurveTest(int n)
	{
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < n*n; i++)
		{
			var (x, y) = d_to_xy(n, i);
			sb.AppendLine($"{x} {y}");
		}
		return sb.ToString();
	}

	public static int xy_to_d(int n, int x, int y)
	{
		int d = 0;
		for(int s = n / 2; s > 0; s /= 2)
		{
			int rx = (x & s) > 0 ? 1 : 0;
			int ry = (y & s) > 0 ? 1 : 0;

			d += s * s * ((3 * rx) ^ ry);

			if (ry == 0)
			{
				if (rx == 1)
				{
					x = s - 1 - x;
					y = s - 1 - y;
				}

				//Swap x and y
				var tmp = x;
				x = y;
				y = tmp;
			}
		}
		return d;
	}

	public static (int, int) d_to_xy(int n, int d)
	{
		int x = 0;
		int y = 0;
		int t = d;
		for (int s = 1; s < n; s *= 2)
		{
			int rx = 1 & (t / 2);
			int ry = 1 & (t ^ rx);

			if (ry == 0)
			{
				if (rx == 1)
				{
					x = s - 1 - x;
					y = s - 1 - y;
				}

				//Swap x and y
				var tmp = x;
				x = y;
				y = tmp;
			}

			x += s * rx;
			y += s * ry;
			t /= 4;
		}
		return (x, y);
	}
}

/*
//convert (x,y) to d
int xy2d (int n, int x, int y) {
    int rx, ry, s, d=0;
    for (s=n/2; s>0; s/=2) {
        rx = (x & s) > 0;
        ry = (y & s) > 0;
        d += s * s * ((3 * rx) ^ ry);
        rot(s, &x, &y, rx, ry);
    }
    return d;
}

//convert d to (x,y)
void d2xy(int n, int d, int x, int y) {
    int rx, ry, s, t=d;
    x = y = 0;
    for (s=1; s<n; s*=2) {
        rx = 1 & (t/2);
        ry = 1 & (t ^ rx);
        rot(s, x, y, rx, ry);
        x += s * rx;
        y += s * ry;
        t /= 4;
    }
}

//rotate/flip a quadrant appropriately
void rot(int n, int x, int y, int rx, int ry) {
    if (ry == 0) {
        if (rx == 1) {
            x = n-1 - x;
            y = n-1 - y;
        }

        //Swap x and y
        int t  = x;
        x = y;
        y = t;
    }
}

 */