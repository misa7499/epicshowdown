﻿using System;
using System.Collections.Generic;
using UnityEngine;
using VoronoiUtils;

/// <summary>
/// Final Voronoi.
/// </summary>
public class Voronoi
{
	public readonly List<GraphEdge> Edges = new List<GraphEdge>();
	public readonly List<Polygon> Polygons = new List<Polygon>();

	/*
		Creates a voronoi diagram with provided list of position and bounding box.
		Will write to the provided existing voronoi object (to reuse memory) if one is provided. Creates a new object if voronoi is left as null.
		Returns voronoi object containing created diagram.
	 */
	public static Voronoi Generate(IList<Vector2> positions, float minX, float maxX, float minY, float maxY, Voronoi voronoi = null)
	{
		if (voronoi == null) { voronoi = new Voronoi(); }
		VoronoiGenerator.Instance.Generate(positions, minX, maxX, minY, maxY, voronoi);
		return voronoi;
	}
}