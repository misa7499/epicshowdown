﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace VoronoiUtils
{
	// use for sites and vertecies
	public class Polygon : IDisposable
	{
		public readonly List<GraphEdge> Edges = new List<GraphEdge>();
		public Vector2 Position;
		public int Index;

		public void Dispose()
		{
			Index = 0;
			Position.x = Position.y = 0f;
		}
	}

	public class Vertex : IDisposable
	{
		public float x;
		public float y;

		public float Dist(Vector2 point)
		{
			var dx = x - point.x;
			var dy = y - point.y;
			return Mathf.Sqrt(dx*dx + dy*dy);
		}

		public void Dispose()
		{
			x = 0f; y = 0f;
		}
	}
	
	public class Edge : IDisposable
	{
		public float a, b, c;
		public readonly Vertex[] ep = new Vertex[2];
		public readonly Polygon[] reg = new Polygon[2];

		public void Dispose()
		{
			a = b = c = 0f;
			ep[0] = null; ep[1] = null;
			reg[0] = null; reg[1] = null;
		}
	}
	
	public class Halfedge : IDisposable
	{
		public Halfedge ELleft, ELright;
		public Edge ELedge;
		public bool deleted;
		public int ELpm;
		public Vertex vertex;
		public float ystar;
		public Halfedge PQnext;

		public void Dispose()
		{
			ELleft = null;
			ELright = null; 
			PQnext = null;
			ELedge = null;
			deleted = false;
			ELpm = 0;
			vertex = null;
			ystar = 0f;
		}
	}
	
	public class GraphEdge : IDisposable
	{
		public Polygon polygon1, polygon2;
		public Vector2 point1, point2;

		public void Dispose()
		{
			polygon1 = polygon2 = null;
			point1 = point2 = new Vector2();
		}
	}
	
	public class SiteSorterYX : IComparer<Polygon>
	{
		public static readonly SiteSorterYX Instance = new SiteSorterYX();

		public int Compare ( Polygon p1, Polygon p2 )
		{
			if ( p1.Position.y < p2.Position.y) return -1;
			if ( p1.Position.y > p2.Position.y ) return 1;
			if ( p1.Position.x < p2.Position.x ) return -1;
			if ( p1.Position.x > p2.Position.x ) return 1;
			return 0;
		}
	}

	public class VoronoiPool<T> where T : IDisposable, new()
	{
		readonly Stack<T> pool = new Stack<T>();
		readonly List<T> active = new List<T>();

		public T Fetch()
		{
			var fetched = pool.Count == 0 ? new T() : pool.Pop();
			active.Add(fetched);
			return fetched;
		}

		public void ReleaseAll()
		{
			ReleaseAll(active);
		}

		public void ReleaseAll(List<T> toRelease)
		{
			if (toRelease == null) { return; }

			var count = toRelease.Count;
			for (int i = 0; i < count; i++)
			{
				var released = toRelease[i];
				if (released == null) { continue; }
				released.Dispose();
				pool.Push(released);
			}
			toRelease.Clear();
		}
	}
}
