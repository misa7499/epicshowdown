﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace VoronoiUtils
{
	/// <summary>
	/// Description of Voronoi.
	/// </summary>
	public class VoronoiGenerator : Singleton<VoronoiGenerator>
	{
		/*
			Where usable results will be pushed!
		*/
		Voronoi voronoi;

		/*
			Temproray shit used during generation.
		 */
		float borderMinX, borderMaxX, borderMinY, borderMaxY;
		int siteidx;
		float xmin, xmax, ymin, ymax, deltax, deltay;
		int nsites;
		Polygon[] sites;
		Polygon bottomsite;
		int sqrt_nsites;
		int PQcount;
		int PQmin;
		int PQhashsize;
		Halfedge[] PQhash;
		
		const int LE = 0;
		const int RE = 1;

		/*
			Buckets!
		 */
		int ELhashsize;
		Halfedge[] ELhash;
		Halfedge ELleftend, ELrightend;

		/*
			Memory managment
		*/
		readonly VoronoiPool<Halfedge> HalfedgePool = new VoronoiPool<Halfedge>();
		readonly VoronoiPool<Polygon> PolygonPool = new VoronoiPool<Polygon>();
		readonly VoronoiPool<Edge> EdgePool = new VoronoiPool<Edge>();
		readonly VoronoiPool<Vertex> VertexPool = new VoronoiPool<Vertex>();
		readonly VoronoiPool<GraphEdge> GraphEdgePool = new VoronoiPool<GraphEdge>();
		
		
		// ************* Public methods ******************
		// ******************************************
		
		/**
		 * 
		 * @param xValuesIn Array of X values for each site.
		 * @param yValuesIn Array of Y values for each site. Must be identical length to yValuesIn
		 * @param minX The minimum X of the bounding box around the voronoi
		 * @param maxX The maximum X of the bounding box around the voronoi
		 * @param minY The minimum Y of the bounding box around the voronoi
		 * @param maxY The maximum Y of the bounding box around the voronoi
		 * @return
		 */
		public void Generate(IList<Vector2> positions, float minX, float maxX, float minY, float maxY, Voronoi voronoi)
		{
			// prepare memory.
			this.voronoi = voronoi;
			GraphEdgePool.ReleaseAll(voronoi.Edges);
			PolygonPool.ReleaseAll(voronoi.Polygons);
			
			// prepare input data.
			sort(positions);
			
			// Check bounding box inputs - if mins are bigger than maxes, swap them
			float temp = 0;
			if ( minX > maxX )
			{
				temp = minX;
				minX = maxX;
				maxX = temp;
			}
			if ( minY > maxY )
			{
				temp = minY;
				minY = maxY;
				maxY = temp;
			}
			
			borderMinX = minX;
			borderMinY = minY;
			borderMaxX = maxX;
			borderMaxY = maxY;
			
			siteidx = 0;

			Profiler.BeginSample("Build diagram");
			{
				BuildDiagram();
			}
			Profiler.EndSample();

			// release all.
			Profiler.BeginSample("Release memory");
			{
				HalfedgePool.ReleaseAll();
				EdgePool.ReleaseAll();
				VertexPool.ReleaseAll();
			}
			Profiler.EndSample();
		}

		/*********************************************************
		 * Private methods - implementation details
		 ********************************************************/

		private void sort (IList<Vector2> positions)
		{
			nsites = positions.Count;
			sqrt_nsites = (int) Math.Sqrt ( (float)nsites + 4 );

			if (sites == null || sites.Length < nsites)
				Array.Resize(ref sites, nsites);

			xmin = positions[0].x;
			ymin = positions[0].y;
			xmax = positions[0].x;
			ymax = positions[0].y;
			for ( int i = 0; i < nsites; i++ )
			{
				sites[i] = pushPolygon(positions[i]);
				sites[i].Index = i;
				
				if ( positions[i].x < xmin )
					xmin = positions[i].x;
				else if ( positions[i].x > xmax )
					xmax = positions[i].x;
				
				if ( positions[i].y < ymin )
					ymin = positions[i].y;
				else if ( positions[i].y > ymax )
					ymax = positions[i].y;
			}
			deltax = xmax - xmin;
			deltay = ymax - ymin;

			// sort sites.
			Array.Sort(sites, SiteSorterYX.Instance);
		}
		
		private Polygon nextone ()
		{
			Polygon s;
			if ( siteidx < nsites )
			{
				s = sites[siteidx];
				siteidx++;
				return s;
			}
			return null;
		}
		
		private Edge bisect ( Polygon s1, Polygon s2 )
		{
			float dx, dy, adx, ady;
			Edge newedge;
			
			newedge = EdgePool.Fetch();
			
			newedge.reg[0] = s1;
			newedge.reg[1] = s2;
			
			newedge.ep [0] = null;
			newedge.ep[1] = null;
			
			dx = s2.Position.x - s1.Position.x;
			dy = s2.Position.y - s1.Position.y;
			
			adx = dx > 0 ? dx : -dx;
			ady = dy > 0 ? dy : -dy;
			newedge.c = (float)(s1.Position.x * dx + s1.Position.y * dy + (dx * dx + dy* dy) * 0.5);
			
			if ( adx > ady )
			{
				newedge.a = 1.0f;
				newedge.b = dy / dx;
				newedge.c /= dx;
			}
			else
			{
				newedge.a = dx / dy;
				newedge.b = 1.0f;
				newedge.c /= dy;
			}
					
			return newedge;
		}
				
		private int PQbucket ( Halfedge he )
		{
			int bucket;
			
			bucket = (int) ((he.ystar - ymin) / deltay * PQhashsize);
			if ( bucket < 0 )
				bucket = 0;
			if ( bucket >= PQhashsize )
				bucket = PQhashsize - 1;
			if ( bucket < PQmin )
				PQmin = bucket;
			
			return bucket;
		}
		
		// push the HalfEdge into the ordered linked list of vertices
		private void PQinsert ( Halfedge he, Vertex v, Polygon polygon)
		{
			Halfedge last, next;
			
			he.vertex = v;
			he.ystar = (float)(v.y + v.Dist(polygon.Position));
			last = PQhash [ PQbucket (he) ];
			
			while
			(
					(next = last.PQnext) != null
					&& 
					(he.ystar > next.ystar || (he.ystar == next.ystar && v.x > next.vertex.x))
				)
			{
				last = next;
			}
			
			he.PQnext = last.PQnext;
			last.PQnext = he;
			PQcount++;
		}
		
		// remove the HalfEdge from the list of vertices
		private void PQdelete ( Halfedge he )
		{
			Halfedge last;
			
			if (he.vertex != null)
			{
				last = PQhash [ PQbucket (he) ];
				while ( last.PQnext != he )
				{
					last = last.PQnext;
				}
				
				last.PQnext = he.PQnext;
				PQcount--;
				he.vertex = null;
			}
		}
		
		private bool PQempty ()
		{
			return ( PQcount == 0 );
		}
		
		private Vector2 PQ_min ()
		{
			var answer = new Vector2();
			
			while ( PQhash[PQmin].PQnext == null  )
			{
				PQmin++;
			}
			
			answer.x = PQhash[PQmin].PQnext.vertex.x;
			answer.y = PQhash[PQmin].PQnext.ystar;
			return answer;
		}
		
		private Halfedge PQextractmin ()
		{
			Halfedge curr;
			
			curr = PQhash[PQmin].PQnext;
			PQhash[PQmin].PQnext = curr.PQnext;
			PQcount--;
			
			return curr;
		}
		
		private Halfedge HEcreate(Edge e, int pm)
		{
			Halfedge answer = HalfedgePool.Fetch();
			answer.ELedge = e;
			answer.ELpm = pm;
			answer.PQnext = null;
			answer.vertex = null;
			
			return answer;
		}
		
		private bool ELinitialize()
		{
			ELhashsize = 2 * sqrt_nsites;

			Array.Resize(ref ELhash, ELhashsize);
			for (int i = 0; i < ELhashsize; i++)
			{
				ELhash[i] = null;
			}
			
			ELleftend = HEcreate ( null, 0 );
			ELrightend = HEcreate ( null, 0 );
			ELleftend.ELleft = null;
			ELleftend.ELright = ELrightend;
			ELrightend.ELleft = ELleftend;
			ELrightend.ELright = null;
			ELhash[0] = ELleftend;
			ELhash[ELhashsize - 1] = ELrightend;
			
			return true;
		}
				
		private Polygon leftreg( Halfedge he )
		{
			if (he.ELedge == null)
			{
				return bottomsite;
			}
			return (he.ELpm == LE ? he.ELedge.reg[LE] : he.ELedge.reg[RE]);
		}
		
		private void ELinsert( Halfedge lb, Halfedge newHe )
		{
			newHe.ELleft = lb;
			newHe.ELright = lb.ELright;
			(lb.ELright).ELleft = newHe;
			lb.ELright = newHe;
		}
		
		/*
		 * This delete routine can't reclaim node, since pointers from hash table
		 * may be present.
		 */
		private void ELdelete( Halfedge he )
		{
			(he.ELleft).ELright = he.ELright;
			(he.ELright).ELleft = he.ELleft;
			he.deleted = true;
		}
		
		/* Get entry from hash table, pruning any deleted nodes */
		private Halfedge ELgethash( int b )
		{
			Halfedge he;
			if (b < 0 || b >= ELhashsize)
				return null;
			
			he = ELhash[b];
			if (he == null || !he.deleted )
				return he;
			
			/* Hash table points to deleted half edge. Patch as necessary. */
			ELhash[b] = null;
			return null;
		}
		
		private Halfedge ELleftbnd(Vector2 p)
		{
			int bucket;
			Halfedge he;
			
			/* Use hash table to get close to desired halfedge */
			// use the hash function to find the place in the hash map that this
			// HalfEdge should be
			bucket = (int) ((p.x - xmin) / deltax * ELhashsize);
			
			// make sure that the bucket position is within the range of the hash
			// array
			if ( bucket < 0 ) bucket = 0;
			if ( bucket >= ELhashsize )	bucket = ELhashsize - 1;
			
			he = ELgethash ( bucket );
			
			// if the HE isn't found, search backwards and forwards in the hash map
			// for the first non-null entry
			if ( he == null )
			{
				for ( int i = 1; i < ELhashsize; i++ )
				{
					if ( (he = ELgethash ( bucket - i ) ) != null )
						break;
					if ( (he = ELgethash ( bucket + i ) ) != null )
						break;
				}
			}
			
			/* Now search linear list of halfedges for the correct one */
			if ( he == ELleftend || ( he != ELrightend && right_of (he, p) ) )
			{
				// keep going right on the list until either the end is reached, or
				// you find the 1st edge which the point isn't to the right of
				do
				{
					he = he.ELright;
				}
				while ( he != ELrightend && right_of(he, p) );
				he = he.ELleft;
			}
			else
				// if the point is to the left of the HalfEdge, then search left for
				// the HE just to the left of the point
			{
				do
				{
					he = he.ELleft;
				}
				while ( he != ELleftend && !right_of(he, p) );
			}
			
			/* Update hash table and reference counts */
			if ( bucket > 0 && bucket < ELhashsize - 1)
			{
				ELhash[bucket] = he;
			}
			
			return he;
		}
			
		private void clip_line( Edge e )
		{
			float pxmin, pxmax, pymin, pymax;
			Vertex s1, s2;
			
			float x1 = e.reg[0].Position.x;
			float y1 = e.reg[0].Position.y;
			float x2 = e.reg[1].Position.x;
			float y2 = e.reg[1].Position.y;
			
			// if the distance between the two points this line was created from is
			// less than the square root of 2, then ignore it
			// float x = x2- x1;
			// float y = y2 - y1;
			// if ( Math.Sqrt ( (x*x) + (y*y) ) < minDistanceBetweenSites )
			// {
			// 	return;
			// }
			pxmin = borderMinX;
			pymin = borderMinY;
			pxmax = borderMaxX;
			pymax = borderMaxY;
			
			if ( e.a == 1.0 && e.b >= 0.0 )
			{
				s1 = e.ep[1];
				s2 = e.ep[0];
			}
			else
			{
				s1 = e.ep[0];
				s2 = e.ep[1];
			}
			
			if ( e.a == 1.0 )
			{
				y1 = pymin;
				
				if ( s1 != null && s1.y > pymin )
					y1 = s1.y;
				if ( y1 > pymax )
					y1 = pymax;
				x1 = e.c - e.b * y1;
				y2 = pymax;
				
				if ( s2 != null && s2.y < pymax )
					y2 = s2.y;
				if ( y2 < pymin )
					y2 = pymin;
				x2 = e.c - e.b * y2;
				if ( ( (x1 > pxmax) & (x2 > pxmax) ) | ( (x1 < pxmin) & (x2 < pxmin) ) )
					return;
				
				if ( x1 > pxmax )
				{
					x1 = pxmax;
					y1 = ( e.c - x1 ) / e.b;
				}
				if ( x1 < pxmin )
				{
					x1 = pxmin;
					y1 = ( e.c - x1 ) / e.b;
				}
				if ( x2 > pxmax )
				{
					x2 = pxmax;
					y2 = ( e.c - x2 ) / e.b;
				}
				if ( x2 < pxmin )
				{
					x2 = pxmin;
					y2 = ( e.c - x2 ) / e.b;
				}
				
			}
			else
			{
				x1 = pxmin;
				if ( s1 != null && s1.x > pxmin )
					x1 = s1.x;
				if ( x1 > pxmax )
					x1 = pxmax;
				y1 = e.c - e.a * x1;
				
				x2 = pxmax;
				if ( s2 != null && s2.x < pxmax )
					x2 = s2.x;
				if ( x2 < pxmin )
					x2 = pxmin;
				y2 = e.c - e.a * x2;
				
				if (((y1 > pymax) & (y2 > pymax)) | ((y1 < pymin) & (y2 < pymin)))
					return;
				
				if ( y1 > pymax )
				{
					y1 = pymax;
					x1 = ( e.c - y1 ) / e.a;
				}
				if ( y1 < pymin )
				{
					y1 = pymin;
					x1 = ( e.c - y1 ) / e.a;
				}
				if ( y2 > pymax )
				{
					y2 = pymax;
					x2 = ( e.c - y2 ) / e.a;
				}
				if ( y2 < pymin )
				{
					y2 = pymin;
					x2 = ( e.c - y2 ) / e.a;
				}
			}
			
			pushGraphEdge ( e.reg[0], e.reg[1], x1, y1, x2, y2 );
		}
		
		private void endpoint( Edge e, int lr, Vertex s )
		{
			e.ep[lr] = s;
			if ( e.ep[RE - lr] == null )
				return;
			clip_line ( e );
		}
		
		/* returns true if p is to right of halfedge e */
		private bool right_of(Halfedge el, Vector2 p)
		{
			Edge e;
			Polygon topsite;
			bool right_of_site;
			bool above, fast;
			float dxp, dyp, dxs, t1, t2, t3, yl;
			
			e = el.ELedge;
			topsite = e.reg[1];
			
			if ( p.x > topsite.Position.x )
				right_of_site = true;
			else
				right_of_site = false;
			
			if ( right_of_site && el.ELpm == LE )
				return true;
			if (!right_of_site && el.ELpm == RE )
				return false;
			
			if ( e.a == 1.0 )
			{
				dxp = p.x - topsite.Position.x;
				dyp = p.y - topsite.Position.y;
				fast = false;
				
				if ( (!right_of_site & (e.b < 0.0)) | (right_of_site & (e.b >= 0.0)) )
				{
					above = dyp >= e.b * dxp;
					fast = above;
				}
				else
				{
					above = p.x + p.y * e.b > e.c;
					if ( e.b < 0.0 )
						above = !above;
					if ( !above )
						fast = true;
				}
				if ( !fast )
				{
					dxs = topsite.Position.x - ( e.reg[0] ).Position.x;
					above = e.b * (dxp * dxp - dyp * dyp) 
					< dxs * dyp * (1.0 + 2.0 * dxp / dxs + e.b * e.b);
					
					if ( e.b < 0 )
						above = !above;
				}
			}
			else // e.b == 1.0
			{
				yl = e.c - e.a * p.x;
				t1 = p.y - yl;
				t2 = p.x - topsite.Position.x;
				t3 = yl - topsite.Position.y;
				above = t1 * t1 > t2 * t2 + t3 * t3;
			}
			return ( el.ELpm == LE ? above : !above );
		}
		
		private Polygon rightreg(Halfedge he)
		{
			if (he.ELedge == (Edge) null)
				// if this halfedge has no edge, return the bottom site (whatever
				// that is)
			{
				return bottomsite;
			}

			// if the ELpm field is zero, return the site 0 that this edge bisects,
			// otherwise return site number 1
			return he.ELpm == LE ? he.ELedge.reg[RE] : he.ELedge.reg[LE];
		}
		
		// create a new site where the HalfEdges el1 and el2 intersect - note that
		// the Point in the argument list is not used, don't know why it's there
		private Vertex intersect( Halfedge el1, Halfedge el2 )
		{
			Edge e1, e2, e;
			Halfedge el;
			float d, xint, yint;
			bool right_of_site;
			
			e1 = el1.ELedge;
			e2 = el2.ELedge;
			
			if ( e1 == null || e2 == null )
				return null;
			
			// if the two edges bisect the same parent, return null
			if ( e1.reg[1] == e2.reg[1] )
				return null;
			
			d = e1.a * e2.b - e1.b * e2.a;
			if ( -1.0e-10 < d && d < 1.0e-10 )
				return null;
			
			xint = ( e1.c * e2.b - e2.c * e1.b ) / d;
			yint = ( e2.c * e1.a - e1.c * e2.a ) / d;
			
			if ( (e1.reg[1].Position.y < e2.reg[1].Position.y)
			    || (e1.reg[1].Position.y == e2.reg[1].Position.y && e1.reg[1].Position.x < e2.reg[1].Position.x) )
			{
				el = el1;
				e = e1;
			}
			else
			{
				el = el2;
				e = e2;
			}
			
			right_of_site = xint >= e.reg[1].Position.x;
			if ((right_of_site && el.ELpm == LE)
			    || (!right_of_site && el.ELpm == RE))
				return null;
			
			// create a new site at the point of intersection - this is a new vector
			// event waiting to happen
			var newVertex = VertexPool.Fetch();
			newVertex.x = xint;
			newVertex.y = yint;
			return newVertex;
		}
		
		/*
		 * implicit parameters: nsites, sqrt_nsites, xmin, xmax, ymin, ymax, deltax,
		 * deltay (can all be estimates). Performance suffers if they are wrong;
		 * better to make nsites, deltax, and deltay too big than too small. (?)
		 */
		private bool BuildDiagram()
		{
			Polygon newsite, bot, top, temp;
			Vertex p;
			Vertex v;
			Vector2 newintstar = new Vector2();
			int pm;
			Halfedge lbnd, rbnd, llbnd, rrbnd, bisector;
			Edge e;

			/*
				Initialize PQ, whatever that means
			 */
			PQcount = 0;
			PQmin = 0;
			PQhashsize = 4 * sqrt_nsites;

			Array.Resize(ref PQhash, PQhashsize);
			for ( int i = 0; i < PQhashsize; i++ )
			{
				PQhash [i] = HalfedgePool.Fetch();
			}

			ELinitialize();

			bottomsite = nextone();
			newsite = nextone();
			while (true)
			{
				if (!PQempty())
				{
					newintstar = PQ_min();
				}
				// if the lowest site has a smaller y value than the lowest vector
				// intersection,
				// process the site otherwise process the vector intersection

				if (newsite != null && (PQempty()
				                        || newsite.Position.y < newintstar.y
				                        || (newsite.Position.y == newintstar.y
				                            && newsite.Position.x < newintstar.x)))
				{
					/* new site is smallest -this is a site event */
					// get the first HalfEdge to the LEFT of the new site
					lbnd = ELleftbnd(newsite.Position);
					// get the first HalfEdge to the RIGHT of the new site
					rbnd = lbnd.ELright;
					// if this halfedge has no edge,bot =bottom site (whatever that
					// is)
					bot = rightreg(lbnd);
					// create a new edge that bisects
					e = bisect(bot, newsite);

					// create a new HalfEdge, setting its ELpm field to 0
					bisector = HEcreate(e, LE);
					// insert this new bisector edge between the left and right
					// vectors in a linked list
					ELinsert(lbnd, bisector);

					// if the new bisector intersects with the left edge,
					// remove the left edge's vertex, and put in the new one
					if ((p = intersect(lbnd, bisector)) != null)
					{
						PQdelete(lbnd);
						PQinsert(lbnd, p, newsite);
					}
					lbnd = bisector;
					// create a new HalfEdge, setting its ELpm field to 1
					bisector = HEcreate(e, RE);
					// insert the new HE to the right of the original bisector
					// earlier in the IF stmt
					ELinsert(lbnd, bisector);

					// if this new bisector intersects with the new HalfEdge
					if ((p = intersect(bisector, rbnd)) != null)
					{
						// push the HE into the ordered linked list of vertices
						PQinsert(bisector, p, newsite);
					}
					newsite = nextone();
				} else if (!PQempty())
					/* intersection is smallest - this is a vector event */
				{
					// pop the HalfEdge with the lowest vector off the ordered list
					// of vectors
					lbnd = PQextractmin();
					// get the HalfEdge to the left of the above HE
					llbnd = lbnd.ELleft;
					// get the HalfEdge to the right of the above HE
					rbnd = lbnd.ELright;
					// get the HalfEdge to the right of the HE to the right of the
					// lowest HE
					rrbnd = rbnd.ELright;
					// get the Site to the left of the left HE which it bisects
					bot = leftreg(lbnd);
					// get the Site to the right of the right HE which it bisects
					top = rightreg(rbnd);

					v = lbnd.vertex; // get the vertex that caused this event
					// earlier since we didn't know when it would be processed
					endpoint(lbnd.ELedge, lbnd.ELpm, v);
					// set the endpoint of
					// the left HalfEdge to be this vector
					endpoint(rbnd.ELedge, rbnd.ELpm, v);
					// set the endpoint of the right HalfEdge to
					// be this vector
					ELdelete(lbnd); // mark the lowest HE for
					// deletion - can't delete yet because there might be pointers
					// to it in Hash Map
					PQdelete(rbnd);
					// remove all vertex events to do with the right HE
					ELdelete(rbnd); // mark the right HE for
					// deletion - can't delete yet because there might be pointers
					// to it in Hash Map
					pm = LE; // set the pm variable to zero

					if (bot.Position.y > top.Position.y)
						// if the site to the left of the event is higher than the
						// Site
					{ // to the right of it, then swap them and set the 'pm'
						// variable to 1
						temp = bot;
						bot = top;
						top = temp;
						pm = RE;
					}
					e = bisect(bot, top); // create an Edge (or line)
					// that is between the two Sites. This creates the formula of
					// the line, and assigns a line number to it
					bisector = HEcreate(e, pm); // create a HE from the Edge 'e',
					// and make it point to that edge
					// with its ELedge field
					ELinsert(llbnd, bisector); // insert the new bisector to the
					// right of the left HE
					endpoint(e, RE - pm, v); // set one endpoint to the new edge
					// to be the vector point 'v'.
					// If the site to the left of this bisector is higher than the
					// right Site, then this endpoint
					// is put in position 0; otherwise in pos 1

					// if left HE and the new bisector intersect, then delete
					// the left HE, and reinsert it
					if ((p = intersect(llbnd, bisector)) != null)
					{
						PQdelete(llbnd);
						PQinsert(llbnd, p, bot);
					}

					// if right HE and the new bisector intersect, then
					// reinsert it
					if ((p = intersect(bisector, rrbnd)) != null)
					{
						PQinsert(bisector, p, bot);
					}
				} else
				{
					break;
				}
			}

			for (lbnd = ELleftend.ELright; lbnd != ELrightend; lbnd = lbnd.ELright)
			{
				e = lbnd.ELedge;
				clip_line(e);
			}

			return true;
		}

		/*
			Methods which push finished usable data to the target Voronoi.
		 */
		private GraphEdge pushGraphEdge( Polygon leftPolygon, Polygon rightPolygon, float x1, float y1, float x2, float y2 )
		{
			GraphEdge newEdge = GraphEdgePool.Fetch();
			newEdge.point1.x = x1;
			newEdge.point1.y = y1;
			newEdge.point2.x = x2;
			newEdge.point2.y = y2;
			newEdge.polygon1 = leftPolygon;
			newEdge.polygon2 = rightPolygon;
			voronoi.Edges.Add(newEdge);
			return newEdge;
		}

		private Polygon pushPolygon( Vector2 position )
		{
			var newPolygon = PolygonPool.Fetch();
			newPolygon.Position = position;
			voronoi.Polygons.Add(newPolygon);
			return newPolygon;
		}

	}
}