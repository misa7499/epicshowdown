using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class DelanauyDemo : MonoBehaviour
{
	public readonly DelanauyMesh DelanauyMesh = new DelanauyMesh();
	public Mesh Mesh { get; private set; }

	public void Update()
	{
		// has to have a mesh filter
		var meshFilter = GetComponent<MeshFilter>();
		if (meshFilter == null)
			return;

		// generate mesh on first run
		if (Mesh == null)
		{
			Mesh = new Mesh();
			Mesh.MarkDynamic();
		}

		// convert 3d points to vertices
		var vertices = new NVector2[transform.childCount];
		for(int i = 0; i < vertices.Length; i++)
		{
			vertices[i] = transform.GetChild(i).transform.localPosition.ToNVector2();
		}

		// actual triangulation
		DelanauyMesh.GenerateTriangulation(vertices, -20, 20, -20, 20);

		// apply delanauy to unity3D mesh
		Mesh.Clear();
		Mesh.vertices = DelanauyMesh.VertexBuffer.Select(v => new Vector3()
		{
			x = (float)v.originalPosition.x,
			z = (float)v.originalPosition.y
		}).ToArray();

		// apply triangles to unity3d mesh.
		var triangleCount = DelanauyMesh.TriangleBuffer.Count;
		var triangles = new int[triangleCount * 3];
		for(int i = 0; i < triangleCount; i++)
		{
			var triangle = DelanauyMesh.TriangleBuffer[i];
			triangles[i*3] = triangle.v0;
			triangles[i*3+1] = triangle.v1;
			triangles[i*3+2] = triangle.v2;
		}
		Mesh.triangles = triangles;

		// show mesh
		Mesh.RecalculateBounds();
		meshFilter.sharedMesh = Mesh;
	}
}