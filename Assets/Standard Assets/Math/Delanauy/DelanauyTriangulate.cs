using System;
using System.Collections.Generic;
using UnityEngine;

public class DelanauyMesh
{
	public NFloat LeftEdge, RightEdge, TopEdge, BottomEdge;
	public NFloat Width, Height;

	public readonly ArrayBuffer<DelanauyVertex> VertexBuffer = new ArrayBuffer<DelanauyVertex>();
	public readonly ArrayBuffer<DelanauyTriangle> TriangleBuffer = new ArrayBuffer<DelanauyTriangle>();

	/// <summary>
	/// When inserting each point, this stack will be initialized with triangle indicies which potentially require flipping
	/// in order to perserve optimized triangulation.
	/// </summary>
	readonly Stack<int> delanauyFlipStack = new Stack<int>();

	public void GenerateTriangulation(NVector2[] vertices, NFloat leftEdge, NFloat rightEdge, NFloat topEdge, NFloat bottomEdge)
	{
		// clear old data
		VertexBuffer.Count = 0;
		TriangleBuffer.Count = 0;

		// calculate width and height of the point set for 0-1 normalization
		LeftEdge = leftEdge;
		TopEdge = topEdge;
		BottomEdge = bottomEdge;
		RightEdge = rightEdge;
		Width = rightEdge - leftEdge;
		Height = bottomEdge - topEdge;

		// add edges
		VertexBuffer.Add(new DelanauyVertex()
		{
			originalPosition = new NVector2(leftEdge, topEdge),
			x = 0, y = 0,
		});
		VertexBuffer.Add(new DelanauyVertex()
		{
			originalPosition = new NVector2(rightEdge, topEdge),
			x = 1, y = 0,
		});
		VertexBuffer.Add(new DelanauyVertex()
		{
			originalPosition = new NVector2(rightEdge, bottomEdge),
			x = 1, y = 1,
		});
		VertexBuffer.Add(new DelanauyVertex()
		{
			originalPosition = new NVector2(leftEdge, bottomEdge),
			x = 0, y = 1,
		});

		// add edge triangles
		// ---
		TriangleBuffer.Add(new DelanauyTriangle()
		{
			v0 = 2,	v1 = 1,	v2 = 0,
			t0 = 1, t1 = -1, t2 = -1,
		});
		// add edge triangles
		TriangleBuffer.Add(new DelanauyTriangle()
		{
			v0 = 3, v1 = 2, v2 = 0,
			t0 = 0, t1 = -1, t2 = -1
		});
		// ---

		// add Vertex set
		for(int i = 0; i < vertices.Length; i++)
		{
			AddVertex(vertices[i]);
		}
	}

	void AddVertex(NVector2 position)
	{
		// normalized coordinates
		var x = (position.x - LeftEdge) / Width;
		var y = (position.y - TopEdge) / Height;

		// initial data
		var newVertex = new DelanauyVertex() { originalPosition = position, x = x, y = y };
		var newV = VertexBuffer.Count;

		// find proper triangle
		var triangleCount = TriangleBuffer.Count;
		var triangleBuffer = TriangleBuffer.buffer;
		for(int i = triangleCount - 1; i >= 0; i--)
		{
			// we must first find a triangle that is enclosing the new point
			ref var triangle = ref triangleBuffer[i];
			if (!IsInsideTriangle(x, y, triangle)) { continue; }

			// split existing triangle into three new triangles
			TriangleBuffer.Add(new DelanauyTriangle()
			{
				v0 = triangle.v0, v1 = triangle.v1, v2 = newV,
				t0 = triangleCount + 1, t1 = triangleCount + 2, t2 = triangle.t2,
			});
			TriangleBuffer.Add(new DelanauyTriangle()
			{
				v0 = triangle.v0, v1 = newV, v2 = triangle.v2,
				t0 = triangleCount + 0, t1 = triangleCount + 2, t2 = triangle.t1,
			});
			TriangleBuffer.Add(new DelanauyTriangle()
			{
				v0 = newV, v1 = triangle.v1, v2 = triangle.v2,
				t0 = triangleCount + 0, t1 = triangleCount + 1, t2 = triangle.t0,
			});

			// new triangles may break delanauy condition.
			// potential flipping time!
			delanauyFlipStack.Clear();
			delanauyFlipStack.Push(triangle.t0);
			delanauyFlipStack.Push(triangle.t1);
			delanauyFlipStack.Push(triangle.t2);

			// flip if necessary
			while(delanauyFlipStack.Count > 0)
			{
				var maybeFlipTriangle = delanauyFlipStack.Pop();

				if (NeedsToFlipTriangle(newVertex, maybeFlipTriangle))
				{

				}
			}

			// remove the old one
			TriangleBuffer.RemoveAt(i);

			// done
			break;
		}

		VertexBuffer.Add(newVertex);
	}

	private bool NeedsToFlipTriangle(DelanauyVertex newVertex, int maybeFlipTriangle)
	{
		return false;
	}

	bool IsInsideTriangle(NFloat x, NFloat y, in DelanauyTriangle triangle)
	{
		// fetch vertices
		ref var v1 = ref VertexBuffer.buffer[triangle.v0];
		ref var v2 = ref VertexBuffer.buffer[triangle.v1];
		ref var v3 = ref VertexBuffer.buffer[triangle.v2];

		var dx_01 = x.value - v1.x.value;
		var dy_01 = y.value - v1.y.value;

		var dx_21 = v2.x.value - v1.x.value;
		var dy_21 = v2.y.value - v1.y.value;

		var dx_31 = v3.x.value - v1.x.value;
		var dy_31 = v3.y.value - v1.y.value;

		if ((dx_21*dy_01 > dx_01*dy_21) != (dx_01*dy_31 > dx_31*dy_01))
		{
			return false;
		}

		var dx_02 = x.value - v2.x.value;
		var dy_02 = y.value - v2.y.value;

		var dx_12 = v1.x.value - v2.x.value;
		var dy_12 = v1.y.value - v2.y.value;

		var dx_32 = v3.x.value - v2.x.value;
		var dy_32 = v3.y.value - v2.y.value;

		return (dx_12*dy_02 > dx_02*dy_12) == (dx_02*dy_32 > dx_32*dy_02);
	}
}

public struct DelanauyVertex
{
	public NVector2 originalPosition;
	public NFloat x;
	public NFloat y;
}

public struct DelanauyTriangle
{
	public int v0, v1, v2;
	public int t0, t1, t2;
}