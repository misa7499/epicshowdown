using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class HUDController : MonoSingleton<HUDController>
{
	public static ScreenSpaceBillboard Spawn(string path, GameObject tracked)
	{
		return Spawn(Resources.Load<ScreenSpaceBillboard>(path), tracked.transform);
	}

	public static ScreenSpaceBillboard Spawn(string path, Component tracked)
	{
		return Spawn(Resources.Load<ScreenSpaceBillboard>(path), tracked.transform);
	}

	public static ScreenSpaceBillboard Spawn(ScreenSpaceBillboard prefab, Component tracked)
	{
		return Spawn(prefab, tracked.transform);
	}

	public static ScreenSpaceBillboard Spawn(ScreenSpaceBillboard prefab, Transform tracked)
	{
		var spawned = ScreenSpaceBillboard.Instantiate(prefab, Instance.transform);
		spawned.canvas = Instance.GetComponent<RectTransform>();
		spawned.trackedTransform = tracked;
		return spawned;
	}

	public void SetScale(float v)
	{
		var canvas = GetComponent<Canvas>();
		if (canvas.scaleFactor != v) { canvas.scaleFactor = v; }
	}
}