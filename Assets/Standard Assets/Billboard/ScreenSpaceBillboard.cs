﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScreenSpaceBillboard : MonoBehaviour, EditorUpdate
{
	public Transform trackedTransform;
	public RectTransform canvas;
	public float Height;
	public float Offset2D;

#if UNITY_EDITOR
	public void OnWillRenderObject()
	{
		if (!Application.isPlaying) { EditorUpdate(); }
	}
#endif

	public void LateUpdate()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying) { return; }
#endif
		DoBillboarding(transform, CachedMainCamera.Instance, trackedTransform.position, Height, Offset2D, canvas);
	}

	public void EditorUpdate()
	{
		if (trackedTransform == null) { return; }
		DoBillboarding(transform, Camera.main, trackedTransform.position, Height, Offset2D, canvas);
	}

	public static void DoBillboarding(Transform transform, Camera camera, Vector3 position, float height3D, float offset2D, RectTransform canvas = null)
	{
		if (camera == null) 
		{ 
			return; 
		}

		// convert 3d position to viewport coordinates
		var trackedPosition = position;
		trackedPosition.y += height3D;
		var screenSpacePosition = camera.WorldToViewportPoint(trackedPosition);

		// screen space offset
		if (offset2D != 0)
		{
			if (!camera.orthographic)
			{
				screenSpacePosition.y += offset2D / (Mathf.Tan(camera.fieldOfView * AnglesToRadians) * screenSpacePosition.z);
			}
			else
			{
				screenSpacePosition.y += offset2D / camera.orthographicSize;
			}
		}

		if (canvas != null)
		{
			var rectTransform = (RectTransform)transform;
	        rectTransform.localPosition = new Vector2((screenSpacePosition.x - 0.5f) * canvas.sizeDelta.x, 
								(screenSpacePosition.y - 0.5f) * canvas.sizeDelta.y);
		}
		else
		{
			transform.SetPositionAndRotation(camera.ViewportToWorldPoint(screenSpacePosition), camera.transform.rotation * Rotate180Degrees);
		}
	}

	static Quaternion Rotate180Degrees = Quaternion.AngleAxis(180f, Vector3.up);
	const float AnglesToRadians = Mathf.PI / 360f;
}

public static class CachedMainCamera
{
	static Camera camera;
	public static Camera Instance
	{
		get
		{
			if (camera == null)	{ camera = Camera.main; }
			return camera;
		}
	}
}