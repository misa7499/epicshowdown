using System;
using System.Collections.Generic;
using UnityEngine;

public class BlobMeshBatch : MonoSingleton<BlobMeshBatch>
{
	public const int MaxBlobs = 50;
	readonly Vector4[] BlobData = new Vector4[MaxBlobs];

	public Renderer Renderer;

	/// <summary>
	/// All blobs currently on screen.
	/// </summary>
	public readonly List<Transform> blobs = new List<Transform>();

	public void Awake()
	{
		transform.position = new Vector3();
		transform.localScale = Vector3.one;
		transform.rotation = Quaternion.identity;
		if (Renderer == null) { Renderer = BlobMeshBatch.Instance.GetComponent<Renderer>(); }
		GetComponent<MeshFilter>().sharedMesh.bounds = new Bounds(Vector3.zero, new Vector3(10000, 10000, 10000));
	}

	public void Add(Transform blobMesh)
	{
		if (blobs.Contains(blobMesh)) return;
		blobs.Add(blobMesh);
	}

	public void Remove(Transform blobMesh)
	{
		blobs.Remove(blobMesh);
	}

	public void LateUpdate()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			// Used for debugging in editor
			blobs.Clear();
			foreach(var blobMeshBatch in GameObject.FindObjectsOfType<BlobMesh>())
				blobs.Add(blobMeshBatch.transform);
		}
#endif

		// pass on transforms to the shader.
		for(int i = 0; i < MaxBlobs; i++)
		{
			if (blobs.Count > i)
			{
				// safety check
				if (blobs[i] == null)
				{
					blobs.RemoveAt(i--);
					continue;
				}

				var position = blobs[i].position;
				var scale = blobs[i].lossyScale;
				BlobData[i] = new Vector4(position.x, position.z, scale.x, scale.z);
			}
			else
			{
				BlobData[i].x = -10;
				BlobData[i].y = -10;
				BlobData[i].z = 0;
				BlobData[i].w = 0;
			}
		}

		Renderer.sharedMaterial.SetVectorArray("_BatchData", BlobData);
	}
}

#if UNITY_EDITOR

/// <summary>
/// Makes sure this can work in editor.
/// </summary>
[UnityEditor.InitializeOnLoad]
public class BlobMeshBatchEditorUpdate
{
	static BlobMeshBatchEditorUpdate()
	{
		UnityEditor.EditorApplication.update += () =>
		{
			if (!Application.isPlaying && BlobMeshBatch.Instance != null)
			{
				BlobMeshBatch.Instance.LateUpdate();
			}
		};
	}
}

#endif