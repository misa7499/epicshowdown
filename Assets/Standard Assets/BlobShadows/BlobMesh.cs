﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobMesh : MonoBehaviour
{
	public void OnEnable()
	{
		if (BlobMeshBatch.Instance != null)
			BlobMeshBatch.Instance.Add(transform);
	}

	public void OnDisable()
	{
		if (BlobMeshBatch.Instance != null)
			BlobMeshBatch.Instance.Remove(transform);
	}
}