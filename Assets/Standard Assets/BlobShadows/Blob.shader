Shader "Custom/Blob"
{
	Properties
	{
		_Color ("Color", Color) = (0, 0, 0, 0.9)
		_PositionY ("Position Y", float) = 0.02
	}
	SubShader
	{
		LOD 100

		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
		}
		
		Pass
		{
			Cull Off
			Lighting Off
			ZWrite Off
			Fog { Mode Off }
			Offset -1, -1
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			// We leave the premultiplication on since we have alpha blending defined, so the shader
			// do not multiplies the color of the texture by its alpha
			#define GUI_TEXTURE_MULTIPLY_ALPHA_OFF
			
			struct VertexInput
			{
				float4 position : POSITION;
				half4 color : COLOR;
				//half2 uv1 : TEXCOORD0;
				float2 uv4 : TEXCOORD3;
			};

			struct VertexToFragment
			{
				float4 position : SV_POSITION;
				half4 color : COLOR;
				//half2 uv1 : TEXCOORD0;
				float index : TEXCOORD3;
			};

			uniform float4 _BatchData[50]; // x y - position, z w - scale
			uniform float4 _Color;
			uniform float _PositionY;
			uniform sampler2D _MainTex;

			VertexToFragment vert(VertexInput input)
			{
				VertexToFragment output = (VertexToFragment)0;
				//output.uv1 = input.uv1;
				output.index = input.uv4.x;
				output.color = input.color;

				float4 data = _BatchData[output.index];
				output.position = UnityObjectToClipPos(float4(input.position.x * data.z + data.x, _PositionY + input.position.y, input.position.z * data.w + data.y, input.position.w));

				return output;
			}

			half4 frag(VertexToFragment input) : COLOR
			{
				return input.color * _Color;
			}

			ENDCG
		}
	} 
}
