using UnityEngine;

public static class CameraRaycastUtil
{
	private static Plane GroundPlane = new Plane(Vector3.up, 0f);

	public static bool GetPoint(out Vector3 point)
	{
		return GetPoint(Input.mousePosition, out point);
	}

	public static bool GetPoint(Vector3 mousePosition, out Vector3 point)
	{
		return Camera.main.GetPoint(mousePosition, out point);
	}

	public static bool GetPoint(this Camera camera, Vector3 mousePosition, out Vector3 point)
	{
		var ray = camera.ScreenPointToRay(mousePosition);
		if (GroundPlane.Raycast(ray, out float dist))
		{
			point = ray.GetPoint(dist);
			return true;
		}

		point = Vector3.zero;
		return false;
	}
}