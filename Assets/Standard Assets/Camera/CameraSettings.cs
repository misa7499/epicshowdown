﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CameraSettings : ScriptableObject<CameraSettings>
{
	[Header("Camera Angle")]
	public float Pitch = 60;
	public float Yaw = 0f;

	[Header("FOV")]
	public bool IsOrthographic = false;
	public float FieldOfView = 50;
	public bool AdjustFOVForAspectRatio;
	[Indent, ShowIf("AdjustFOVForAspectRatio")] public float MinimumAspectRatio;

	[Header("Panning")]
	public bool PanCameraOnDrag = true;
	public float PanSpeed = 10;

	[Header("Double Tap")]
	public float DoubleTapDelay = 0.25f;

	[Header("Zooming")]
	public float StartDistance = 2;
	public float MinDistance = 1;
	public float MaxDistance = 20;
	public float ScrollSpeed = 300;

	[Header("Animator")]
	public RuntimeAnimatorController Animator;
}