using UnityEngine;

public static class RaycastUtil
{
	/// <summary>
	/// Returns Float.MaxValue if nothing is hit, and distance to hit point otherwise.
	/// </summary>
	public static float PlaneCheck(Vector3 origin, Vector3 dir, Vector3 normal, Vector3 plane)
	{
		var dot = Vector3.Dot(dir, normal);
		if (dot == 0) { return float.MaxValue; }

		var dist = Vector3.Dot(plane - origin, normal) / dot;
		return dist >= 0f ? dist : float.MaxValue;
	}

	/// <summary>
	/// Returns Float.MaxValue if nothing is hit, and distance to hit point otherwise.
	/// </summary>
	public static float SphereCheck(Vector3 origin, Vector3 dir, Vector3 center, float radius)
	{
		var dist = origin - center;
		var d = Vector3.Dot(dir, dist);
		var det = d*d - dist.sqrMagnitude + radius * radius;
		if (det < 0) { return float.MaxValue; } // nothing!
		return -d - Mathf.Sqrt(det);
	}

	/// <summary>
	/// Returns Float.MaxValue if nothing is hit, and distance to hit point otherwise.
	/// </summary>
	public static float BoxCheck(Vector3 origin, Vector3 dir, Vector3 center, Vector3 size)
	{
		float dist = float.MaxValue;
		dist = Mathf.Min(dist, BoxPlaneCheck(origin, dir, new Vector3( 1, 0, 0), new Vector3(center.x + size.x, center.y, center.z), size));
		dist = Mathf.Min(dist, BoxPlaneCheck(origin, dir, new Vector3(-1, 0, 0), new Vector3(center.x - size.x, center.y, center.z), size));
		dist = Mathf.Min(dist, BoxPlaneCheck(origin, dir, new Vector3( 0, 1, 0), new Vector3(center.x, center.y + size.y, center.z), size));
		dist = Mathf.Min(dist, BoxPlaneCheck(origin, dir, new Vector3( 0,-1, 0), new Vector3(center.x, center.y - size.y, center.z), size));
		dist = Mathf.Min(dist, BoxPlaneCheck(origin, dir, new Vector3( 0, 0, 1), new Vector3(center.x, center.y, center.z + size.z), size));
		dist = Mathf.Min(dist, BoxPlaneCheck(origin, dir, new Vector3( 0, 0,-1), new Vector3(center.x, center.y, center.z - size.z), size));
		return dist;
	}

	public static float BoxPlaneCheck(Vector3 origin, Vector3 dir, Vector3 normal, Vector3 plane, Vector3 size)
	{
		var dot = Vector3.Dot(dir, normal);
		if (dot == 0) { return float.MaxValue; }

		var dist = Vector3.Dot(plane - origin, normal) / dot;
		if (dist <= 0) { return float.MaxValue; }

		var point = origin + dir * dist;
		if (Mathf.Abs(point.x - plane.x) > size.x) { return float.MaxValue; }
		if (Mathf.Abs(point.y - plane.y) > size.y) { return float.MaxValue; }
		if (Mathf.Abs(point.z - plane.z) > size.z) { return float.MaxValue; }

		return dist;
	}	
}