using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CameraController<C> :
	/* Captures input events not captured by UnityUI */
	BaseRaycaster, IBeginDragHandler, IDragHandler, IEndDragHandler, IScrollHandler, IPointerClickHandler, IPointerDownHandler, EditorUpdate

	/* Allows extending functionality */
	where C : CameraController<C>
{
	/* Instructions on how this is camera created. */
	public CameraSettings Settings;

	/* Camera is embedded on child gameObject */
	public Camera Camera;

	/* Position at which camera is pointed. */
	[Header("Position")]
	public Vector3 Position;

	/* Pan Limits */
	[Header("Pan limits")]
	public bool HasPanLimits = true;
	public float LeftEdge = -10;
	public float RightEdge = 10;
	public float TopEdge = -10;
	public float BottomEdge = 10;

	/* Current distance for camera */
	[Header("Zooming")]
	public float Distance;
	public float? activePinch;

	public static C Spawn(CameraSettings settings)
	{
		/* Main gameObject */
		var gameObject = new GameObject("CameraController");
		var cameraController = gameObject.AddComponent<C>();

		/* Event system, if needed */
		if (FindObjectOfType<EventSystem>() == null) { gameObject.AddComponent<EventSystem>().pixelDragThreshold = 0; }
		if (FindObjectOfType<BaseInputModule>() == null) { gameObject.AddComponent<StandaloneInputModule>(); }

		/* Child gameObject where camera component will be attached */
		var cameraGameObject = new GameObject("Camera");
		cameraGameObject.tag = "MainCamera";
		cameraGameObject.transform.parent = gameObject.transform;
		cameraGameObject.transform.localPosition = new Vector3(0, 0, -1);

		// animator!
		if (settings.Animator != null)
		{
			var animatorGO = new GameObject("CameraAnimator");
			var animator = animatorGO.AddComponent<Animator>();
			animator.runtimeAnimatorController = settings.Animator;
			gameObject.transform.parent = animatorGO.transform;
		}

		/* Camera component */
		cameraController.Settings = settings;
		cameraController.Distance = settings.StartDistance;
		cameraController.Camera = cameraGameObject.AddComponent<Camera>();
		cameraController.Update();

		/* Camera raycast component */
		return cameraController;
	}

	public void Update()
	{
		ReadInput();
	}

	public virtual void ReadInput()
	{
		if (KeyboardPanningAllowed)
		{
			var angle = -Settings.Yaw / 180f * Mathf.PI;
			var xOffset = Input.GetAxis("Horizontal") * Settings.PanSpeed * Time.deltaTime;
			var yOffset = Input.GetAxis("Vertical") * Settings.PanSpeed * Time.deltaTime;
			Position.x += xOffset * Mathf.Cos(angle) - yOffset * Mathf.Sin(angle);
			Position.z += xOffset * Mathf.Sin(angle) + yOffset * Mathf.Cos(angle);
		}
	}

	public virtual void LateUpdate()
	{
		Sync();
	}

	public void Sync()
	{
		/* Position */
		SyncPosition();

		/* Zoom  */
		Distance = Mathf.Clamp(Distance, Settings.MinDistance, Settings.MaxDistance);
		transform.localScale = new Vector3(1f, 1f, Distance);
		Camera.orthographic = Settings.IsOrthographic;
		Camera.orthographicSize = Distance;

		/* Rotation */
		transform.localRotation = Quaternion.Euler(Settings.Pitch, Settings.Yaw, 0f);

		/* Field of View */
		if (!Settings.IsOrthographic)
		{
			SyncFOV();
		}
	}

	public void SyncFOV()
	{
		var fov = Settings.FieldOfView;
		if (Settings.AdjustFOVForAspectRatio)
		{
			var aspectRatio = Screen.width / (float)Screen.height;
			if (aspectRatio < Settings.MinimumAspectRatio)
			{
				fov *= Settings.MinimumAspectRatio / aspectRatio;
			}
		}
		Camera.fieldOfView = fov;
	}

	private void SyncPosition()
	{
		if (HasPanLimits)
		{
			Position.x = Mathf.Clamp(Position.x, LeftEdge, RightEdge);
			Position.z = Mathf.Clamp(Position.z, TopEdge, BottomEdge);
		}
		transform.localPosition = Position;
	}

	/// <summary>
	/// Position that was grabbed when dragging has started.
	/// </summary>
	private Vector3 grabbedPosition;

	/// <summary>
	/// Used for calculating is next tap a double tap.
	/// </summary>
	private float lastTapTime;

	/// <summary>
	/// When begging dragging, dispatch event.
	/// </summary>
	public void OnBeginDrag(PointerEventData evt)
	{
		if (Camera.GetPoint(Input.mousePosition, out grabbedPosition))
		{
			DragStarted(evt);
		}
	}

	/// <summary>
	/// Move camera around when dragging.
	/// </summary>
	public void OnDrag(PointerEventData evt)
	{
		if (HandlePinch())
		{
			return;
		}
		if (!Settings.PanCameraOnDrag)
		{
			Drag(evt);
			return;
		}

		if (evt.dragging && Camera.GetPoint(Input.mousePosition, out Vector3 currentPosition))
		{
			var dragCaptured = CaptureDrag(evt, currentPosition); // somebody wants to use drag for something else other than free moving?
			if (!dragCaptured)
			{
				Position -= currentPosition - grabbedPosition;
				SyncPosition();
				Camera.GetPoint(Input.mousePosition, out grabbedPosition);
			}
			Drag(evt);
		}
	}

	/// <summary>
	/// Returns true if currently pinching.
	/// </summary>
	public bool HandlePinch()
	{
		if (Input.touchCount > 1)
		{
			var touchA = Input.GetTouch(0);
			var touchB = Input.GetTouch(1);
			var touchDist = Vector2.Distance(touchA.position, touchB.position);

			if (activePinch == null)
			{
				activePinch = touchDist * Distance;
			}
			else
			{
				Distance = activePinch.Value / touchDist;

				// update reference pinch value if going over camera zoom limits
				if (Distance < Settings.MinDistance)
				{
					Distance = Settings.MinDistance;
					activePinch = touchDist * Distance;
				}
				if (Distance > Settings.MaxDistance)
				{
					Distance = Settings.MaxDistance;
					activePinch = touchDist * Distance;
				}

			}

			return true;
		}
		else
		{
			if (activePinch != null)
			{
				// reset drag if returned to one finger dragging.
				if (Input.touchCount > 0) { Camera.GetPoint(Input.GetTouch(0).position, out grabbedPosition); }
				activePinch = null;
			}
			return false;
		}
	}

	/// <summary>
	/// Does the implementation want to capture the drag for its own purposes?
	/// If not, normal camera drag will follow.
	/// </summary>
	public virtual bool CaptureDrag(PointerEventData data, Vector3 currentPosition) => false;

	/// <summary>
	/// Should panning be allowed at the moment?
	/// </summary>
	public virtual bool KeyboardPanningAllowed => true;

	/// <summary>
	/// Dispatch drag end.
	/// </summary>
	public void OnEndDrag(PointerEventData evt)
	{
		// end pinch, if this was a pinch
		if (activePinch != null)
		{
			activePinch = null;
			return;
		}

		Camera.GetPoint(Input.mousePosition, out Vector3 currentPosition);
		DragEnded(evt);
	}

	/// <summary>
	/// Zoom Camera in/out when scrolling.
	/// </summary>
	public void OnScroll(PointerEventData eventData)
	{
		Distance -= Input.GetAxis("Mouse ScrollWheel") * Settings.ScrollSpeed * Time.deltaTime;
		Sync();
	}

	/// <summary>
	/// Dispatch event when ground gets clicked.
	/// </summary>
	public void OnPointerClick(PointerEventData evt)
	{
		if (Camera.GetPoint(evt.position, out Vector3 groundPoint))
		{
			OnGroundClick(evt, groundPoint);
			if (Time.realtimeSinceStartup - lastTapTime <= Settings.DoubleTapDelay)
			{
				OnGroundDoubleTap(evt, groundPoint);
			}
			lastTapTime = Time.realtimeSinceStartup;
		}
	}

	/// <summary>
	/// Dispatches OnPointerDown event
	/// </summary>
	public void OnPointerDown(PointerEventData evt)
	{
		if (Camera.GetPoint(evt.position, out Vector3 groundPoint))
		{
			OnGroundTap(evt);
		}
	}

	public virtual void OnGroundTap(PointerEventData evt) {}
	public virtual void OnGroundClick(PointerEventData evt, Vector3 groundPoint) {}
	public virtual void OnGroundDoubleTap(PointerEventData evt, Vector3 groundPoint) {}
	public virtual void Drag(PointerEventData evt) {}
	public virtual void DragStarted(PointerEventData evt) {}
	public virtual void DragEnded(PointerEventData evt) {}

	/// <summary>
	/// Takes in input events which were not captured by UI and other systems.
	/// </summary>
	public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
	{
		resultAppendList.Add(new RaycastResult()
		{
			gameObject = gameObject,
			screenPosition = eventData.position,
			module = this,
		});
	}
	public override Camera eventCamera => Camera;

	public void EditorUpdate()
	{
		var a = 4;
		a = 2;
		var b = a;

		if (Settings != null)
		{
			Distance = Settings.StartDistance;
			Sync();
		}
	}
}