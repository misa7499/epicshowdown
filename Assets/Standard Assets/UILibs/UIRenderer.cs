﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIRenderer : UIMonoBehaviour
{
	[NonSerialized] object data;
    [NonSerialized] public UIList List;
    [NonSerialized] public int Index;

	public object Data
	{
		get { return data; }
		set
		{
			var oldValue = data;
			data = value;

			try { OnDataChanged(value); }
			catch(Exception e) { Debug.LogException(e); }
		}
	}

	protected abstract void OnDataChanged(object newValue);
	public void Refresh() => OnDataChanged(Data);
}

public abstract class UIRenderer<T> : UIRenderer
{
	[HideInInspector, SerializeField] T serializedData;

	public new T Data
	{
		get { return (T)base.Data; }
		set { base.Data = value; }
	}

	protected override sealed void OnDataChanged(object newValue) =>
        OnDataChanged((T)newValue);

    protected abstract void OnDataChanged(T newValue);
}