using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UIRenderer), true)]
public class UIRendererInspector : Editor<UIRenderer>
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (Application.isPlaying) { return; }

        var rendererComponent = target as UIRenderer;
        var dataForRenderer = serializedObject.FindProperty("serializedData");
        if (dataForRenderer == null) { return; }

        EditorGUILayout.PropertyField(dataForRenderer, true);

        serializedObject.ApplyModifiedProperties();
        rendererComponent.Data = dataForRenderer.Parse().Value;
    }
}
