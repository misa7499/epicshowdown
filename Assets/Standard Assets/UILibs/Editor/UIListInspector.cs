using System.Collections.Generic;
using static TableStyles;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UIList))]
public class UIListInspector : Editor<UIList>
{
	public const float horizontalTablePadding = 10;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if (Application.isPlaying) { return; }

		var list = target as UIList;
		if (PrefabUtility.IsPartOfPrefabAsset(list)) { return; }
		if (PrefabUtility.IsPartOfPrefabInstance(list) && PrefabUtility.GetOutermostPrefabInstanceRoot(list) != PrefabUtility.GetNearestPrefabInstanceRoot(list)) { return; }
		if (list.RendererPrefab == null) { return; }

		var prefabProp = new SerializedObject(list.RendererPrefab).FindProperty("serializedData");
		if (prefabProp == null) { return; }

		var allRenderers = list.AllRenderersIncludingDisabled.Where(x => x != null).Distinct().ToList();
		var dataProvider = new List<object>();

		// buttons group
		GUILayout.BeginHorizontal();

			GUILayout.FlexibleSpace();
			var addClicked = GUILayout.Button("Add", EditorStyles.miniButtonLeft, GUILayout.Width(120));
			var clearClicked = GUILayout.Button("Clear", EditorStyles.miniButtonRight, GUILayout.Width(120));

		GUILayout.EndHorizontal();

		// Draw renderer stuff in table
		GUILayout.Space(12f);
		for (int i = 0; i < allRenderers.Count; i++)
		{
			// check if renderer is ok
			var renderer = allRenderers[i];
			if (renderer == null) { continue; }

			// check if prop is ok.
			var prop = GetSerializedObject(renderer).FindProperty("serializedData");
			if (prop == null) { continue; }

			dataProvider.Add(prop.GetValue<object>());

			var isSingle = prop.propertyType == SerializedPropertyType.ObjectReference || !prop.hasChildren;

			// draw header if first row.
			if (i == 0)
			{
				var headerPosition = EditorGUILayout.GetControlRect();
				headerPosition.width = 140;
					GUI.Label(headerPosition, "Renderer", CenteredLabel);
				headerPosition.x += headerPosition.width + horizontalTablePadding;

				if (isSingle)
				{
					GUI.Label(headerPosition, prop.displayName, CenteredLabel);
				}
				else
				{
					var childProp = GetSerializedObject(renderer).FindProperty("serializedData");
					var endProperty = childProp.GetEndProperty();
					for (childProp.Next(true); childProp.propertyPath != endProperty.propertyPath; childProp.Next(false))
					{
						headerPosition.width = Mathf.Min(childProp.GetDefaultWidth(TableStyles.CenteredLabel), 150);
							GUI.Label(headerPosition, childProp.displayName, CenteredLabel);
						headerPosition.x += headerPosition.width + horizontalTablePadding;
					}
				}
			}

			// draw one row
			var position = EditorGUILayout.GetControlRect();
			position.width = 140;
				EditorGUI.ObjectField(position, renderer, typeof(UIRenderer), false);
			position.x += position.width + horizontalTablePadding;

			if (isSingle)
			{
				EditorGUI.PropertyField(position, prop, GUIContent.none, false);
			}
			else
			{
				var childProp = GetSerializedObject(renderer).FindProperty("serializedData");
				var endProperty = childProp.GetEndProperty();
				for (childProp.Next(true); childProp.propertyPath != endProperty.propertyPath; childProp.Next(false))
				{
					position.width = Mathf.Min(childProp.GetDefaultWidth(CenteredLabel), 150);
						DrawProperty(position, childProp);
					position.x += position.width + horizontalTablePadding;
				}
			}

			// update renderer data if needed.
			if (prop.serializedObject.ApplyModifiedProperties())
			{
				renderer.Data = prop.GetValue<object>();
			}
			renderer.Refresh();
		}

		// handle button actions
		if (clearClicked)
		{
			allRenderers.ForEach(x => DestroyImmediate(x.gameObject));
			dataProvider.Clear();
			list.DataProvider = dataProvider;
			return;
		}
		if (addClicked)
		{
			var objectToAdd = dataProvider.Count > 0 ? dataProvider.Last() : prefabProp.GetValue<object>();
			dataProvider.Add(objectToAdd);
		}

		list.DataProvider = dataProvider;

		// sack stuff that is of the same type as my renderer
		foreach(var child in list.transform.GetComponentsInChildren<UIRenderer>())
		{
			if (list.AllRenderersIncludingDisabled.Contains(child)) { continue; }
			if (child.GetType() != list.RendererPrefab.GetType()) { continue; }
			DestroyImmediate(child.gameObject);
		}
	}

	void DrawProperty(Rect position, SerializedProperty property)
	{
		if (property.propertyType == SerializedPropertyType.Boolean)
		{
			position.x += position.width / 2 - 8f;
			position.width = 16f;
		}

		EditorGUI.PropertyField(position, property, GUIContent.none);
	}

	Dictionary<UIRenderer, SerializedObject> rendererSerializedObjects = new Dictionary<UIRenderer, SerializedObject>();
	SerializedObject GetSerializedObject(UIRenderer renderer)
	{
		if (!rendererSerializedObjects.TryGetValue(renderer, out SerializedObject so))
		{
			so = new SerializedObject(renderer);
			rendererSerializedObjects.Add(renderer, so);
		}

		so.Update();
		return so;
	}
}