using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Unity events support.
/// </summary>
public partial class UIMonoBehaviour : IPointerClickHandler
{
	public void OnPointerClick(PointerEventData eventData)
	{
		OnClicked(eventData);
		Dispatch(UIEvents.Click, eventData);
	}

	protected virtual void OnClicked(PointerEventData eventData) {}
}

public partial class UIMonoBehaviour
{
	Dictionary<IEventType, List<object>> handlersMap;

	public void Subscribe(IEventType evtType, Action handler)
	{
		if (handlersMap == null) { handlersMap = new Dictionary<IEventType, List<object>>(); }
		if (!handlersMap.TryGetValue(evtType, out var handlers))
		{
			handlers = new List<object>();
			handlersMap.Add(evtType, handlers);
		}
		handlers.Add(handler);
	}

	public void Subscribe(IEventType evtType, Action<EventData> handler)
	{
		if (handlersMap == null) { handlersMap = new Dictionary<IEventType, List<object>>(); }
		if (!handlersMap.TryGetValue(evtType, out var handlers))
		{
			handlers = new List<object>();
			handlersMap.Add(evtType, handlers);
		}
		handlers.Add(handler);
	}

	public void Unsubscribe(IEventType evtType, Action handler)
	{
		if (handlersMap != null && handlersMap.TryGetValue(evtType, out var handlers))
			handlers.Remove(handler);
	}

	public void Unsubscribe(IEventType evtType, Action<EventData> handler)
	{
		if (handlersMap != null && handlersMap.TryGetValue(evtType, out var handlers))
			handlers.Remove(handler);
	}

	public void Dispatch<T>(EventType<T> evtType, T data)
	{
		Dispatch(new EventData()
		{
			OriginalSource = this,
			EventType = evtType,
			Data = data
		});
	}

	public void Dispatch(IEventType evtType)
	{
		Dispatch(new EventData()
		{
			OriginalSource = this,
			EventType = evtType,
		});
	}

	public void Dispatch(in EventData evtData)
	{
		// fetch new invoke list from the pool.
		var invokeList = invokeListPool.Count > 0 ? invokeListPool.Pop() : new List<UIMonoBehaviour>();

		try
		{
			// bubble event upward.
			Transform currentTarget = transform;
			while(!evtData.IsConsumed && currentTarget != null)
			{
				currentTarget.GetComponents(invokeList);

				foreach(var target in invokeList)
					target.Invoke(evtData);

				currentTarget = currentTarget.transform.parent;
			}
		}
		finally
		{
			invokeListPool.Push(invokeList);
		}
	}
	static readonly Stack<List<UIMonoBehaviour>> invokeListPool = new Stack<List<UIMonoBehaviour>>();

	void Invoke(in EventData evtData)
	{
		if (handlersMap != null && handlersMap.TryGetValue(evtData.EventType, out var handlers))
		{
			foreach(var handler in handlers)
			{
				switch(handler)
				{
					case Action<EventData> action:
						action.Dispatch(evtData);
						break;
					case Action action:
						action.Dispatch();
						break;
				}
			}
		}
	}

	/// <summary>
	/// Base class of event types.
	/// </summary>
	public class IEventType
	{
		public readonly string Name;
		public IEventType(string name) => Name = name;
	}
}

public struct EventData
{
	public UIMonoBehaviour.IEventType EventType;
	public UIMonoBehaviour OriginalSource;
	public bool IsConsumed;
	public object Data;
}

public class UIEvents
{
	public static readonly EventType<PointerEventData> Click = new EventType<PointerEventData>("Click");
	public static readonly EventType<PointerEventData> PressDown = new EventType<PointerEventData>("PressDown");
	public static readonly EventType<PointerEventData> PressUp = new EventType<PointerEventData>("PressUp");
	public static readonly EventType OnSelect = new EventType("OnSelect");
	public static readonly EventType OnDeselect = new EventType("OnDeselect");
}

public class EventType : UIMonoBehaviour.IEventType
{
	public EventType(string name) : base(name) {}
}

public class EventType<T> : UIMonoBehaviour.IEventType
{
	public EventType(string name) : base(name) {}
}