using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class UIMonoBehaviour : MonoBehaviour
{
	/// <summary>
	/// Data of one delayed dispose action.
	/// </summary>
	public struct DisposeAction
	{
		public Action Action;
		public object UnsubscribeToken;
	}

	/// <summary>
	/// A list of active onDestroy actions.
	/// </summary>
	List<DisposeAction> onDestroyActions;

	public void Subscribe(Action handler, Action action)
	{
		handler += action;
		ScheduleOnDestroy(() => handler -= action, action);
	}

	public void Subscribe<T>(Action<T> handler, Action<T> action)
	{
		handler += action;
		ScheduleOnDestroy(() => handler -= action, action);
	}

	public void ScheduleOnDestroy(Action action, object token)
	{
		if (action != null)
		{
			if (onDestroyActions == null) { onDestroyActions = new List<DisposeAction>(); }
			onDestroyActions.Add(new DisposeAction()
			{
				Action = action,
				UnsubscribeToken = token
			});
		}
	}

	public virtual void OnDestroy()
	{
		if (onDestroyActions != null)
		{
			foreach(var disposeAction in onDestroyActions)
			{
				disposeAction.Action.Dispatch();
			}
		}
	}

	public void InvokeAfterSeconds(float delay, Action action)
	{
		if (delay <= 0)
		{
			action.Dispatch();
		}
		else
		{
			StartCoroutine(routine());
		}

		IEnumerator routine()
		{
			yield return new WaitForSeconds(delay);
			action.Dispatch();
		}
	}
}