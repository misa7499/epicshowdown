using UnityEngine;
using UnityEngine.UI;

public static class UnityUiAnimations
{
	public static void FadeIn(this Graphic graphic, float duration, bool ignoreTimeScale = false)
	{
		var targetAlpha = graphic.color.a;
		graphic.canvasRenderer.SetAlpha(0f);
		graphic.CrossFadeAlpha(targetAlpha, duration, ignoreTimeScale);
	}

	public static void FadeOut(this Graphic graphic, float duration, bool ignoreTimeScale = false)
	{
		graphic.CrossFadeAlpha(0f, duration, ignoreTimeScale);
	}

	public static void SetAlpha(this Graphic graphic, float alpha)
	{
		var currentColor = graphic.color;
		if (currentColor.a != alpha)
		{
			currentColor.a = alpha;
			graphic.color = currentColor;
		}
	}

	public static void SetColorAndAlpha(this Graphic graphic, Color color, float alpha)
	{
		color.a = alpha;
		graphic.color = color;
	}
}