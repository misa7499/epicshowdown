﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIList : UIMonoBehaviour
{
	public UIRenderer RendererPrefab;

	[HideInInspector, SerializeField] List<UIRenderer> renderersInPool = new List<UIRenderer>();

	public List<UIRenderer> ActiveRenderers { get; } = new List<UIRenderer>();
	public List<UIRenderer> AllRenderersIncludingDisabled => renderersInPool;

	IList dataProvider;
	public IList DataProvider
	{
		get { return dataProvider; }
		set
		{
			dataProvider = value;
			OnDataProviderChanged();
		}
	}

	void Awake()
	{
		OnDataProviderChanged();
		Subscribe(UIEvents.Click, OnItemClicked);
	}

	private void OnItemClicked(EventData evtData)
	{
		if (evtData.OriginalSource is UIRenderer renderer && renderer.List == this)
		{
			Dispatch(UIListEvents.ItemClicked, renderer);
		}
	}

	protected void OnDataProviderChanged()
	{
		// if there are more instantiated renderers than dataCount, deactivate extra renderers and keep them in the pool.
		var dataCount = dataProvider != null ? dataProvider.Count : 0;
		for(int i = 0; i < renderersInPool.Count; i++)
		{
			if (renderersInPool[i] == null)
			{
				renderersInPool.RemoveAt(i--);
				continue;
			}

			if (i >= dataCount)
			{
				if (Application.isPlaying)
				{
					renderersInPool[i].gameObject.SetActive(false);
				}
				else
				{
					DestroyImmediate(renderersInPool[i].gameObject);
					renderersInPool.RemoveAt(i--);
					continue;
				}
			}
		}

		// update active renderers list.
		ActiveRenderers.Clear();
		for(int i = 0; i < dataCount; i++)
		{
			// take existing one from the pool, or instantiate a new one.
			var newRenderer = renderersInPool.Count > i ? renderersInPool[i] : InstantiateRenderer();
			newRenderer.gameObject.SetActive(true);
			newRenderer.Index = i;
			newRenderer.List = this;
			newRenderer.Data = dataProvider[i];
			ActiveRenderers.Add(newRenderer);
		}
	}

	private UIRenderer InstantiateRenderer()
	{
#if !UNITY_EDITOR
		var newRenderer = GameObject.Instantiate(RendererPrefab, transform);
#else
		var newRenderer = Application.isPlaying ?
			GameObject.Instantiate(RendererPrefab, transform) :
			(UIRenderer)UnityEditor.PrefabUtility.InstantiatePrefab(RendererPrefab);

		((RectTransform)newRenderer.transform).SetParent(transform);
#endif
		renderersInPool.Add(newRenderer);
		return newRenderer;
	}
}

public class UIListEvents
{
	public static readonly EventType<UIRenderer> ItemClicked = new EventType<UIRenderer>("ItemClicked");
}